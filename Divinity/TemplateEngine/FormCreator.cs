﻿using Divinity.Models;
using Divinity.Models.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace Divinity.TemplateEngine {
    public class FormCreator {
        private Guid _formTemplateID;
        private Guid _dealID;
        private DivinityContext _db;

        public FormCreator(Guid formTemplateID, Guid dealID, DivinityContext db) {
            _formTemplateID = formTemplateID;
            _dealID = dealID;
            _db = db;
        }

        public async Task<Form> Create(bool persist) {
            var deal = await _db.Deal.FindAsync(_dealID);
            var template = await _db.FormTemplate
                .Include(ft => ft.Document.SignatureFields)
                .SingleOrDefaultAsync(ft => ft.FormTemplateID == _formTemplateID);
            var mapping = Utils.MapDealToTemplate(_dealID, _formTemplateID, _db);

            var form = new Form {
                CreatedAt = DateTimeOffset.Now,
                UpdatedAt = DateTimeOffset.Now,
                DealID = _dealID,
                DisplayName = template.DisplayName,
                FormID = SequentialGuidCreator.Create(),
                FormTemplateID = _formTemplateID,
                FormType = template.FormType
            };

            foreach (string key in mapping.Keys) {
                var field = new FormField {
                    CreatedAt = DateTimeOffset.Now,
                    UpdatedAt = DateTimeOffset.Now,
                    FieldName = key,
                    FieldValue = mapping[key],
                    FormFieldID = SequentialGuidCreator.Create(),
                    FormID = form.FormID
                };

                form.Fields.Add(field);
            }

            if (persist) {
                _db.Form.Add(form);
                await _db.SaveChangesAsync();
            }

            return form;
        }
    }
}
