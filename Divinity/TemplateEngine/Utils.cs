﻿using Divinity.Models;
using Divinity.Models.Helpers;
using iTextSharp.text.pdf;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace Divinity.TemplateEngine {
    public class Utils {
        public static Dictionary<string, string> MapDealToTemplate(Guid dealID, Guid formTemplateID, DivinityContext db) {
            var sourceMapping = GetSourceMappingForDeal(dealID, db);

            var allFields = db.FormTemplateAcroFieldInfo.Where(af => af.FormTemplateID == formTemplateID);

            var templateMapping = db.FormTemplateFieldMapping.Where(m => m.FormTemplateID == formTemplateID).ToArray();

            Dictionary<string, string> fieldsAndValues = new Dictionary<string, string>();
            foreach (var acrofield in allFields) {
                var mapping = templateMapping.FirstOrDefault(t => t.FormTemplateID == formTemplateID && t.AcroFieldName == acrofield.AcroFieldName && !String.IsNullOrWhiteSpace(t.SourceFieldName));
                if (mapping != null && sourceMapping.ContainsKey(mapping.SourceFieldName)) {
                    fieldsAndValues.Add(mapping.AcroFieldName, sourceMapping[mapping.SourceFieldName]);
                }
                else {
                    fieldsAndValues.Add(acrofield.AcroFieldName, null);
                }
            }

            return fieldsAndValues;
        }

        public static Dictionary<string, string> GetSourceMappingForDeal(Guid dealID, DivinityContext db) {
            Dictionary<string, string> sourceMapping = new Dictionary<string, string>();
            var deal = db.Deal.Find(dealID);
            var people = db.Person.Where(dc => dc.DealID == dealID);
            var property = db.Property.FirstOrDefault(p => p.DealID == deal.DealID);

            MapProperty(sourceMapping, property);
            MapPeople(sourceMapping, people);

            sourceMapping["displayName"] = deal.DisplayName;

            return sourceMapping;
        }

        public static byte[] RemoveSpecificFieldFromPDF(byte[] file, string fieldName) {
            try {
                using (MemoryStream pageMS = new MemoryStream()) {
                    //PdfReader.unethicalreading = true;
                    PdfReader pageReader = new PdfReader(file);
                    PdfStamper pageStamper = new PdfStamper(pageReader, pageMS);
                    pageStamper.FormFlattening = false;

                    pageStamper.AcroFields.RemoveField(fieldName);
                    
                    pageStamper.Close();
                    pageReader.Close();

                    return pageMS.ToArray();
                }
            } catch (Exception ex) {
                // not sure what happened, but I should probably be logging this at some point.
                throw ex;
            }
        }

        public static byte[] RemoveAllFieldsFromPDF(byte[] file) {
            List<string> toRemove = new List<string>();
            try {
                using (MemoryStream pageMS = new MemoryStream()) {
                    //PdfReader.unethicalreading = true;
                    PdfReader pageReader = new PdfReader(file);
                    PdfStamper pageStamper = new PdfStamper(pageReader, pageMS);
                    pageStamper.FormFlattening = false;

                    foreach (string key in pageStamper.AcroFields.Fields.Keys) {
                        toRemove.Add(key);
                    }
                    foreach (string key in toRemove) {
                        pageStamper.AcroFields.RemoveField(key);
                    }

                    pageStamper.Close();
                    pageReader.Close();

                    return pageMS.ToArray();
                }
            }
            catch (Exception ex) {
                // not sure what happened, but I should probably be logging this at some point.
                throw ex;
            }
        }

        public static byte[] RemoveGarbageFromPDF(byte[] file) {
            int temp;
            return RemoveGarbageFromPDF(file, out temp);
        }

        public static byte[] RemoveGarbageFromPDF(byte[] file, out int numFound) {
            numFound = 0;
            List<string> toRemove = new List<string>();
            try {
                using (MemoryStream pageMS = new MemoryStream()) {
                    //PdfReader.unethicalreading = true;
                    PdfReader pageReader = new PdfReader(file);
                    PdfStamper pageStamper = new PdfStamper(pageReader, pageMS);
                    pageStamper.FormFlattening = false;

                    foreach (string key in pageStamper.AcroFields.Fields.Keys) {
                        switch (pageStamper.AcroFields.GetFieldType(key)){
                            case AcroFields.FIELD_TYPE_SIGNATURE:
                            case AcroFields.FIELD_TYPE_PUSHBUTTON:
                                toRemove.Add(key);
                                continue;
                        }
                    }
                    numFound = toRemove.Count;
                    foreach (string key in toRemove) {
                        pageStamper.AcroFields.RemoveField(key);
                    }

                    pageStamper.Close();
                    pageReader.Close();

                    return pageMS.ToArray();
                }
            } catch (Exception ex) {
                // not sure what happened, but I should probably be logging this at some point.
                throw ex;
            }
        }

        public static void AddAcroFieldInfoToTemplate(byte[] file, FormTemplate template) {
            
            

            using (MemoryStream pageMS = new MemoryStream()) {
                PdfReader pageReader = new PdfReader(file);
                
                
                PdfStamper pageStamper = new PdfStamper(pageReader, pageMS);

                foreach (string key in pageStamper.AcroFields.Fields.Keys) {
                    var field = pageStamper.AcroFields.GetFieldItem(key);
                    int type = pageStamper.AcroFields.GetFieldType(key);
                    
                    var position = pageStamper.AcroFields.GetFieldPositions(key)[0].position;
                    // have to adjust pts to pixels @ 96 dpi
                    var x = position.Left * 4.0 / 3.0;
                    var y = position.Top * 4.0 / 3.0;
                    var w = position.Width * 4.0 / 3.0;
                    var h = position.Height * 4.0 / 3.0;
                    var visible = true;
                    var editable = true;

                    string[] optionValues = null, optionTexts = null;

                    string inputType = "unknown";

                    switch (type) {
                        case AcroFields.FIELD_TYPE_CHECKBOX:
                            inputType = "checkbox";
                            var states = pageStamper.AcroFields.GetAppearanceStates(key);
                            break;
                        case AcroFields.FIELD_TYPE_RADIOBUTTON:
                        case AcroFields.FIELD_TYPE_COMBO:
                            inputType = "select";
                            optionValues = pageStamper.AcroFields.GetListOptionExport(key);
                            optionTexts = pageStamper.AcroFields.GetListOptionDisplay(key);
                            break;
                        case AcroFields.FIELD_TYPE_TEXT:
                            inputType = "text";
                            break;
                        case AcroFields.FIELD_TYPE_SIGNATURE:
                            inputType = "signature";
                            editable = false;
                            break;
                        default:
                            continue; // unsupported
                    }

                    var info = new FormTemplateAcroFieldInfo {
                        AcroFieldName = key,
                        FieldDescription = String.Empty,
                        FieldTitle = String.Empty,
                        FormTemplateAcroFieldInfoID = SequentialGuidCreator.Create(),
                        FormTemplateID = template.FormTemplateID,
                        InputType = inputType,
                        TabOrder = field.GetTabOrder(0),
                        UseDefaultDisplayInfoIfMapped = true,
                        PageNumber = field.GetPage(0),
                        LeftX = (int)Math.Round(x),
                        BottomY = (int)Math.Round(y),
                        Width = (int)Math.Round(w),
                        Height = (int)Math.Round(h),
                        Visible = visible,
                        Editable = editable
                    };
                    if (optionValues != null) {
                        for (int i = 0; i < optionValues.Length; i++) {
                            info.SelectionOptions.Add(new FormTemplateAcroFieldSelectionOption {
                                DisplayName = optionTexts[i],
                                FormTemplateAcroFieldInfoID = info.FormTemplateAcroFieldInfoID,
                                FormTemplateAcroFieldSelectionOptionID = SequentialGuidCreator.Create(),
                                SelectionValue = optionValues[i],
                                SortOrder = i
                            });
                        }
                    }
                    if (inputType != "signature") {
                        template.AcroFields.Add(info);
                    }
                    else {
                        template.Document.SignatureFields.Add(new SignatureInfo { 
                            DocumentID = template.Document.DocumentID,
                            SignatureInfoID = SequentialGuidCreator.Create(),
                            SignatureType = "Signature",
                            HasDate = false,
                            Height = info.Height,
                            Width = info.Width,
                            X = info.LeftX,
                            Y = info.BottomY,
                            PageNumber = info.PageNumber
                        });
                    }
                }
            }
        }


        private static void MapPeople(Dictionary<string, string> sourceMapping, IQueryable<Person> people) {
            foreach (var person in people) {
                string role = person.RoleName;
                sourceMapping[String.Format("{0}.firstLast", role)] = person.FirstName + " " + person.LastName;
                sourceMapping[String.Format("{0}.lastFirst", role)] = person.LastName + ", " + person.FirstName;
                sourceMapping[String.Format("{0}.firstName", role)] = person.FirstName;
                sourceMapping[String.Format("{0}.lastName", role)] = person.LastName;
                sourceMapping[String.Format("{0}.address1", role)] = person.Address1;
                sourceMapping[String.Format("{0}.address2", role)] = person.Address2;
                sourceMapping[String.Format("{0}.city", role)] = person.City;
                sourceMapping[String.Format("{0}.state", role)] = person.State;
                sourceMapping[String.Format("{0}.zipCode", role)] = person.ZipCode;
                sourceMapping[String.Format("{0}.email", role)] = person.Email;
                sourceMapping[String.Format("{0}.mobileNumber", role)] = person.MobileNumber;
            }
            // regex to match Buyer N
            Regex buyerRegex = new Regex("^Buyer [0-9]+$");
            string buyerNames = String.Join(", ", people.ToList().Where(p => buyerRegex.IsMatch(p.RoleName)).Select(p => p.FirstName + " " + p.LastName));
            sourceMapping["buyernames"] = buyerNames;

            Regex sellerRegex = new Regex("^Seller [0-9]+$");
            string sellerNames = String.Join(", ", people.ToList().Where(p => sellerRegex.IsMatch(p.RoleName)).Select(p => p.FirstName + " " + p.LastName));
            sourceMapping["sellernames"] = sellerNames;
        }

        private static void MapProperty(Dictionary<string, string> sourceMapping, Property property) {
            StringBuilder fullAddress = new StringBuilder();
            fullAddress.Append(property.Address1);
            if (!String.IsNullOrWhiteSpace(property.Address2)) {
                fullAddress.AppendFormat(" {0}", property.Address2);
            }
            fullAddress.AppendFormat(", {0}, {1}, {2} {3}", property.City, property.County, property.State, property.ZipCode);

            sourceMapping["property.fullAddress"] = fullAddress.ToString();

            sourceMapping["property.address1"] = property.Address1;
            sourceMapping["property.address2"] = property.Address2;
            sourceMapping["property.address1plus2"] = String.Format("{0} {1}",property.Address1,property.Address2);
            sourceMapping["property.cityCountyStateZip"] = String.Format("{0}, {1}, {2} {3}", property.City, property.County, property.State, property.ZipCode);
            sourceMapping["property.city"] = property.City;
            sourceMapping["property.county"] = property.County;
            sourceMapping["property.state"] = property.State;
            sourceMapping["property.zipCode"] = property.ZipCode;
            sourceMapping["property.taxID"] = property.TaxID;
            sourceMapping["property.legalDescription"] = property.LegalDescription;
        }
    }
}