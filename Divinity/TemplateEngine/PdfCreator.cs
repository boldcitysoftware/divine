﻿using System.Data.Entity.Infrastructure;
using System.Data.Entity;
using Divinity.Models;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Text;
using System.Runtime.InteropServices;
using System.Resources;
using Divinity.Models.Helpers;
using iTextSharp.text;
using System.Web;
using System.Reflection;
using System.Xml;

namespace Divinity.TemplateEngine {
    public static class PdfCreator {
        private static PrivateFontCollection _fonts;

        static PdfCreator() {
            _fonts = new PrivateFontCollection();
            DivinityContext db = new DivinityContext();
            foreach (var font in db.Font) {
                AddFontFromFile(_fonts, font);
            }
        }

        public static async Task<byte[]> CreateSignedSnapshotAsync(Guid documentID, DivinityContext db) {
            try {
                var form = await db.Form
                    .Include(f => f.Document.SignatureFields)
                    .Include(f => f.Document.Pages)
                    .Include(f => f.Document.SignatureRequests.Select(r => r.Signatures))
                    .Include(f => f.Document.SignatureRequests.Select(r => r.Signer))
                    .SingleOrDefaultAsync(f => f.DocumentID == documentID);
                if (form != null) {
                    return await PdfCreator.CreateSignedSnapshotAsync(form, db);
                }
                else {
                    byte[] formPDF = await new Storage.AzureBlobStorage().GetBytesAsync("document", documentID.ToString().ToLower());
                    var document = await db.Document
                        .Include(d => d.SignatureRequests)
                        .Include(d => d.SignatureFields)
                        .Include(d => d.Pages)
                        .SingleOrDefaultAsync(d => d.DocumentID == documentID);
                    var signedPDF = await StampSignaturesOntoPDFAsync(document, formPDF, db);
                    await new Storage.AzureBlobStorage().StoreBytesAsync("signed-document", document.DocumentID.ToString(), signedPDF);
                    await RefreshDocumentPages(document, signedPDF, db);
                    return signedPDF;
                }
            }
            catch (Exception ex) {
                ex.ToString();
                return null;
            }
        }

        public static async System.Threading.Tasks.Task RefreshDocumentPages(Divinity.Models.Document document, byte[] pdfToUse, DivinityContext db) {
            var pages = document.Pages;

            IEnumerable<PdfToPng.Converter.PageImageInfo> images = PdfToPng.Converter.Convert(pdfToUse);

            foreach (var image in images) {
                var page = pages.SingleOrDefault(p => p.PageNumber == image.PageNumber);
                if (page == null) {
                    // I need to create this page.
                    page = new Page {
                        DocumentID = document.DocumentID,
                        PageID = SequentialGuidCreator.Create(),
                        PageHeight = image.Height,
                        PageWidth = image.Width,
                        PageNumber = image.PageNumber
                    };
                    pages.Add(page);
                }
                await new Storage.AzureBlobStorage().StoreBytesAsync("page-image", page.PageID.ToString(), image.PngBytes);
            }

            await db.SaveChangesAsync();
        }

        public static async Task<byte[]> CreateSignedSnapshotAsync(Form form, DivinityContext db) {
            if (form.Document == null || form.Document.Pages == null || form.Document.SignatureFields == null || form.Document.SignatureRequests == null) {
                throw new ArgumentException("form must have a document with included pages, signature fields, and signature requests collections.");
            }

            var document = form.Document;

            byte[] formPDF = await new Storage.AzureBlobStorage().GetBytesAsync("document", document.DocumentID.ToString().ToLower());

            byte[] signedPDF = await StampSignaturesOntoPDFAsync(document, formPDF, db);

            await new Storage.AzureBlobStorage().StoreBytesAsync("signed-document", document.DocumentID.ToString(), signedPDF);

            await RefreshDocumentPages(document, signedPDF, db);

            return signedPDF;
        }

        public static async Task<byte[]> CreateFormPdfAsync(Form form, DivinityContext db, bool saveToCloudBlob = true) {

            byte[] formPDF = null;
            if (formPDF == null) {
                byte[] templateBytes = await new Storage.AzureBlobStorage().GetBytesAsync("template", form.FormTemplateID.ToString().ToLower());
                formPDF = GeneratePDF(templateBytes, form.Fields.ToDictionary(f => f.FieldName, f => f.FieldValue), true);
                if (saveToCloudBlob) {
                    await new Storage.AzureBlobStorage().StoreBytesAsync("filled-form", form.FormID.ToString().ToLower(), formPDF);
                }
            }

            return formPDF;
        }

        public static Bitmap CreateSignatureBitmap(string signatureText, float sizeInPoints, Divinity.Models.Font signatureFont) {
            var objBmpImage = new Bitmap(1, 1);

            int intWidth = 0;
            int intHeight = 0;

            var family = _fonts.Families.Single(f => f.Name == signatureFont.Name);

            var ttf = new System.Drawing.Font(family, sizeInPoints, FontStyle.Bold);

            // Create a graphics object to measure the text's width and height.
            Graphics objGraphics = Graphics.FromImage(objBmpImage);

            // This is where the bitmap size is determined.
            intWidth = (int)objGraphics.MeasureString(signatureText, ttf).Width;
            intHeight = (int)objGraphics.MeasureString(signatureText, ttf).Height;

            // Create the bmpImage again with the correct size for the text and font.
            objBmpImage = new Bitmap(objBmpImage, new Size(intWidth, intHeight));

            // Add the colors to the new bitmap.
            objGraphics = Graphics.FromImage(objBmpImage);

            // Set Background color
            objGraphics.Clear(Color.Transparent);
            objGraphics.SmoothingMode = SmoothingMode.AntiAlias;
            objGraphics.TextRenderingHint = TextRenderingHint.AntiAlias;
            objGraphics.DrawString(signatureText, ttf, new SolidBrush(Color.DarkBlue), 0, 0);
            objGraphics.Flush();
            return (objBmpImage);
        }

        public static void AddFontFromFile(PrivateFontCollection pfc, Divinity.Models.Font myFont) {
            string fontPath = null;
            if (HttpContext.Current != null) {
                fontPath = Path.Combine(HttpContext.Current.Server.MapPath("~/bin/Fonts"), myFont.RelativePath);
            }
            else {
                fontPath = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "Fonts", myFont.RelativePath);
            }
            pfc.AddFontFile(fontPath);
        }

        public static void AddFontFromResource(PrivateFontCollection pfc, Divinity.Models.Font myFont) {
            ResourceManager rm = new ResourceManager(typeof(Fonts));
            
            using (Stream fontStream = new MemoryStream((byte[])rm.GetObject(myFont.ResourceName))) {
                if (null == fontStream) {
                    return;
                }

                int fontStreamLength = (int)fontStream.Length;

                IntPtr data = Marshal.AllocCoTaskMem(fontStreamLength);

                byte[] fontData = new byte[fontStreamLength];
                fontStream.Read(fontData, 0, fontStreamLength);

                Marshal.Copy(fontData, 0, data, fontStreamLength);

                pfc.AddMemoryFont(data, fontStreamLength);

                Marshal.FreeCoTaskMem(data);
            }
        }

        public static async Task<byte[]> StampSignaturesOntoPDFAsync(Divinity.Models.Document document, byte[] filledPDF, DivinityContext db) {
            var signaturePositions = await db.SignaturePosition.Where(p => p.DocumentID == document.DocumentID).ToListAsync();
            var fonts = await db.Font.ToListAsync();

            using (MemoryStream pageMS = new MemoryStream()) {
                //PdfReader.unethicalreading = true;
                var reader = new PdfReader(filledPDF);
                var stamper = new PdfStamper(reader, pageMS);
                stamper.FormFlattening = true;
                stamper.AcroFields.GenerateAppearances = true;
                
                var regularFont = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.EMBEDDED);
                var courier = BaseFont.CreateFont(BaseFont.COURIER, BaseFont.CP1252, BaseFont.EMBEDDED);

                foreach (var signature in document.SignatureRequests.SelectMany(r => r.Signatures)) {
                    var positionInfo = signaturePositions.Single(sp => sp.SignatureInfoID == signature.SignatureInfoID);
                    var content = stamper.GetOverContent(positionInfo.PageNumber);
                    var ptSize = (float)positionInfo.FieldHeight * (float)(3.0 / 4.0);
                    var signatureFont = fonts.Single(f => f.Name == signature.FontName);

                    string fontPath = null;
                    if (HttpContext.Current != null) {
                        fontPath = Path.Combine(HttpContext.Current.Server.MapPath("~/bin/Fonts"), signatureFont.RelativePath);
                    }
                    else {
                        fontPath = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "Fonts", signatureFont.RelativePath);
                    }
                    var sigFont = BaseFont.CreateFont(fontPath, BaseFont.CP1252, BaseFont.EMBEDDED);
                    
                    content.BeginText();
                    
                    content.SetRGBColorStroke(Color.DarkBlue.R, Color.DarkBlue.G, Color.DarkBlue.B);
                    content.SetRGBColorFill(Color.DarkBlue.R, Color.DarkBlue.G, Color.DarkBlue.B);
                    content.SetFontAndSize(sigFont, (float)positionInfo.FieldHeight * (float)(3.0 / 4.0));
                    content.ShowTextAligned(Element.ALIGN_LEFT, signature.SignatureText, (float)positionInfo.FieldX * (float)(3.0 / 4.0), (float)(positionInfo.FieldY - positionInfo.FieldHeight) * (float)(3.0 / 4.0), (float)0);

                    if (positionInfo.HasDate) {
                        content.SetFontAndSize(regularFont, (float)positionInfo.DateFieldHeight * (float)(3.0 / 4.0));
                        content.ShowTextAligned(Element.ALIGN_LEFT, signature.ExecutedAt.Date.ToShortDateString(), (float)positionInfo.DateFieldX * (float)(3.0 / 4.0), (float)(positionInfo.DateFieldY - positionInfo.DateFieldHeight) * (float)(3.0 / 4.0), (float)0);
                    }

                    content.EndText();
                }

                var p1 = stamper.GetOverContent(1);
                Anchor a1 = new Anchor(String.Format("To verify document, visit: https://deals.divinity.io/#/verify/{0}", document.DocumentID), new iTextSharp.text.Font(courier, 8, iTextSharp.text.Font.UNDERLINE, BaseColor.GRAY));
                a1.Reference = String.Format("https://deals.divinity.io/#/verify/{0}", document.DocumentID);
                ColumnText.ShowTextAligned(p1, Element.ALIGN_LEFT, a1, 15, reader.GetPageSize(1).Height - 20, 0);

                //p1.BeginText();
                //p1.SetFontAndSize(regularFont, 10);
                //p1.ShowTextAligned(Element.ALIGN_LEFT, , 0, reader.GetPageSize(1).Height - 15, 15);
                //p1.EndText();

                stamper.Close();
                reader.Close();

                return pageMS.ToArray();
            }
        }

        public static byte[] GeneratePDF(byte[] templateBytes, Dictionary<string, string> fieldMapping, bool makeReadOnly) {
            
            using (MemoryStream pageMS = new MemoryStream()) {
                PdfReader pageReader = new PdfReader(templateBytes);
                PdfStamper pageStamper = new PdfStamper(pageReader, pageMS);
                pageStamper.FormFlattening = makeReadOnly;

                foreach (string key in fieldMapping.Keys) {
                    bool successfulStamp = pageStamper.AcroFields.SetField(key, fieldMapping[key] ?? "");
                }

                pageStamper.Close();
                pageReader.Close();

                return pageMS.ToArray();
            }
            
        }
    }
}
