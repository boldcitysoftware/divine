﻿using Microsoft.Azure.Documents.Linq;
using Divinity.Models;
using Divinity.TemplateEngine;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Stripe;
using Microsoft.Azure.Documents;
using Microsoft.Azure.Documents.Client;
using iTextSharp.text.pdf;
using System.Xml;
using Divinity.PdfToPng;

namespace Divinity.TestCLI {
    class Program {
        static void Main(string[] args) {
            byte[] templateBytes = File.ReadAllBytes(@"C:\Users\Dave\Google Drive\DivineRES\Nefar\Doctored\contract.pdf");
            Dictionary<string, string> fieldMapping = new Dictionary<string, string>();
            using (MemoryStream pageMS = new MemoryStream()) {
                
                PdfReader pageReader = new PdfReader(templateBytes);
                PdfStamper pageStamper = new PdfStamper(pageReader, pageMS);

                pageStamper.FormFlattening = true;

                pageStamper.AcroFields.SetField("buyer", "Jeremiah McCormick & Bartholomew Stephanopolis");
                pageStamper.AcroFields.SetField("seller", "Jeremiah McCormick & Bartholomew Stephanopolis");
                pageStamper.AcroFields.SetField("property_street_city_zip", "10940 Raley Creek Dr S, Jacksonville, 32225");
                pageStamper.AcroFields.SetField("property_county", "Duval");
                pageStamper.AcroFields.SetField("property_tax_id", "12-345-6789");
                pageStamper.AcroFields.SetField("property_legal_description", "test, test2, test3\r\ntest4, test5, test6");
                pageStamper.AcroFields.SetField("binder_amount", "100,000");
                pageStamper.AcroFields.SetField("binder_deposit_due_within", "45");
                pageStamper.AcroFields.SetField("binder_deposit_amount", "15,000");
                pageStamper.AcroFields.SetField("additional_binder_deposit_deadline", "May 1, 2015");
                pageStamper.AcroFields.SetField("additional_binder_deposit_due_within", "45");
                pageStamper.AcroFields.SetField("additional_binder_deposit_amount", "5,000");
                pageStamper.AcroFields.SetField("balance_due_at_closing", "10,000");
                pageStamper.AcroFields.SetField("third_party_financed_amount", "100,000");
                pageStamper.AcroFields.SetField("seller_financed_amount", "0");
                pageStamper.AcroFields.SetField("purchase_price", "200,000");
                pageStamper.AcroFields.SetField("escrow_agent_name", "Bobby Tables");
                pageStamper.AcroFields.SetField("escrow_agent_address", "123 Main St, Jacksonville, FL 32211");
                pageStamper.AcroFields.SetField("escrow_agent_phone", "904-123-4567");
                pageStamper.AcroFields.SetField("escrow_agent_fax", "904-123-4567");
                pageStamper.AcroFields.SetField("escrow_agent_email", "escrowsrus@gmail.com");
                pageStamper.AcroFields.SetField("cash_transaction", "On");
                pageStamper.AcroFields.SetField("loan_without_financing_contingency", "On");
                pageStamper.AcroFields.SetField("loan_as_marked_below_with_financing_contingency", "On");
                pageStamper.AcroFields.SetField("have_the_privilege_and_option_of_proceeding_with_consummation_of_this_Contract_without_regard_to_the", "200,000");
                pageStamper.AcroFields.SetField("fha", "On");
                pageStamper.AcroFields.SetField("va", "On");
                pageStamper.AcroFields.SetField("conventional_or_usda", "On");
                pageStamper.AcroFields.SetField("other_financing", "On");
                pageStamper.AcroFields.SetField("mortgage_assumption", "On");
                pageStamper.AcroFields.SetField("seller_financing", "On");
                pageStamper.AcroFields.SetField("loan_application_within", "10");
                pageStamper.AcroFields.SetField("loan_approval_period_days", "90");
                pageStamper.AcroFields.SetField("including_legal_access_the_transaction_will_be_closed_and_the_deed_and_other_closing_papers_delivered[0]", "On");
                pageStamper.AcroFields.SetField("including_legal_access_the_transaction_will_be_closed_and_the_deed_and_other_closing_papers_delivered[1]", "On");
                pageStamper.AcroFields.SetField("Agreement_unless_extended_by_other_conditions_of_this_Agreement_Marketable_title_means_title_which", "May 1, 2015");
                pageStamper.AcroFields.SetField("days_after_date_of_acceptance_of_this", "30");
                pageStamper.AcroFields.SetField("days_before_date_of_closing_5_days_if_left_blank_the_party_paying_for", "10");
                pageStamper.AcroFields.SetField("Title_insurance_commitment_for_mortgage_policy_in_the_amount_of", "On");
                pageStamper.AcroFields.SetField("the_new_mortgage_Any_expense_of_curing_title_defects_such_as_but_not_limited_to_legal_fees_discharge_of", "On");
                pageStamper.AcroFields.SetField("days_before_date_of_closing_5_days_if_left_blank_the_party_paying_for_the_survey", "10");
                pageStamper.AcroFields.SetField("months_of_date_of_closing_showing_all_improvements_certified_to_BUYER_lender_and_the_title_insurer_in", "On");
                pageStamper.AcroFields.SetField("improvements_and_sufficient_to_allow_removal_of_the_survey_exceptions_from_the_title_insurance", "On");
                pageStamper.AcroFields.SetField("If_a_surveyors_flood_elevation_certificate_is_required_BUYER_shall_pay_for_it", "On");
                pageStamper.AcroFields.SetField("facts_materially_affecting_property_value", "test\r\ntest2");
                pageStamper.AcroFields.SetField("citations_issued", "test\r\ntest2");
                pageStamper.AcroFields.SetField("noncompliant_improvements", "test\r\ntest2");
                pageStamper.AcroFields.SetField("other_personal_property_list", "test\r\ntest2");
                pageStamper.AcroFields.SetField("property_excluded", "test\r\ntest2");
                pageStamper.AcroFields.SetField("additional_terms_conditions", "test\r\ntest2\r\ntest3\r\ntest4\r\ntest5\r\ntest6\r\ntest7\r\ntest8\r\ntest9");

                pageStamper.AcroFields.SetField("known_hazards_expl", "Hi. This is Dave's silly little test. It should span two lines and line up properly. Again, this is just a simple test.");
                pageStamper.AcroFields.SetField("documents_list", "Hi. This is Dave's silly little test. It should span two lines and line up properly. Again, this is just a simple test.");
                pageStamper.Close();
                pageReader.Close();

                File.WriteAllBytes(@"C:\Users\Dave\Google Drive\DivineRES\Nefar\Doctored\result.pdf", pageMS.ToArray());
            }

            //byte[] file = File.ReadAllBytes(@"C:\Users\Dave\Google Drive\DivineRES\Nefar\Doctored\result.pdf");
            //var result = Converter.Convert(file);
            //result.ToString();
            //File.WriteAllBytes(@"C:\Users\Dave\Google Drive\DivineRES\Nefar\Doctored\img.png", result.First().PngBytes);
        }
    }
}
