namespace Divinity.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("DealAccess")]
    public partial class DealAccess
    {
        [Key]
        public Guid AccessID { get; set; }

        public Guid DealID { get; set; }

        public Guid PersonID { get; set; }

        [Required]
        [StringLength(128)]
        public string AccessGrantedBy { get; set; }

        public DateTimeOffset AccessGrantedAt { get; set; }

        [Required]
        [StringLength(128)]
        public string UserID { get; set; }

        public bool Revoked { get; set; }

        public virtual Deal Deal { get; set; }

        public virtual Person Person { get; set; }
    }
}
