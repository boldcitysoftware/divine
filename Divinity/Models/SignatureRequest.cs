namespace Divinity.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SignatureRequest")]
    public partial class SignatureRequest
    {
        public SignatureRequest()
        {
            Signatures = new HashSet<Signature>();
        }

        [Key]
        public Guid RequestID { get; set; }

        public Guid DealID { get; set; }

        public Guid DocumentID { get; set; }

        [Required]
        [StringLength(128)]
        public string RequesterUserID { get; set; }

        public Guid SignerPersonID { get; set; }

        [Required]
        [StringLength(256)]
        public string SignerEmail { get; set; }

        public DateTimeOffset CreatedAt { get; set; }

        public bool Revoked { get; set; }

        public DateTimeOffset? AllSignaturesConfirmedAt { get; set; }
        public DateTimeOffset? ConsentGivenAt { get; set; }
        public DateTimeOffset? SignatureAdoptedAt { get; set; }

        public virtual Deal Deal { get; set; }

        public virtual Document Document { get; set; }

        public virtual Person Signer { get; set; }

        public virtual ICollection<Signature> Signatures { get; set; }
    }
}
