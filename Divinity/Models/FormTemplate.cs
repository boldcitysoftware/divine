namespace Divinity.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("FormTemplate")]
    public partial class FormTemplate
    {
        public FormTemplate()
        {
            AcroFields = new HashSet<FormTemplateAcroFieldInfo>();
            FieldMappings = new HashSet<FormTemplateFieldMapping>();
        }

        public Guid FormTemplateID { get; set; }

        [Required]
        [StringLength(150)]
        public string DisplayName { get; set; }

        [Required]
        [StringLength(100)]
        public string FormType { get; set; }

        [Required]
        [StringLength(50)]
        public string Jurisdiction { get; set; }

        public DateTimeOffset CreatedAt { get; set; }

        public DateTimeOffset UpdatedAt { get; set; }

        public bool Public { get; set; }

        public int PageCount { get; set; }

        public Guid DocumentID { get; set; }

        [StringLength(150)]
        public string CustomHtmlPath { get; set; }

        public virtual Document Document { get; set; }

        public virtual SupportedJurisdiction SupportedJurisdiction { get; set; }

        public virtual ICollection<FormTemplateAcroFieldInfo> AcroFields { get; set; }

        public virtual ICollection<FormTemplateFieldMapping> FieldMappings { get; set; }
    }
}
