namespace Divinity.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Document")]
    public partial class Document
    {
        public Document()
        {
            Pages = new HashSet<Page>();
            SignatureFields = new HashSet<SignatureInfo>();
            SignatureRequests = new HashSet<SignatureRequest>();
        }

        public Guid DocumentID { get; set; }

        public Guid? DealID { get; set; }

        [Required]
        [StringLength(150)]
        public string FileName { get; set; }

        [Required]
        [StringLength(150)]
        public string ContentType { get; set; }

        public DateTimeOffset CreatedAt { get; set; }

        public DateTimeOffset UpdatedAt { get; set; }

        [Required]
        [StringLength(150)]
        public string DisplayName { get; set; }

        public DateTimeOffset? DeletedAt { get; set; }

        public virtual Deal Deal { get; set; }

        public virtual ICollection<Page> Pages { get; set; }

        public virtual ICollection<SignatureInfo> SignatureFields { get; set; }

        public virtual ICollection<SignatureRequest> SignatureRequests { get; set; }
    }
}
