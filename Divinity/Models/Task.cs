namespace Divinity.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Task")]
    public partial class Task
    {
        public Guid TaskID { get; set; }

        public Guid TaskListID { get; set; }

        [Required]
        [StringLength(150)]
        public string DisplayName { get; set; }

        public DateTimeOffset CreatedAt { get; set; }

        public DateTimeOffset UpdatedAt { get; set; }

        public DateTimeOffset? DueAt { get; set; }

        public bool Done { get; set; }

        public int? SortOrder { get; set; }

        public virtual TaskList TaskList { get; set; }
    }
}
