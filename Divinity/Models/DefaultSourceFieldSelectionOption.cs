namespace Divinity.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("DefaultSourceFieldSelectionOption")]
    public partial class DefaultSourceFieldSelectionOption
    {
        [Key]
        [Column(Order = 0)]
        [StringLength(50)]
        public string SourceFieldName { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int SortOrder { get; set; }

        [StringLength(250)]
        public string DisplayName { get; set; }

        [StringLength(250)]
        public string SelectionValue { get; set; }
    }
}
