namespace Divinity.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("TransactionType")]
    public partial class TransactionType
    {
        public TransactionType()
        {
            Deal = new HashSet<Deal>();
        }

        [Key]
        [StringLength(50)]
        public string TypeName { get; set; }

        [Required]
        public string TypeDescription { get; set; }

        public virtual ICollection<Deal> Deal { get; set; }
    }
}
