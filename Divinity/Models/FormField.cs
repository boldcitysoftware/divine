namespace Divinity.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("FormField")]
    public partial class FormField
    {
        public Guid FormFieldID { get; set; }

        public Guid FormID { get; set; }

        [StringLength(250)]
        public string FieldName { get; set; }

        [StringLength(250)]
        public string FieldValue { get; set; }

        public DateTimeOffset CreatedAt { get; set; }

        public DateTimeOffset UpdatedAt { get; set; }
    }
}
