namespace Divinity.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("FormTemplateFieldMapping")]
    public partial class FormTemplateFieldMapping
    {
        [Key]
        public Guid FieldMappingID { get; set; }

        public Guid FormTemplateID { get; set; }

        [Required]
        [StringLength(150)]
        public string AcroFieldName { get; set; }

        [StringLength(50)]
        [DefaultValue("")]
        public string SourceFieldName { get; set; }

        public virtual FormTemplate FormTemplate { get; set; }
    }
}
