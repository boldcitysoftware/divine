namespace Divinity.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("DefaultSourceFieldInfo")]
    public partial class DefaultSourceFieldInfo
    {
        [Key]
        [StringLength(50)]
        public string SourceFieldName { get; set; }

        [Required]
        [StringLength(50)]
        public string FieldTitle { get; set; }

        [Required]
        public string FieldDescription { get; set; }

        [StringLength(100)]
        public string InputType { get; set; }
    }
}
