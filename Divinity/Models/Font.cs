namespace Divinity.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Font")]
    public partial class Font
    {
        public Font()
        {
        }

        [Key]
        [StringLength(100)]
        public string Name { get; set; }

        [Required]
        [StringLength(200)]
        public string RelativePath { get; set; }

        [Required]
        [StringLength(50)]
        public string CssFamily { get; set; }

        [StringLength(50)]
        public string ResourceName { get; set; }
    }
}
