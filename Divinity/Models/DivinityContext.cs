namespace Divinity.Models {
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class DivinityContext : DbContext {
        public DivinityContext()
            : base("name=DivinityContext") {
        }

        public virtual DbSet<C__MigrationHistory> C__MigrationHistory { get; set; }
        public virtual DbSet<AspNetUser> AspNetUsers { get; set; }
        public virtual DbSet<Deal> Deal { get; set; }
        public virtual DbSet<DealAccess> DealAccess { get; set; }
        public virtual DbSet<DefaultSourceFieldInfo> DefaultSourceFieldInfo { get; set; }
        public virtual DbSet<DefaultSourceFieldSelectionOption> DefaultSourceFieldSelectionOption { get; set; }
        public virtual DbSet<Document> Document { get; set; }
        public virtual DbSet<Font> Font { get; set; }
        public virtual DbSet<Form> Form { get; set; }
        public virtual DbSet<FormField> FormField { get; set; }
        public virtual DbSet<FormTemplate> FormTemplate { get; set; }
        public virtual DbSet<FormTemplateAcroFieldInfo> FormTemplateAcroFieldInfo { get; set; }
        public virtual DbSet<FormTemplateAcroFieldSelectionOption> FormTemplateAcroFieldSelectionOption { get; set; }
        public virtual DbSet<FormTemplateFieldMapping> FormTemplateFieldMapping { get; set; }
        public virtual DbSet<Page> Page { get; set; }
        public virtual DbSet<Person> Person { get; set; }
        public virtual DbSet<PersonRole> PersonRole { get; set; }
        public virtual DbSet<Property> Property { get; set; }
        public virtual DbSet<Signature> Signature { get; set; }
        public virtual DbSet<SignatureInfo> SignatureInfo { get; set; }
        public virtual DbSet<SignatureRequest> SignatureRequest { get; set; }
        public virtual DbSet<SignatureType> SignatureType { get; set; }
        public virtual DbSet<SupportedJurisdiction> SupportedJurisdiction { get; set; }
        public virtual DbSet<Task> Task { get; set; }
        public virtual DbSet<TaskList> TaskList { get; set; }
        public virtual DbSet<TransactionStatus> TransactionStatus { get; set; }
        public virtual DbSet<TransactionType> TransactionType { get; set; }
        public virtual DbSet<UserProfile> UserProfile { get; set; }
        public virtual DbSet<UserSignature> UserSignature { get; set; }
        public virtual DbSet<UserSubscription> UserSubscription { get; set; }
        public virtual DbSet<AcroFieldPositionOnPageImage> AcroFieldPositionOnPageImage { get; set; }
        public virtual DbSet<DisplayNameLookup> DisplayNameLookup { get; set; }
        public virtual DbSet<SignaturePosition> SignaturePosition { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder) {

            modelBuilder.Entity<Deal>()
                .Property(e => e.DisplayName)
                .IsUnicode(false);

            modelBuilder.Entity<Deal>()
                .Property(e => e.Jurisdiction)
                .IsUnicode(false);

            modelBuilder.Entity<Deal>()
                .Property(e => e.TransactionStatus)
                .IsUnicode(false);

            modelBuilder.Entity<Deal>()
                .Property(e => e.TransactionType)
                .IsUnicode(false);

            modelBuilder.Entity<Deal>()
                .HasMany(e => e.AccessGrants)
                .WithRequired(e => e.Deal)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Deal>()
                .HasMany(e => e.Signatures)
                .WithRequired(e => e.Deal)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Deal>()
                .HasMany(e => e.SignatureRequests)
                .WithRequired(e => e.Deal)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Deal>()
                .HasMany(e => e.TaskLists)
                .WithRequired(e => e.Deal)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<DefaultSourceFieldInfo>()
                .Property(e => e.SourceFieldName)
                .IsUnicode(false);

            modelBuilder.Entity<DefaultSourceFieldInfo>()
                .Property(e => e.FieldTitle)
                .IsUnicode(false);

            modelBuilder.Entity<DefaultSourceFieldInfo>()
                .Property(e => e.FieldDescription)
                .IsUnicode(false);

            modelBuilder.Entity<DefaultSourceFieldInfo>()
                .Property(e => e.InputType)
                .IsUnicode(false);

            modelBuilder.Entity<DefaultSourceFieldSelectionOption>()
                .Property(e => e.SourceFieldName)
                .IsUnicode(false);

            modelBuilder.Entity<DefaultSourceFieldSelectionOption>()
                .Property(e => e.DisplayName)
                .IsUnicode(false);

            modelBuilder.Entity<DefaultSourceFieldSelectionOption>()
                .Property(e => e.SelectionValue)
                .IsUnicode(false);

            modelBuilder.Entity<Document>()
                .Property(e => e.FileName)
                .IsUnicode(false);

            modelBuilder.Entity<Document>()
                .Property(e => e.ContentType)
                .IsUnicode(false);

            modelBuilder.Entity<Document>()
                .Property(e => e.DisplayName)
                .IsUnicode(false);

            modelBuilder.Entity<Document>()
                .HasMany(e => e.Pages)
                .WithRequired(e => e.Document)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Document>()
                .HasMany(e => e.SignatureFields)
                .WithRequired(e => e.Document)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Document>()
                .HasMany(e => e.SignatureRequests)
                .WithRequired(e => e.Document)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Font>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<Font>()
                .Property(e => e.RelativePath)
                .IsUnicode(false);

            modelBuilder.Entity<Font>()
                .Property(e => e.CssFamily)
                .IsUnicode(false);

            modelBuilder.Entity<Font>()
                .Property(e => e.ResourceName)
                .IsUnicode(false);

            modelBuilder.Entity<Form>()
                .Property(e => e.DisplayName)
                .IsUnicode(false);

            modelBuilder.Entity<Form>()
                .Property(e => e.FormType)
                .IsUnicode(false);

            modelBuilder.Entity<Form>()
                .HasMany(e => e.Fields);

            modelBuilder.Entity<FormField>()
                .Property(e => e.FieldName)
                .IsUnicode(false);

            modelBuilder.Entity<FormField>()
                .Property(e => e.FieldValue)
                .IsUnicode(false);

            modelBuilder.Entity<FormTemplate>()
                .Property(e => e.DisplayName)
                .IsUnicode(false);

            modelBuilder.Entity<FormTemplate>()
                .Property(e => e.FormType)
                .IsUnicode(false);

            modelBuilder.Entity<FormTemplate>()
                .Property(e => e.Jurisdiction)
                .IsUnicode(false);
            
            modelBuilder.Entity<FormTemplate>()
                .HasMany(e => e.AcroFields)
                .WithRequired(e => e.FormTemplate)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<FormTemplate>()
                .HasMany(e => e.FieldMappings)
                .WithRequired(e => e.FormTemplate)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<FormTemplateAcroFieldInfo>()
                .Property(e => e.AcroFieldName)
                .IsUnicode(false);

            modelBuilder.Entity<FormTemplateAcroFieldInfo>()
                .Property(e => e.FieldTitle)
                .IsUnicode(false);

            modelBuilder.Entity<FormTemplateAcroFieldInfo>()
                .Property(e => e.FieldDescription)
                .IsUnicode(false);

            modelBuilder.Entity<FormTemplateAcroFieldInfo>()
                .Property(e => e.InputType)
                .IsUnicode(false);

            modelBuilder.Entity<FormTemplateAcroFieldInfo>()
                .HasMany(e => e.SelectionOptions)
                .WithRequired(e => e.FormTemplateAcroFieldInfo)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<FormTemplateAcroFieldSelectionOption>()
                .Property(e => e.DisplayName)
                .IsUnicode(false);

            modelBuilder.Entity<FormTemplateAcroFieldSelectionOption>()
                .Property(e => e.SelectionValue)
                .IsUnicode(false);

            modelBuilder.Entity<FormTemplateFieldMapping>()
                .Property(e => e.AcroFieldName)
                .IsUnicode(false);

            modelBuilder.Entity<FormTemplateFieldMapping>()
                .Property(e => e.SourceFieldName)
                .IsUnicode(false);

            modelBuilder.Entity<Person>()
                .Property(e => e.FirstName)
                .IsUnicode(false);

            modelBuilder.Entity<Person>()
                .Property(e => e.LastName)
                .IsUnicode(false);

            modelBuilder.Entity<Person>()
                .Property(e => e.Email)
                .IsUnicode(false);

            modelBuilder.Entity<Person>()
                .Property(e => e.MobileNumber)
                .IsUnicode(false);

            modelBuilder.Entity<Person>()
                .Property(e => e.Address1)
                .IsUnicode(false);

            modelBuilder.Entity<Person>()
                .Property(e => e.Address2)
                .IsUnicode(false);

            modelBuilder.Entity<Person>()
                .Property(e => e.City)
                .IsUnicode(false);

            modelBuilder.Entity<Person>()
                .Property(e => e.State)
                .IsUnicode(false);

            modelBuilder.Entity<Person>()
                .Property(e => e.ZipCode)
                .IsUnicode(false);

            modelBuilder.Entity<Person>()
                .Property(e => e.RoleName)
                .IsUnicode(false);

            modelBuilder.Entity<Person>()
                .HasMany(e => e.DealAccess)
                .WithRequired(e => e.Person)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Person>()
                .HasMany(e => e.SignatureRequest)
                .WithRequired(e => e.Signer)
                .HasForeignKey(e => e.SignerPersonID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<PersonRole>()
                .Property(e => e.RoleName)
                .IsUnicode(false);

            modelBuilder.Entity<PersonRole>()
                .Property(e => e.RoleDescription)
                .IsUnicode(false);

            modelBuilder.Entity<Property>()
                .Property(e => e.Address1)
                .IsUnicode(false);

            modelBuilder.Entity<Property>()
                .Property(e => e.Address2)
                .IsUnicode(false);

            modelBuilder.Entity<Property>()
                .Property(e => e.City)
                .IsUnicode(false);

            modelBuilder.Entity<Property>()
                .Property(e => e.State)
                .IsUnicode(false);

            modelBuilder.Entity<Property>()
                .Property(e => e.ZipCode)
                .IsUnicode(false);

            modelBuilder.Entity<Property>()
                .Property(e => e.LegalDescription)
                .IsUnicode(false);

            modelBuilder.Entity<Property>()
                .Property(e => e.TaxID)
                .IsUnicode(false);

            modelBuilder.Entity<Property>()
                .Property(e => e.County)
                .IsUnicode(false);

            modelBuilder.Entity<Signature>()
                .Property(e => e.FontName)
                .IsUnicode(false);

            modelBuilder.Entity<Signature>()
                .Property(e => e.SignatureText)
                .IsUnicode(false);

            modelBuilder.Entity<SignatureInfo>()
                .Property(e => e.RoleName)
                .IsUnicode(false);

            modelBuilder.Entity<SignatureInfo>()
                .Property(e => e.SignatureType)
                .IsUnicode(false);

            modelBuilder.Entity<SignatureInfo>()
                .HasMany(e => e.Signature)
                .WithRequired(e => e.SignatureInfo)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<SignatureRequest>()
                .Property(e => e.SignerEmail)
                .IsUnicode(false);

            modelBuilder.Entity<SignatureRequest>()
                .HasMany(e => e.Signatures)
                .WithRequired(e => e.SignatureRequest)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<SignatureType>()
                .Property(e => e.SignatureTypeName)
                .IsUnicode(false);

            modelBuilder.Entity<SignatureType>()
                .HasMany(e => e.SignatureInfo)
                .WithRequired(e => e.SignatureType1)
                .HasForeignKey(e => e.SignatureType)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<SupportedJurisdiction>()
                .Property(e => e.JurisdictionID)
                .IsUnicode(false);

            modelBuilder.Entity<SupportedJurisdiction>()
                .Property(e => e.DisplayName)
                .IsUnicode(false);

            modelBuilder.Entity<SupportedJurisdiction>()
                .HasMany(e => e.Deal)
                .WithRequired(e => e.SupportedJurisdiction)
                .HasForeignKey(e => e.Jurisdiction)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<SupportedJurisdiction>()
                .HasMany(e => e.FormTemplate)
                .WithRequired(e => e.SupportedJurisdiction)
                .HasForeignKey(e => e.Jurisdiction)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Task>()
                .Property(e => e.DisplayName)
                .IsUnicode(false);

            modelBuilder.Entity<TaskList>()
                .Property(e => e.DisplayName)
                .IsUnicode(false);

            modelBuilder.Entity<TaskList>()
                .HasMany(e => e.Tasks)
                .WithRequired(e => e.TaskList)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<TransactionStatus>()
                .Property(e => e.StatusName)
                .IsUnicode(false);

            modelBuilder.Entity<TransactionStatus>()
                .Property(e => e.StatusDescription)
                .IsUnicode(false);

            modelBuilder.Entity<TransactionStatus>()
                .HasMany(e => e.Deal)
                .WithRequired(e => e.TransactionStatus1)
                .HasForeignKey(e => e.TransactionStatus)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<TransactionType>()
                .Property(e => e.TypeName)
                .IsUnicode(false);

            modelBuilder.Entity<TransactionType>()
                .Property(e => e.TypeDescription)
                .IsUnicode(false);

            modelBuilder.Entity<TransactionType>()
                .HasMany(e => e.Deal)
                .WithRequired(e => e.TransactionType1)
                .HasForeignKey(e => e.TransactionType)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<UserProfile>()
                .Property(e => e.FirstName)
                .IsUnicode(false);

            modelBuilder.Entity<UserProfile>()
                .Property(e => e.LastName)
                .IsUnicode(false);

            modelBuilder.Entity<UserSignature>()
                .Property(e => e.SignatureText)
                .IsUnicode(false);

            modelBuilder.Entity<UserSignature>()
                .Property(e => e.InitialsText)
                .IsUnicode(false);

            modelBuilder.Entity<UserSignature>()
                .Property(e => e.SignatureFont)
                .IsUnicode(false);

            modelBuilder.Entity<UserSignature>()
                .Property(e => e.InitialsFont)
                .IsUnicode(false);

            modelBuilder.Entity<UserSubscription>()
                .Property(e => e.StripeToken)
                .IsUnicode(false);

            modelBuilder.Entity<AcroFieldPositionOnPageImage>()
                .Property(e => e.src)
                .IsUnicode(false);

            modelBuilder.Entity<DisplayNameLookup>()
                .Property(e => e.EntityType)
                .IsUnicode(false);

            modelBuilder.Entity<SignaturePosition>()
                .Property(e => e.SignatureType)
                .IsUnicode(false);

            modelBuilder.Entity<SignaturePosition>()
                .Property(e => e.RoleName)
                .IsUnicode(false);
        }
    }
}
