namespace Divinity.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("FormTemplateAcroFieldSelectionOption")]
    public partial class FormTemplateAcroFieldSelectionOption
    {
        public Guid FormTemplateAcroFieldSelectionOptionID { get; set; }

        public Guid FormTemplateAcroFieldInfoID { get; set; }

        public int? SortOrder { get; set; }

        [StringLength(250)]
        public string DisplayName { get; set; }

        [StringLength(250)]
        public string SelectionValue { get; set; }

        public virtual FormTemplateAcroFieldInfo FormTemplateAcroFieldInfo { get; set; }
    }
}
