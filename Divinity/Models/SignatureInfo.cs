namespace Divinity.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SignatureInfo")]
    public partial class SignatureInfo
    {
        public SignatureInfo()
        {
            Signature = new HashSet<Signature>();
        }

        public Guid SignatureInfoID { get; set; }

        [StringLength(50)]
        public string RoleName { get; set; }

        [Required]
        [StringLength(50)]
        public string SignatureType { get; set; }

        public Guid DocumentID { get; set; }

        public int X { get; set; }

        public int Y { get; set; }

        public int Width { get; set; }

        public int Height { get; set; }

        public bool HasDate { get; set; }

        public int? DateX { get; set; }

        public int? DateY { get; set; }

        public int? DateWidth { get; set; }

        public int? DateHeight { get; set; }

        public int PageNumber { get; set; }

        public virtual Document Document { get; set; }

        public virtual PersonRole PersonRole { get; set; }

        public virtual ICollection<Signature> Signature { get; set; }

        public virtual SignatureType SignatureType1 { get; set; }
    }
}
