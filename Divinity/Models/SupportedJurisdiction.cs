namespace Divinity.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SupportedJurisdiction")]
    public partial class SupportedJurisdiction
    {
        public SupportedJurisdiction()
        {
            Deal = new HashSet<Deal>();
            FormTemplate = new HashSet<FormTemplate>();
        }

        [Key]
        [StringLength(50)]
        public string JurisdictionID { get; set; }

        [Required]
        [StringLength(250)]
        public string DisplayName { get; set; }

        public virtual ICollection<Deal> Deal { get; set; }

        public virtual ICollection<FormTemplate> FormTemplate { get; set; }
    }
}
