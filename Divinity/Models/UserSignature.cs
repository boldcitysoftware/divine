namespace Divinity.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("UserSignature")]
    public partial class UserSignature
    {
        [Key]
        public string UserID { get; set; }

        [Required]
        [StringLength(200)]
        public string SignatureText { get; set; }

        [Required]
        [StringLength(10)]
        public string InitialsText { get; set; }

        [Required]
        [StringLength(100)]
        public string SignatureFont { get; set; }

        [Required]
        [StringLength(100)]
        public string InitialsFont { get; set; }
    }
}
