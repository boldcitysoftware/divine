namespace Divinity.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("TaskList")]
    public partial class TaskList
    {
        public TaskList()
        {
            Tasks = new HashSet<Task>();
        }

        public Guid TaskListID { get; set; }

        public Guid DealID { get; set; }

        [Required]
        [StringLength(150)]
        public string DisplayName { get; set; }

        public DateTimeOffset CreatedAt { get; set; }

        public DateTimeOffset UpdatedAt { get; set; }

        public virtual Deal Deal { get; set; }

        public virtual ICollection<Task> Tasks { get; set; }
    }
}
