﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace Divinity.Models.Helpers {
    public static class SequentialGuidCreator {

        private class NativeMethods {
            [DllImport("rpcrt4.dll", SetLastError = true)]
            public static extern int UuidCreateSequential(out Guid guid);
        }
        const int RPC_S_OK = 0;
        public static Guid Create() {

            Guid guid;
            int result = NativeMethods.UuidCreateSequential(out guid);
            if (result == RPC_S_OK) {
                return guid;
            } else {
                throw new Exception("CreateGuid() RPC_S_OK was not ok!");
            }
        }
    }
}