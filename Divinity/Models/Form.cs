namespace Divinity.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Form")]
    public partial class Form
    {
        public Form()
        {
            Fields = new HashSet<FormField>();
        }

        public Guid FormID { get; set; }

        public Guid? FormTemplateID { get; set; }

        public Guid? DealID { get; set; }

        [Required]
        [StringLength(150)]
        public string DisplayName { get; set; }

        [Required]
        [StringLength(100)]
        public string FormType { get; set; }

        public DateTimeOffset CreatedAt { get; set; }

        public DateTimeOffset UpdatedAt { get; set; }

        public Guid? DocumentID { get; set; }

        public virtual Deal Deal { get; set; }

        public virtual Document Document { get; set; }

        public virtual FormTemplate FormTemplate { get; set; }

        public virtual ICollection<FormField> Fields { get; set; }
    }
}
