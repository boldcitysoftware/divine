namespace Divinity.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Signature")]
    public partial class Signature
    {
        public Guid SignatureID { get; set; }

        public Guid RequestID { get; set; }

        public Guid SignatureInfoID { get; set; }

        [Required]
        [StringLength(100)]
        public string FontName { get; set; }

        [Required]
        [StringLength(256)]
        public string SignatureText { get; set; }

        public DateTimeOffset ExecutedAt { get; set; }

        public Guid DealID { get; set; }

        public virtual Deal Deal { get; set; }

        public virtual Font Font { get; set; }

        public virtual SignatureRequest SignatureRequest { get; set; }

        public virtual SignatureInfo SignatureInfo { get; set; }
    }
}
