namespace Divinity.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("FormTemplateAcroFieldInfo")]
    public partial class FormTemplateAcroFieldInfo
    {
        public FormTemplateAcroFieldInfo()
        {
            SelectionOptions = new HashSet<FormTemplateAcroFieldSelectionOption>();
        }

        public Guid FormTemplateAcroFieldInfoID { get; set; }

        [Required]
        [StringLength(150)]
        public string AcroFieldName { get; set; }

        public Guid FormTemplateID { get; set; }

        public string FieldTitle { get; set; }

        public string FieldDescription { get; set; }

        [StringLength(100)]
        public string InputType { get; set; }

        public bool UseDefaultDisplayInfoIfMapped { get; set; }

        public int TabOrder { get; set; }

        public int PageNumber { get; set; }

        public int LeftX { get; set; }

        public int BottomY { get; set; }

        public int Width { get; set; }

        public int Height { get; set; }

        public bool Editable { get; set; }

        public bool Visible { get; set; }

        public virtual FormTemplate FormTemplate { get; set; }

        public virtual ICollection<FormTemplateAcroFieldSelectionOption> SelectionOptions { get; set; }
    }
}
