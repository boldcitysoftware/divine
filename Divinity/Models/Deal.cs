namespace Divinity.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Deal")]
    public partial class Deal
    {
        public Deal()
        {
            AccessGrants = new HashSet<DealAccess>();
            Documents = new HashSet<Document>();
            Forms = new HashSet<Form>();
            People = new HashSet<Person>();
            Properties = new HashSet<Property>();
            Signatures = new HashSet<Signature>();
            SignatureRequests = new HashSet<SignatureRequest>();
            TaskLists = new HashSet<TaskList>();
        }

        public Guid DealID { get; set; }

        [StringLength(128)]
        public string UserID { get; set; }

        [Required]
        [StringLength(150)]
        public string DisplayName { get; set; }

        public DateTimeOffset CreatedAt { get; set; }

        public DateTimeOffset UpdatedAt { get; set; }

        [Required]
        [StringLength(50)]
        public string Jurisdiction { get; set; }

        [Required]
        [StringLength(50)]
        public string TransactionStatus { get; set; }

        [Required]
        [StringLength(50)]
        public string TransactionType { get; set; }

        public virtual SupportedJurisdiction SupportedJurisdiction { get; set; }

        public virtual TransactionStatus TransactionStatus1 { get; set; }

        public virtual TransactionType TransactionType1 { get; set; }

        public virtual ICollection<DealAccess> AccessGrants { get; set; }

        public virtual ICollection<Document> Documents { get; set; }

        public virtual ICollection<Form> Forms { get; set; }

        public virtual ICollection<Person> People { get; set; }

        public virtual ICollection<Property> Properties { get; set; }

        public virtual ICollection<Signature> Signatures { get; set; }

        public virtual ICollection<SignatureRequest> SignatureRequests { get; set; }

        public virtual ICollection<TaskList> TaskLists { get; set; }
    }
}
