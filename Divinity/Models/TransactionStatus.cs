namespace Divinity.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class TransactionStatus
    {
        public TransactionStatus()
        {
            Deal = new HashSet<Deal>();
        }

        [Key]
        [StringLength(50)]
        public string StatusName { get; set; }

        [Required]
        public string StatusDescription { get; set; }

        public virtual ICollection<Deal> Deal { get; set; }
    }
}
