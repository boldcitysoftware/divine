namespace Divinity.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Person")]
    public partial class Person
    {
        public Person()
        {
            DealAccess = new HashSet<DealAccess>();
            SignatureRequest = new HashSet<SignatureRequest>();
        }

        public Guid PersonID { get; set; }

        [StringLength(100)]
        public string FirstName { get; set; }

        [Required]
        [StringLength(100)]
        public string LastName { get; set; }

        [StringLength(250)]
        public string Email { get; set; }

        [StringLength(20)]
        public string MobileNumber { get; set; }

        [StringLength(100)]
        public string Address1 { get; set; }

        [StringLength(100)]
        public string Address2 { get; set; }

        [StringLength(100)]
        public string City { get; set; }

        [StringLength(50)]
        public string State { get; set; }

        [StringLength(10)]
        public string ZipCode { get; set; }

        public DateTimeOffset CreatedAt { get; set; }

        public DateTimeOffset UpdatedAt { get; set; }

        public Guid? DealID { get; set; }

        [StringLength(50)]
        public string RoleName { get; set; }

        public virtual Deal Deal { get; set; }

        public virtual ICollection<DealAccess> DealAccess { get; set; }

        public virtual PersonRole PersonRole { get; set; }

        public virtual ICollection<SignatureRequest> SignatureRequest { get; set; }
    }
}
