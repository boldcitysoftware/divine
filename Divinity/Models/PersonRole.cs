namespace Divinity.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("PersonRole")]
    public partial class PersonRole
    {
        public PersonRole()
        {
            Person = new HashSet<Person>();
            SignatureInfo = new HashSet<SignatureInfo>();
        }

        [Key]
        [StringLength(50)]
        public string RoleName { get; set; }

        [Required]
        [StringLength(500)]
        public string RoleDescription { get; set; }

        public virtual ICollection<Person> Person { get; set; }

        public virtual ICollection<SignatureInfo> SignatureInfo { get; set; }
    }
}
