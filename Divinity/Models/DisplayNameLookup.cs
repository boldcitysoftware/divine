namespace Divinity.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("DisplayNameLookup")]
    public partial class DisplayNameLookup
    {
        public Guid ID { get; set; }

        [StringLength(256)]
        public string DisplayName { get; set; }

        [StringLength(169)]
        public string EntityType { get; set; }
    }
}
