namespace Divinity.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("UserSubscription")]
    public partial class UserSubscription
    {
        [Key]
        public string UserID { get; set; }

        public int FreeDeals { get; set; }

        [StringLength(128)]
        public string StripeToken { get; set; }
    }
}
