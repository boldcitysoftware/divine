namespace Divinity.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SignatureType")]
    public partial class SignatureType
    {
        public SignatureType()
        {
            SignatureInfo = new HashSet<SignatureInfo>();
        }

        [Key]
        [StringLength(50)]
        public string SignatureTypeName { get; set; }

        public virtual ICollection<SignatureInfo> SignatureInfo { get; set; }
    }
}
