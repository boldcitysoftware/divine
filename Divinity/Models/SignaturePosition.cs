namespace Divinity.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SignaturePosition")]
    public partial class SignaturePosition
    {
        [Key]
        [Column(Order = 0)]
        public Guid SignatureInfoID { get; set; }

        [Key]
        [Column(Order = 1)]
        public Guid DocumentID { get; set; }

        [Key]
        [Column(Order = 2)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int FieldY { get; set; }

        [Key]
        [Column(Order = 3)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int FieldX { get; set; }

        [Key]
        [Column(Order = 4)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int FieldWidth { get; set; }

        [Key]
        [Column(Order = 5)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int FieldHeight { get; set; }

        [Key]
        [Column(Order = 6)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int PageWidth { get; set; }

        [Key]
        [Column(Order = 7)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int PageHeight { get; set; }

        [Key]
        [Column(Order = 8)]
        [StringLength(50)]
        public string SignatureType { get; set; }

        [StringLength(50)]
        public string RoleName { get; set; }

        [Key]
        [Column(Order = 9)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int PageNumber { get; set; }

        [Key]
        [Column(Order = 10)]
        public bool HasDate { get; set; }

        public int? DateFieldY { get; set; }

        public int? DateFieldX { get; set; }

        public int? DateFieldWidth { get; set; }

        public int? DateFieldHeight { get; set; }
    }
}
