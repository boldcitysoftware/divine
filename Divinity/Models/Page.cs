namespace Divinity.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Page")]
    public partial class Page
    {
        public Guid PageID { get; set; }

        public Guid DocumentID { get; set; }

        public int PageNumber { get; set; }

        public int PageWidth { get; set; }

        public int PageHeight { get; set; }

        public virtual Document Document { get; set; }
    }
}
