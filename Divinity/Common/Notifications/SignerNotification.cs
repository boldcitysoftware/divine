﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using Divinity.Models;
using SendGrid;
using SmartFormat;

namespace Divinity.Common.Notifications {
    public class SignerNotification {

        public SignatureRequest Request { get; set; }
        public AspNetUser Sender { get; set; }
        public UserProfile SenderProfile { get; set; }
        
        public SignerNotification(SignatureRequest request, Document document, DivinityContext db) {
            this.Request = request;
            this.Document = document;
            Sender = db.AspNetUsers.Find(request.RequesterUserID);
            SenderProfile = db.UserProfile.Find(request.RequesterUserID);
        }

        public async System.Threading.Tasks.Task SendNotificationAsync() {
            var myMessage = new SendGridMessage();
            myMessage.ReplyTo = new MailAddress[] { new MailAddress(Sender.Email, SenderProfile.FirstName + " " + SenderProfile.LastName) };
            myMessage.To = new MailAddress[] { new MailAddress(Request.SignerEmail, Request.Signer.FirstName + " " + Request.Signer.LastName) };
            myMessage.From = new System.Net.Mail.MailAddress(
                                "notifications@divinity.io", "Divine Real Estate Solutions");
            myMessage.Subject = String.Format("Please sign this document: {0}", Document.DisplayName);

            var formatObj = new { 
                RecipientFirstName = Request.Signer.FirstName,
                SenderFirstName = SenderProfile.FirstName,
                SenderLastName = SenderProfile.LastName,
                SenderEmail = Sender.Email,
                SignerUrl = String.Format("https://deals.divinity.io/#/secure-sign/{0}", Request.RequestID)
            };
            myMessage.Text = Smart.Format(
@"{RecipientFirstName}, 

{SenderFirstName} {SenderLastName} ({SenderEmail}) sent you a document to review and sign. Please visit the secure address below to get started.

{SignerUrl}", formatObj);
            myMessage.Html = Smart.Format(
@"{RecipientFirstName}, 
<br/><br/>
{SenderFirstName} {SenderLastName} ({SenderEmail}) sent you a document to review and sign. Please visit the secure address below to get started.
<br/><br/>
<a href=""{SignerUrl}"">Secure Link</a>", formatObj);

            var credentials = new NetworkCredential(System.Configuration.ConfigurationManager.AppSettings["SendGridUserName"], System.Configuration.ConfigurationManager.AppSettings["SendGridPassword"]);

            // Create a Web transport for sending email.
            var transportWeb = new Web(credentials);

            // Send the email.
            if (transportWeb != null) {
                await transportWeb.DeliverAsync(myMessage);
            }
        }

        public Document Document { get; set; }
    }
}
