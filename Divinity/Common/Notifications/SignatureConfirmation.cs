﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using Divinity.Models;
using SendGrid;
using SmartFormat;

namespace Divinity.Common.Notifications {
    public class SignatureConfirmation {
        public SignatureRequest Request { get; set; }
        public AspNetUser Sender { get; set; }
        public UserProfile SenderProfile { get; set; }
        public Document Document{ get; set; }

        public SignatureConfirmation(SignatureRequest request, DivinityContext db) {
            this.Request = request;
            Sender = db.AspNetUsers.Find(request.RequesterUserID);
            SenderProfile = db.UserProfile.Find(request.RequesterUserID);
            Document = db.Document.Find(request.DocumentID);
        }

        public async System.Threading.Tasks.Task SendNotificationAsync() {
            var myMessage = new SendGridMessage();
            myMessage.To = new MailAddress[] { new MailAddress(Sender.Email, SenderProfile.FirstName + " " + SenderProfile.LastName) };
            myMessage.From = new System.Net.Mail.MailAddress(
                                "notifications@divinity.io", "Divine Real Estate Solutions");
            myMessage.Subject = String.Format("Signature Confirmed: {0}", Document.DisplayName);
            var formatObj = new { 
                RecipientFirstName = Request.Signer.FirstName,
                RecipientLastName = Request.Signer.LastName,
                RecipientEmail = Request.SignerEmail,
                SenderFirstName = SenderProfile.FirstName,
                FormUrl = String.Format("https://deals.divinity.io/#/deals/{0}/documents/{1}", Document.DealID, Document.DocumentID),
                FormName = Document.DisplayName
            };
            myMessage.Text = Smart.Format(
@"{SenderFirstName}, 

This email is being sent to notify you that {RecipientFirstName} {RecipientLastName} ({RecipientEmail}) has executed a signature on the form {FormName}. You can review the form details by visiting the link below.

{FormUrl}", formatObj);
            myMessage.Html = Smart.Format(
@"{SenderFirstName}, 
<br/><br/>
This email is being sent to notify you that <strong>{RecipientFirstName} {RecipientLastName} ({RecipientEmail})</strong> has executed a signature on the form <strong>{FormName}</strong>. You can review the form details by visiting the link below.
<br/><br/>
<a href=""{FormUrl}"">View Form Details</a>", formatObj);

            var credentials = new NetworkCredential(System.Configuration.ConfigurationManager.AppSettings["SendGridUserName"], System.Configuration.ConfigurationManager.AppSettings["SendGridPassword"]);

            // Create a Web transport for sending email.
            var transportWeb = new Web(credentials);

            // Send the email.
            if (transportWeb != null) {
                await transportWeb.DeliverAsync(myMessage);
            }
        }
    }
}
