﻿using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using Divinity.Models;
using SendGrid;
using SmartFormat;

namespace Divinity.Common.Notifications {
    public class FormFinalizationNotification {
        private DivinityContext _db;

        public FormFinalizationNotification(Guid formID, DivinityContext db) {
            this.FormID = formID;
            this._db = db;
        }

        public async System.Threading.Tasks.Task SendNotificationAsync() {
            var form = await _db.Form
                .Include(f => f.Document.SignatureRequests.Select(r => r.Signer))
                .SingleOrDefaultAsync(f => f.FormID == FormID);
            var deal = await _db.Deal.Include(d => d.People).SingleOrDefaultAsync(d=>d.DealID == form.DealID);
            List<MailAddress> recipients = new List<MailAddress>();
            foreach (var person in deal.People) {
                if (!String.IsNullOrWhiteSpace(person.Email)) {
                    recipients.Add(new MailAddress(person.Email, person.FirstName + " " + person.LastName));
                }
            }
            foreach (var request in form.Document.SignatureRequests) {
                if (!recipients.Any(r => r.Address.ToLower() == request.SignerEmail.ToLower())) {
                    recipients.Add(new MailAddress(request.SignerEmail, request.Signer.FirstName + " " + request.Signer.LastName));
                }
            }

            var credentials = new NetworkCredential(System.Configuration.ConfigurationManager.AppSettings["SendGridUserName"], System.Configuration.ConfigurationManager.AppSettings["SendGridPassword"]);

            // Create a Web transport for sending email.
            var transportWeb = new Web(credentials);

            foreach (var recipient in recipients) {
                var myMessage = new SendGridMessage();
                myMessage.To = new MailAddress[] { recipient };
                myMessage.From = new MailAddress("notifications@divinity.io", "Divine Real Estate Solutions");
                myMessage.ReplyTo = new MailAddress[] { new MailAddress("support@divineres.com", "Divine Forms") };
                myMessage.Subject = String.Format("Form Finalized: {0}", form.DisplayName);

                myMessage.Text = Smart.Format(
@"The form '{FormDisplayName}' has been finalized. You may access it at the link below. If you have any questions, please contact your real estate professional.

{FormUrl}", new { FormDisplayName = form.DisplayName, FormUrl = Smart.Format("https://deals.divinity.io/api/downloadform/{FormID}", form) });

                myMessage.Html = Smart.Format(
@"The form '{FormDisplayName}' has been finalized. You may access it at the link below. If you have any questions, please contact your real estate professional.
<br/><br/>
<a href=""{FormUrl}"">Download {FormDisplayName}</a>", new { FormDisplayName = form.DisplayName, FormUrl = Smart.Format("https://deals.divinity.io/api/downloadform/{FormID}", form) });


                // Send the email.
                if (transportWeb != null) {
                    await transportWeb.DeliverAsync(myMessage);
                }
            }
        }

        public Guid FormID { get; set; }
    }
}
