﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using Divinity.Models;
using Microsoft.AspNet.Identity;

namespace Divinity.Common.BaseClasses {
    public abstract class UserController : ApiController {
        public UserController() {
            db.Configuration.ProxyCreationEnabled = false;
            db.Configuration.LazyLoadingEnabled = false;
        }
        
        protected DivinityContext db = new DivinityContext();

        protected string UserID {
            get {
                return User.Identity.GetUserId();
            }
        }
        protected string UserEmail {
            get {
                return User.Identity.GetUserName();
            }
        }

        protected override void Dispose(bool disposing) {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}