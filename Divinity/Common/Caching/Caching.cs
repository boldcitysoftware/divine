﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Caching;
using System.Web.Hosting;

namespace Divinity.Common {
    public static class Caching {
        public static T Get<T>(string key, Func<T> getFunc) {
            var cache = HttpContext.Current.Cache;
            if (cache == null){
                return getFunc();
            }

            object value = cache.Get(key);
            if (!(value is T)) {
                value = getFunc();   
            }

            return (T)value;
        }

        public static TResult Get<TArg1, TResult>(string key, TArg1 arg1, Func<TArg1, TResult> getFunc) {
            var cache = HttpContext.Current.Cache;
            if (cache == null) {
                return getFunc(arg1);
            }

            object value = cache.Get(key);
            if (!(value is TResult)) {
                value = getFunc(arg1);
            }

            return (TResult)value;
        }

        public static void Remove(string key) {
            var cache = HttpContext.Current.Cache;
            if (cache != null) {
                cache.Remove(key);
            }
        }

        public static async Task<T> GetAsync<T>(string key, Func<Task<T>> getFunc) {
            var cache = HttpContext.Current.Cache;
            if (cache == null) {
                return await getFunc();
            }

            object value = cache.Get(key);
            if (!(value is T)) {
                value = await getFunc();
                cache.Add(key, value, null, Cache.NoAbsoluteExpiration, new TimeSpan(1, 0, 0), CacheItemPriority.Normal, null);
            }

            return (T)value;
        }

        public static async Task<TResult> GetAsync<TArg1, TResult>(string key, TArg1 arg1, Func<TArg1, Task<TResult>> getFunc) {
            var cache = HttpContext.Current.Cache;
            if (cache == null) {
                return await getFunc(arg1);
            }

            object value = cache.Get(key);
            if (!(value is TResult)) {
                value = await getFunc(arg1);
                cache.Add(key, value, null, Cache.NoAbsoluteExpiration, new TimeSpan(1, 0, 0), CacheItemPriority.Normal, null);
            }

            return (TResult)value;
        }
    }
}
