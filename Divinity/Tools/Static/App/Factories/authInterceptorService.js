﻿'use strict';
app.factory('authInterceptorService', ['$q', '$location', 'localStorageService', 'flash', function ($q, $location, localStorageService, flash) {

    var authInterceptorServiceFactory = {};

    var _request = function (config) {

        config.headers = config.headers || {};

        var authData = localStorageService.get('authorizationData');
        if (authData) {
            config.headers.Authorization = 'Bearer ' + authData.token;
        }

        return config;
    }

    var _responseError = function (rejection) {
        
        if (rejection.status === 401) {
            flash.warn = 'Please sign in to continue.';
            $location.path('/login');
        } else if (rejection.status >= 500) {
            flash.warn = 'Connection error. Please try again in a few moments.';
        }

        return $q.reject(rejection);
    }

    authInterceptorServiceFactory.request = _request;
    authInterceptorServiceFactory.responseError = _responseError;

    return authInterceptorServiceFactory;
}]);