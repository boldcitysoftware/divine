﻿angular.module('models', []).factory('models', ['apiService', function (apiService) {
    var models = {};

    // begin Example
    models.Example = function (extender) {
        this.exampleID = '00000000-0000-0000-0000-000000000000';
            
        angular.extend(this, extender);
    };

    models.Example.entity = function () { return "examples"; };

    models.Example.query = function (params) { return apiService.query(models.Example.entity(), models.Example, params, true); };

    models.Example.prototype.save = function () {
        if (this.exampleID && this.exampleID !== '00000000-0000-0000-0000-000000000000') {
            return apiService.update(models.Example.entity(), this.exampleID, this);
        } else {
            return apiService.insert(models.Example.entity(), this);
        }
    };

    models.Example.prototype.destroy = function () { return apiService.destroy(models.Example.entity(), this.exampleID); };
    // end Example

    // begin FormTemplate
    models.FormTemplate = function (extender) {
        this.formTemplateID = '00000000-0000-0000-0000-000000000000';
        this.displayName = '';
        this.formType = '';
        this.jurisdiction = '';
        angular.extend(this, extender);
    };

    models.FormTemplate.entity = function () { return "formTemplates"; };

    models.FormTemplate.query = function (params) { return apiService.query(models.FormTemplate.entity(), models.FormTemplate, params); };

    models.FormTemplate.prototype.save = function () {
        if (this.formTemplateID && this.formTemplateID !== '00000000-0000-0000-0000-000000000000') {
            return apiService.update(models.FormTemplate.entity(), this.formTemplateID, this);
        } else {
            return apiService.insert(models.FormTemplate.entity(), this);
        }
    };

    models.FormTemplate.prototype.destroy = function () { return apiService.destroy(models.FormTemplate.entity(), this.formTemplateID); };
    // end FormTemplate

    // begin PersonRole
    models.PersonRole = function (extender) {
        this.roleName = '';
        this.roleDescription = '';

        angular.extend(this, extender);
    };

    models.PersonRole.entity = function () { return "PersonRoles"; };

    models.PersonRole.query = function (params) {
        params = params || {};
        return apiService.query(models.PersonRole.entity(), models.PersonRole, params, !params.id);
    };

    // end PersonRole

    // begin FieldPreview
    models.FieldPreview = function (extender) {
        this.formTemplateAcroFieldInfoID = '00000000-0000-0000-0000-000000000000';
        this.src = '';
        this.fieldY = 0;
        this.fieldX = 0;
        this.fieldWidth = 0;
        this.fieldHeight = 0;
        this.imgWidth = 0;
        this.imgHeight = 0;

        angular.extend(this, extender);
    };
    models.FieldPreview.prototype.getBottomOffset = function () {
        return this.imgHeight - (this.fieldY) - (this.fieldHeight * 5);
    };
    models.FieldPreview.prototype.getLeftOffset = function () {
        return (this.fieldX);
    };
    models.FieldPreview.prototype.getBorderBoxStyle = function () {
        var height = ((this.fieldHeight * 10) + 18) + 'px';
        var width = (this.imgWidth + 20);
        width = width + 'px';

        return {
            border: '1px solid #777',
            height: height,
            width: width,
            padding: '9px 10px',
            background:'white'
        };
    };
    models.FieldPreview.prototype.getContainerStyle = function () {
        var height = (this.fieldHeight * 10) + 'px';
        var width = (this.imgWidth);
        width = width + 'px';

        return {
            height:height,
            width: width,
            overflow: 'hidden',
            position: 'relative'
        };
    };
    models.FieldPreview.prototype.getImageStyle = function () {
        var bottom = this.getBottomOffset() + 'px';
        var width = this.imgWidth + 'px';

        return {
            position: 'relative',
            bottom: bottom,
            width: width
        };
    };
    models.FieldPreview.prototype.getHighlightStyle = function () {
        var width = this.fieldWidth + 'px';
        var height = this.fieldHeight + 'px';
        var marginTop = (this.fieldHeight * 5) + 'px';

        var leftOffsetAdj = this.fieldX;

        leftOffsetAdj = leftOffsetAdj + 'px';

        return {
            'line-height': height,
            'font-size': height,
            width: width,
            height: height,
            position: 'absolute',
            overflow: 'hidden',
            top: 0,
            background: '#FFC',
            'margin-top': marginTop,
            'margin-left': leftOffsetAdj
        };
    };

    models.FieldPreview.entity = function () { return "FieldPreview"; };

    models.FieldPreview.query = function (params) { return apiService.query(models.FieldPreview.entity(), models.FieldPreview, params); };
    // end FieldPreview

    // begin SignatureInfo
    models.SignatureInfo = function (extender) {
        this.SignatureInfoID = '00000000-0000-0000-0000-000000000000';
        this.formTemplateID = '00000000-0000-0000-0000-000000000000';
        this.roleName = '';
        this.signatureType = 'Signature';

        this.x = 50;
        this.y = 50;
        this.width = 200;
        this.height = 16;

        this.pageNumber = 1;

        this.hasDate = false;
        this.dateX = '';
        this.dateY = '';
        this.dateWidth = '';
        this.dateHeight = '';
        
        angular.extend(this, extender);
    };
    // end SignatureInfo
        
    // begin FormTemplateFieldMapping
    models.FormTemplateFieldMapping = function (extender) {
        this.fieldMappingID = '00000000-0000-0000-0000-000000000000';
        this.formTemplateID = '00000000-0000-0000-0000-000000000000';
        this.acroFieldName = '';
        this.sourceFieldName = '';

        angular.extend(this, extender);
    };
    // end FormTemplateFieldMapping

    // begin FormTemplateAcroFieldInfo
    models.FormTemplateAcroFieldInfo = function (extender) {

        this.formTemplateAcroFieldInfoID = '00000000-0000-0000-0000-000000000000';
        this.acroFieldName = '';
        this.formTemplateID = '00000000-0000-0000-0000-000000000000';
        this.fieldTitle = '';
        this.fieldDescription = '';
        this.inputType = '';
        this.useDefaultDisplayInfoIfMapped = true;
        this.tabOrder = 0;
        this.pageNumber = 1;
        this.leftX = 0;
        this.bottomY = 0;
        this.width = 0;
        this.height = 0;
        this.editable = true;
        this.visible = true;

        angular.extend(this, extender);
    };
    models.FormTemplateAcroFieldInfo.entity = function () { return "FormTemplateFields"; };
    models.FormTemplateAcroFieldInfo.prototype.destroy = function () { return apiService.destroy(models.FormTemplateAcroFieldInfo.entity(), this.formTemplateAcroFieldInfoID); };
    // end FormTemplateAcroFieldInfo

    // begin FormTemplateAcroFieldSelectionOption
    models.FormTemplateAcroFieldSelectionOption = function (extender) {

        this.formTemplateAcroFieldSelectionOptionID = '00000000-0000-0000-0000-000000000000';
        this.formTemplateAcroFieldInfoID = '00000000-0000-0000-0000-000000000000';
        this.sortOrder = 0;
        this.displayName = '';
        this.selectionValue = '';

        angular.extend(this, extender);
    };
    // end FormTemplateAcroFieldSelectionOption

    return models;
}]);