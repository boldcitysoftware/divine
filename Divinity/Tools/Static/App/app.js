﻿// Main configuration file. Sets up AngularJS module and routes and any other config objects

var app = angular.module('main', ['flow', 'ui.bootstrap', 'angular-flash.service', 'angular-flash.flash-alert-directive', 'ngRoute', 'ui.grid', 'ngResource', 'LocalStorageModule', 'models', 'context']);     //Define the main module

app.config(['$routeProvider', 'flashProvider', 'flowFactoryProvider', '$httpProvider', function ($routeProvider, flashProvider, flowFactoryProvider, $httpProvider) {
    //Setup routes to load partial templates from server. TemplateUrl is the location for the server view (Razor .cshtml view)
    $routeProvider
        .when('/', { templateUrl: '/static/app/views/login.min.html', controller: 'loginController' })
        .when('/templates', { templateUrl: '/static/app/views/templates.min.html', controller: 'templatesController' })
        .when('/templates/:formTemplateID', { templateUrl: '/static/app/views/template.min.html', controller: 'templateController' })
        .when('/templates/:formTemplateID/fields', { templateUrl: '/static/app/views/template-fields.min.html', controller: 'templateFieldsController' })
        .when('/templates/:formTemplateID/signatures', { templateUrl: '/static/app/views/template-signatures.min.html', controller: 'templateSignaturesController' })
        .when('/new-template', { templateUrl: '/static/app/views/new-template.min.html', controller: 'newTemplateController' })
        .when('/new-template/:displayName/:formType/:jurisdiction', { templateUrl: '/static/app/views/new-template-2.min.html', controller: 'newTemplate2Controller' })
        .when('/login', { templateUrl: '/static/app/views/login.min.html', controller: 'loginController' })
        .otherwise({ redirectTo: '/' });

    // setup flash
    flashProvider.errorClassnames.push('alert-danger');
    flashProvider.warnClassnames.push('alert-warning');
    flashProvider.infoClassnames.push('alert-info');
    flashProvider.successClassnames.push('alert-success');

    flowFactoryProvider.defaults = {
        target: '/api/templateupload',
        permanentErrors: [401, 403, 404, 500, 501]
    };

    $httpProvider.interceptors.push('authInterceptorService');
}]);

app.run(['$location', '$rootScope', 'models', '$q', 'authService', function ($location, $rootScope, models, $q, authService) {
    authService.fillAuthData();

    $rootScope.utils = {
        stringToNumber: function (text) {
            if (this == "") {
                return text;
            }
            var transformedInput = text.toString().replace(/[^0-9]/g, '');

            return transformedInput;
        },
        stringToDecimal: function (text) {
            if (this == "") {
                return text;
            }
            var transformedInput = text.toString().replace(/\$|[^0-9\.\$\-]/g, '');

            transformedInput = parseFloat(Math.round(parseFloat(transformedInput) * 100) / 100).toFixed(2);

            return transformedInput;
        },
        roundUp: function (num) {
            var int = parseInt(num);
            var float = parseFloat(num);

            if (parseFloat(int) === float) {
                // they gave me an integral
                return int;
            } else {
                // floating point detected! add one.
                return int + 1;
            }
        },
        mmddyyyy: function (date) {
            var yyyy = date.getFullYear().toString();
            var mm = (date.getMonth() + 1).toString(); // getMonth() is zero-based
            var dd = date.getDate().toString();
            return (mm[1] ? mm : "0" + mm[0]) + "/" + (dd[1] ? dd : "0" + dd[0]) + "/" + yyyy; // padding
        }
    };
}])

app.controller('RootController', ['$scope', '$location', 'authService','models', '$window', function ($scope, $location, authService,models,$window) {
    $scope.go = function (route, params) {
        $location.path(route).search(params || {});
    };

    $scope.$back = function () {
        $window.history.back();
    };

    $scope.$on('$routeChangeSuccess', function (e, current, previous) {
        $scope.activeViewPath = $location.path();
    });
    $scope.logOut = function () {
        authService.logOut();
        $location.path('/login');
    }

    $scope.defaultGridOptions = {
        multiSelect: false,
        enableSorting: true,
        enableRowSelection: false,
        enableColumnResize: true,
        showColumnMenu: true,
        enableFiltering: true
    };

    $scope.defaultGridScope = {
        go: $scope.go
    };

    $scope.authentication = authService.authentication;

    var init = function () {
        if (!$scope.authentication.isAuth) {
            $scope.go('/login');
        }
    };
    init();
}]);
app.directive('ngConfirmClick', [
  function () {
      return {
          priority: -1,
          restrict: 'A',
          link: function (scope, element, attrs) {
              element.bind('click', function (e) {
                  var message = attrs.ngConfirmClick;
                  if (message && !confirm(message)) {
                      e.stopImmediatePropagation();
                      e.preventDefault();
                  }
              });
          }
      }
  }
]);
app.directive('decimalInput', function () {
    return {
        require: 'ngModel',
        link: function (scope, element, attr, ngModelCtrl) {
            element.css('text-align', 'right');
            var fromUser = function (text) {
                var transformedInput = scope.utils.stringToDecimal(text);

                if (transformedInput !== text) {
                    ngModelCtrl.$setViewValue(transformedInput);
                    ngModelCtrl.$render();
                }
                return transformedInput; // or return Number(transformedInput)
            };
            ngModelCtrl.$parsers.push(fromUser);
        }
    };
});

app.directive('numericInput', function () {
    return {
        require: 'ngModel',
        link: function (scope, element, attr, ngModelCtrl) {
            //element.css('text-align', 'right');
            var fromUser = function (text) {
                var transformedInput = scope.utils.stringToNumber(text);

                if (transformedInput !== text) {
                    ngModelCtrl.$setViewValue(transformedInput);
                    ngModelCtrl.$render();
                }
                return transformedInput; // or return Number(transformedInput)
            };
            ngModelCtrl.$parsers.push(fromUser);
        }
    };
});

app.directive('signatureBox', function () {
    return {
        // A = attribute, E = Element, C = Class and M = HTML Comment
        restrict: 'A',
        link: function (scope, element, attrs) {
            $(element.context).draggable({
                containment: 'parent',
                opacity:.7,
                stop: scope.moved
            });
            $(element.context).resizable({
                containment: 'parent',
                handles: 'ne, se, sw, nw',
                stop:scope.resized
            });
        }
    }
});

app.directive('dateBox', function () {
    return {
        // A = attribute, E = Element, C = Class and M = HTML Comment
        restrict: 'A',
        link: function (scope, element, attrs) {
            $(element.context).draggable({
                containment: 'parent',
                opacity: .7,
                stop: scope.dateMoved
            });
            $(element.context).resizable({
                containment: 'parent',
                handles: 'ne, se, sw, nw',
                stop: scope.dateResized
            });
        }
    }
});
    
var Guid = Guid || (function () {

    var EMPTY = '00000000-0000-0000-0000-000000000000';

    var _padLeft = function (paddingString, width, replacementChar) {
        return paddingString.length >= width ? paddingString : _padLeft(replacementChar + paddingString, width, replacementChar || ' ');
    };

    var _s4 = function (number) {
        var hexadecimalResult = number.toString(16);
        return _padLeft(hexadecimalResult, 4, '0');
    };

    var _cryptoGuid = function () {
        var buffer = new window.Uint16Array(8);
        window.crypto.getRandomValues(buffer);
        return [_s4(buffer[0]) + _s4(buffer[1]), _s4(buffer[2]), _s4(buffer[3]), _s4(buffer[4]), _s4(buffer[5]) + _s4(buffer[6]) + _s4(buffer[7])].join('-');
    };

    var _guid = function () {
        var currentDateMilliseconds = new Date().getTime();
        return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (currentChar) {
            var randomChar = (currentDateMilliseconds + Math.random() * 16) % 16 | 0;
            currentDateMilliseconds = Math.floor(currentDateMilliseconds / 16);
            return (currentChar === 'x' ? randomChar : (randomChar & 0x7 | 0x8)).toString(16);
        });
    };

    var create = function () {
        var hasCrypto = typeof (window.crypto) != 'undefined',
            hasRandomValues = typeof (window.crypto.getRandomValues) != 'undefined';
        return (hasCrypto && hasRandomValues) ? _cryptoGuid() : _guid();
    };

    return {
        newGuid: create,
        empty: EMPTY
    };
})();