﻿///#source 1 1 /Static/App/app.js
// Main configuration file. Sets up AngularJS module and routes and any other config objects

var app = angular.module('main', ['flow', 'ui.bootstrap', 'angular-flash.service', 'angular-flash.flash-alert-directive', 'ngRoute', 'ui.grid', 'ngResource', 'LocalStorageModule', 'models', 'context']);     //Define the main module

app.config(['$routeProvider', 'flashProvider', 'flowFactoryProvider', '$httpProvider', function ($routeProvider, flashProvider, flowFactoryProvider, $httpProvider) {
    //Setup routes to load partial templates from server. TemplateUrl is the location for the server view (Razor .cshtml view)
    $routeProvider
        .when('/', { templateUrl: '/static/app/views/login.min.html', controller: 'loginController' })
        .when('/templates', { templateUrl: '/static/app/views/templates.min.html', controller: 'templatesController' })
        .when('/templates/:formTemplateID', { templateUrl: '/static/app/views/template.min.html', controller: 'templateController' })
        .when('/templates/:formTemplateID/fields', { templateUrl: '/static/app/views/template-fields.min.html', controller: 'templateFieldsController' })
        .when('/templates/:formTemplateID/signatures', { templateUrl: '/static/app/views/template-signatures.min.html', controller: 'templateSignaturesController' })
        .when('/new-template', { templateUrl: '/static/app/views/new-template.min.html', controller: 'newTemplateController' })
        .when('/new-template/:displayName/:formType/:jurisdiction', { templateUrl: '/static/app/views/new-template-2.min.html', controller: 'newTemplate2Controller' })
        .when('/login', { templateUrl: '/static/app/views/login.min.html', controller: 'loginController' })
        .otherwise({ redirectTo: '/' });

    // setup flash
    flashProvider.errorClassnames.push('alert-danger');
    flashProvider.warnClassnames.push('alert-warning');
    flashProvider.infoClassnames.push('alert-info');
    flashProvider.successClassnames.push('alert-success');

    flowFactoryProvider.defaults = {
        target: '/api/templateupload',
        permanentErrors: [401, 403, 404, 500, 501]
    };

    $httpProvider.interceptors.push('authInterceptorService');
}]);

app.run(['$location', '$rootScope', 'models', '$q', 'authService', function ($location, $rootScope, models, $q, authService) {
    authService.fillAuthData();

    $rootScope.utils = {
        stringToNumber: function (text) {
            if (this == "") {
                return text;
            }
            var transformedInput = text.toString().replace(/[^0-9]/g, '');

            return transformedInput;
        },
        stringToDecimal: function (text) {
            if (this == "") {
                return text;
            }
            var transformedInput = text.toString().replace(/\$|[^0-9\.\$\-]/g, '');

            transformedInput = parseFloat(Math.round(parseFloat(transformedInput) * 100) / 100).toFixed(2);

            return transformedInput;
        },
        roundUp: function (num) {
            var int = parseInt(num);
            var float = parseFloat(num);

            if (parseFloat(int) === float) {
                // they gave me an integral
                return int;
            } else {
                // floating point detected! add one.
                return int + 1;
            }
        },
        mmddyyyy: function (date) {
            var yyyy = date.getFullYear().toString();
            var mm = (date.getMonth() + 1).toString(); // getMonth() is zero-based
            var dd = date.getDate().toString();
            return (mm[1] ? mm : "0" + mm[0]) + "/" + (dd[1] ? dd : "0" + dd[0]) + "/" + yyyy; // padding
        }
    };
}])

app.controller('RootController', ['$scope', '$location', 'authService','models', '$window', function ($scope, $location, authService,models,$window) {
    $scope.go = function (route, params) {
        $location.path(route).search(params || {});
    };

    $scope.$back = function () {
        $window.history.back();
    };

    $scope.$on('$routeChangeSuccess', function (e, current, previous) {
        $scope.activeViewPath = $location.path();
    });
    $scope.logOut = function () {
        authService.logOut();
        $location.path('/login');
    }

    $scope.defaultGridOptions = {
        multiSelect: false,
        enableSorting: true,
        enableRowSelection: false,
        enableColumnResize: true,
        showColumnMenu: true,
        enableFiltering: true
    };

    $scope.defaultGridScope = {
        go: $scope.go
    };

    $scope.authentication = authService.authentication;

    var init = function () {
        if (!$scope.authentication.isAuth) {
            $scope.go('/login');
        }
    };
    init();
}]);
app.directive('ngConfirmClick', [
  function () {
      return {
          priority: -1,
          restrict: 'A',
          link: function (scope, element, attrs) {
              element.bind('click', function (e) {
                  var message = attrs.ngConfirmClick;
                  if (message && !confirm(message)) {
                      e.stopImmediatePropagation();
                      e.preventDefault();
                  }
              });
          }
      }
  }
]);
app.directive('decimalInput', function () {
    return {
        require: 'ngModel',
        link: function (scope, element, attr, ngModelCtrl) {
            element.css('text-align', 'right');
            var fromUser = function (text) {
                var transformedInput = scope.utils.stringToDecimal(text);

                if (transformedInput !== text) {
                    ngModelCtrl.$setViewValue(transformedInput);
                    ngModelCtrl.$render();
                }
                return transformedInput; // or return Number(transformedInput)
            };
            ngModelCtrl.$parsers.push(fromUser);
        }
    };
});

app.directive('numericInput', function () {
    return {
        require: 'ngModel',
        link: function (scope, element, attr, ngModelCtrl) {
            //element.css('text-align', 'right');
            var fromUser = function (text) {
                var transformedInput = scope.utils.stringToNumber(text);

                if (transformedInput !== text) {
                    ngModelCtrl.$setViewValue(transformedInput);
                    ngModelCtrl.$render();
                }
                return transformedInput; // or return Number(transformedInput)
            };
            ngModelCtrl.$parsers.push(fromUser);
        }
    };
});

app.directive('signatureBox', function () {
    return {
        // A = attribute, E = Element, C = Class and M = HTML Comment
        restrict: 'A',
        link: function (scope, element, attrs) {
            $(element.context).draggable({
                containment: 'parent',
                opacity:.7,
                stop: scope.moved
            });
            $(element.context).resizable({
                containment: 'parent',
                handles: 'ne, se, sw, nw',
                stop:scope.resized
            });
        }
    }
});

app.directive('dateBox', function () {
    return {
        // A = attribute, E = Element, C = Class and M = HTML Comment
        restrict: 'A',
        link: function (scope, element, attrs) {
            $(element.context).draggable({
                containment: 'parent',
                opacity: .7,
                stop: scope.dateMoved
            });
            $(element.context).resizable({
                containment: 'parent',
                handles: 'ne, se, sw, nw',
                stop: scope.dateResized
            });
        }
    }
});
    
var Guid = Guid || (function () {

    var EMPTY = '00000000-0000-0000-0000-000000000000';

    var _padLeft = function (paddingString, width, replacementChar) {
        return paddingString.length >= width ? paddingString : _padLeft(replacementChar + paddingString, width, replacementChar || ' ');
    };

    var _s4 = function (number) {
        var hexadecimalResult = number.toString(16);
        return _padLeft(hexadecimalResult, 4, '0');
    };

    var _cryptoGuid = function () {
        var buffer = new window.Uint16Array(8);
        window.crypto.getRandomValues(buffer);
        return [_s4(buffer[0]) + _s4(buffer[1]), _s4(buffer[2]), _s4(buffer[3]), _s4(buffer[4]), _s4(buffer[5]) + _s4(buffer[6]) + _s4(buffer[7])].join('-');
    };

    var _guid = function () {
        var currentDateMilliseconds = new Date().getTime();
        return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (currentChar) {
            var randomChar = (currentDateMilliseconds + Math.random() * 16) % 16 | 0;
            currentDateMilliseconds = Math.floor(currentDateMilliseconds / 16);
            return (currentChar === 'x' ? randomChar : (randomChar & 0x7 | 0x8)).toString(16);
        });
    };

    var create = function () {
        var hasCrypto = typeof (window.crypto) != 'undefined',
            hasRandomValues = typeof (window.crypto.getRandomValues) != 'undefined';
        return (hasCrypto && hasRandomValues) ? _cryptoGuid() : _guid();
    };

    return {
        newGuid: create,
        empty: EMPTY
    };
})();
///#source 1 1 /Static/App/Services/context.js
angular.module('context', [])
    .service('apiService', ['$resource', '$q', function ($resource, $q) {
        var API = function (entity) {
            return $resource('/api/' + entity + '/:id', {}, { getOne: {method: 'GET', isArray: false}, getByID: { method: 'GET', isArray: false }, update: { method: 'PUT' }, insert: { method: 'POST' }, destroy: { method: 'DELETE' } });
        }

        this.query = function (entity, model, params, returnsArray) {
            params = params || { };
            var api = new API(entity);

            if (params.id) {
                return api.getByID(params).$promise.then(function (entity) {
                    var result = new model(entity);
                    return result;
                }, function (res) {
                    return $q.reject(res);
                });
            } else if (returnsArray === false) {
                return api.getOne(params).$promise.then(function (data) {
                    return new model(data);
                }, function (res) {
                    return $q.reject(res);
                });
            } else {
                return api.query(params).$promise.then(function (data) {
                    var result = [];
                    angular.forEach(data, function (element) {
                        result.push(new model(element));
                    });
                    return result;
                }, function (res) {
                    return $q.reject(res);
                });
            }
        };

        this.update = function (entity, id, element) {
            return new API(entity).update({ id: id }, element).$promise;
        };

        this.insert = function (entity, element, params) {
            params = params || {};
            return new API(entity).insert(params, element).$promise;
        };

        this.destroy = function (entity, id, params) {
            if (!params) {
                params = {};
            }
            params.id = id;
            return new API(entity).destroy(params).$promise;
        }
    }]);
///#source 1 1 /Static/App/Factories/models.js
angular.module('models', []).factory('models', ['apiService', function (apiService) {
    var models = {};

    // begin Example
    models.Example = function (extender) {
        this.exampleID = '00000000-0000-0000-0000-000000000000';
            
        angular.extend(this, extender);
    };

    models.Example.entity = function () { return "examples"; };

    models.Example.query = function (params) { return apiService.query(models.Example.entity(), models.Example, params, true); };

    models.Example.prototype.save = function () {
        if (this.exampleID && this.exampleID !== '00000000-0000-0000-0000-000000000000') {
            return apiService.update(models.Example.entity(), this.exampleID, this);
        } else {
            return apiService.insert(models.Example.entity(), this);
        }
    };

    models.Example.prototype.destroy = function () { return apiService.destroy(models.Example.entity(), this.exampleID); };
    // end Example

    // begin FormTemplate
    models.FormTemplate = function (extender) {
        this.formTemplateID = '00000000-0000-0000-0000-000000000000';
        this.displayName = '';
        this.formType = '';
        this.jurisdiction = '';
        angular.extend(this, extender);
    };

    models.FormTemplate.entity = function () { return "formTemplates"; };

    models.FormTemplate.query = function (params) { return apiService.query(models.FormTemplate.entity(), models.FormTemplate, params); };

    models.FormTemplate.prototype.save = function () {
        if (this.formTemplateID && this.formTemplateID !== '00000000-0000-0000-0000-000000000000') {
            return apiService.update(models.FormTemplate.entity(), this.formTemplateID, this);
        } else {
            return apiService.insert(models.FormTemplate.entity(), this);
        }
    };

    models.FormTemplate.prototype.destroy = function () { return apiService.destroy(models.FormTemplate.entity(), this.formTemplateID); };
    // end FormTemplate

    // begin PersonRole
    models.PersonRole = function (extender) {
        this.roleName = '';
        this.roleDescription = '';

        angular.extend(this, extender);
    };

    models.PersonRole.entity = function () { return "PersonRoles"; };

    models.PersonRole.query = function (params) {
        params = params || {};
        return apiService.query(models.PersonRole.entity(), models.PersonRole, params, !params.id);
    };

    // end PersonRole

    // begin FieldPreview
    models.FieldPreview = function (extender) {
        this.formTemplateAcroFieldInfoID = '00000000-0000-0000-0000-000000000000';
        this.src = '';
        this.fieldY = 0;
        this.fieldX = 0;
        this.fieldWidth = 0;
        this.fieldHeight = 0;
        this.imgWidth = 0;
        this.imgHeight = 0;

        angular.extend(this, extender);
    };
    models.FieldPreview.prototype.getBottomOffset = function () {
        return this.imgHeight - (this.fieldY) - (this.fieldHeight * 5);
    };
    models.FieldPreview.prototype.getLeftOffset = function () {
        return (this.fieldX);
    };
    models.FieldPreview.prototype.getBorderBoxStyle = function () {
        var height = ((this.fieldHeight * 10) + 18) + 'px';
        var width = (this.imgWidth + 20);
        width = width + 'px';

        return {
            border: '1px solid #777',
            height: height,
            width: width,
            padding: '9px 10px',
            background:'white'
        };
    };
    models.FieldPreview.prototype.getContainerStyle = function () {
        var height = (this.fieldHeight * 10) + 'px';
        var width = (this.imgWidth);
        width = width + 'px';

        return {
            height:height,
            width: width,
            overflow: 'hidden',
            position: 'relative'
        };
    };
    models.FieldPreview.prototype.getImageStyle = function () {
        var bottom = this.getBottomOffset() + 'px';
        var width = this.imgWidth + 'px';

        return {
            position: 'relative',
            bottom: bottom,
            width: width
        };
    };
    models.FieldPreview.prototype.getHighlightStyle = function () {
        var width = this.fieldWidth + 'px';
        var height = this.fieldHeight + 'px';
        var marginTop = (this.fieldHeight * 5) + 'px';

        var leftOffsetAdj = this.fieldX;

        leftOffsetAdj = leftOffsetAdj + 'px';

        return {
            'line-height': height,
            'font-size': height,
            width: width,
            height: height,
            position: 'absolute',
            overflow: 'hidden',
            top: 0,
            background: '#FFC',
            'margin-top': marginTop,
            'margin-left': leftOffsetAdj
        };
    };

    models.FieldPreview.entity = function () { return "FieldPreview"; };

    models.FieldPreview.query = function (params) { return apiService.query(models.FieldPreview.entity(), models.FieldPreview, params); };
    // end FieldPreview

    // begin SignatureInfo
    models.SignatureInfo = function (extender) {
        this.SignatureInfoID = '00000000-0000-0000-0000-000000000000';
        this.formTemplateID = '00000000-0000-0000-0000-000000000000';
        this.roleName = '';
        this.signatureType = 'Signature';

        this.x = 50;
        this.y = 50;
        this.width = 200;
        this.height = 16;

        this.pageNumber = 1;

        this.hasDate = false;
        this.dateX = '';
        this.dateY = '';
        this.dateWidth = '';
        this.dateHeight = '';
        
        angular.extend(this, extender);
    };
    // end SignatureInfo
        
    // begin FormTemplateFieldMapping
    models.FormTemplateFieldMapping = function (extender) {
        this.fieldMappingID = '00000000-0000-0000-0000-000000000000';
        this.formTemplateID = '00000000-0000-0000-0000-000000000000';
        this.acroFieldName = '';
        this.sourceFieldName = '';

        angular.extend(this, extender);
    };
    // end FormTemplateFieldMapping

    // begin FormTemplateAcroFieldInfo
    models.FormTemplateAcroFieldInfo = function (extender) {

        this.formTemplateAcroFieldInfoID = '00000000-0000-0000-0000-000000000000';
        this.acroFieldName = '';
        this.formTemplateID = '00000000-0000-0000-0000-000000000000';
        this.fieldTitle = '';
        this.fieldDescription = '';
        this.inputType = '';
        this.useDefaultDisplayInfoIfMapped = true;
        this.tabOrder = 0;
        this.pageNumber = 1;
        this.leftX = 0;
        this.bottomY = 0;
        this.width = 0;
        this.height = 0;
        this.editable = true;
        this.visible = true;

        angular.extend(this, extender);
    };
    models.FormTemplateAcroFieldInfo.entity = function () { return "FormTemplateFields"; };
    models.FormTemplateAcroFieldInfo.prototype.destroy = function () { return apiService.destroy(models.FormTemplateAcroFieldInfo.entity(), this.formTemplateAcroFieldInfoID); };
    // end FormTemplateAcroFieldInfo

    // begin FormTemplateAcroFieldSelectionOption
    models.FormTemplateAcroFieldSelectionOption = function (extender) {

        this.formTemplateAcroFieldSelectionOptionID = '00000000-0000-0000-0000-000000000000';
        this.formTemplateAcroFieldInfoID = '00000000-0000-0000-0000-000000000000';
        this.sortOrder = 0;
        this.displayName = '';
        this.selectionValue = '';

        angular.extend(this, extender);
    };
    // end FormTemplateAcroFieldSelectionOption

    return models;
}]);
///#source 1 1 /Static/App/Factories/authService.js
'use strict';
app.factory('authService', ['$http', '$q', 'localStorageService', function ($http, $q, localStorageService) {

    var serviceBase = '/';
    var authServiceFactory = {};

    var _authentication = {
        isAuth: false,
        userName: ""
    };

    var _saveRegistration = function (registration) {

        _logOut();

        return $http.post(serviceBase + 'api/account/register', registration).then(function (response) {
            return response;
        });

    };

    var _login = function (loginData) {

        var data = "grant_type=password&username=" + loginData.userName + "&password=" + loginData.password;

        var deferred = $q.defer();

        $http.post(serviceBase + 'token', data, { headers: { 'Content-Type': 'application/x-www-form-urlencoded' } }).success(function (response) {

            localStorageService.set('authorizationData', { token: response.access_token, userName: loginData.userName });

            _authentication.isAuth = true;
            _authentication.userName = loginData.userName;

            deferred.resolve(response);

        }).error(function (err, status) {
            _logOut();
            deferred.reject(err);
        });

        return deferred.promise;

    };

    var _logOut = function () {

        localStorageService.remove('authorizationData');

        _authentication.isAuth = false;
        _authentication.userName = "";

    };

    var _fillAuthData = function () {

        var authData = localStorageService.get('authorizationData');
        if (authData) {
            _authentication.isAuth = true;
            _authentication.userName = authData.userName;
        }

    }

    authServiceFactory.saveRegistration = _saveRegistration;
    authServiceFactory.login = _login;
    authServiceFactory.logOut = _logOut;
    authServiceFactory.fillAuthData = _fillAuthData;
    authServiceFactory.authentication = _authentication;

    return authServiceFactory;
}]);
///#source 1 1 /Static/App/Factories/authInterceptorService.js
'use strict';
app.factory('authInterceptorService', ['$q', '$location', 'localStorageService', 'flash', function ($q, $location, localStorageService, flash) {

    var authInterceptorServiceFactory = {};

    var _request = function (config) {

        config.headers = config.headers || {};

        var authData = localStorageService.get('authorizationData');
        if (authData) {
            config.headers.Authorization = 'Bearer ' + authData.token;
        }

        return config;
    }

    var _responseError = function (rejection) {
        
        if (rejection.status === 401) {
            flash.warn = 'Please sign in to continue.';
            $location.path('/login');
        } else if (rejection.status >= 500) {
            flash.warn = 'Connection error. Please try again in a few moments.';
        }

        return $q.reject(rejection);
    }

    authInterceptorServiceFactory.request = _request;
    authInterceptorServiceFactory.responseError = _responseError;

    return authInterceptorServiceFactory;
}]);
///#source 1 1 /Static/App/Controllers/templates.js
angular.module('main').controller('templatesController', ['$scope', 'models', function ($scope, models) {
    $scope.templates = [];
    $scope.loadingTemplates = true;
    $scope.fillTemplates = function () {
        return models.FormTemplate.query().then(function (data) {
            $scope.templates = data;
            $scope.loadingTemplates = false;
        });
    };

    // init/page setup
    var init = function () {
        $scope.fillTemplates();
    };

    init();
}]);
///#source 1 1 /Static/App/Controllers/templateFields.js
angular.module('main').controller('templateFieldsController', ['$q','$scope', 'models', '$routeParams','flash', function ($q,$scope, models, $routeParams, flash) {
    $scope.template = [];
    $scope.loadingTemplate = true;
    $scope.mappings = [];
    $scope.fields = [];
    $scope.selectionOptions = [];
    $scope.newOption = [];
    $scope.previewInfos = [];
    $scope.page = 1;

    // big global save
    $scope.saveWork = function () {
        $scope.template.acroFields = $scope.fields;
        $scope.template.fieldMappings = $scope.mappings;
        $scope.template.save().then(function (ret) {
            flash.success = "Template saved.";
        }); // save everything
    };

    // get from API
    $scope.fillTemplate = function () {
        
        return models.FormTemplate.query({ id: $routeParams.formTemplateID }).then(function (template) {
            $scope.template = template;
            $scope.loadingTemplate = false;
            angular.forEach($scope.template.fieldMappings, function (mapping) {
                $scope.mappings.push(new models.FormTemplateFieldMapping(mapping));
            });
            var calls = [];
            $scope.totalFields = $scope.template.acroFields.length;
            $scope.fieldsLoaded = 0;

            calls.push(models.FieldPreview.query({ formTemplateID: $routeParams.formTemplateID }).then(function (data) {
                angular.forEach(data, function (prev) {
                    $scope.previewInfos[prev.formTemplateAcroFieldInfoID] = prev;
                    $scope.fieldsLoaded += 1;
                    $scope.percentFieldsLoaded = $scope.fieldsLoaded / $scope.totalFields * 100;
                });
            }));

            angular.forEach($scope.template.acroFields, function (field) {
                $scope.initNewOption(field);

                $scope.fields.push(new models.FormTemplateAcroFieldInfo(field));
                angular.forEach(field.selectionOptions, function (option) {
                    $scope.selectionOptions.push(new models.FormTemplateAcroFieldSelectionOption(option));
                });
            });
            $q.all(calls).then(function () {
                $scope.loaded = true;
            });
        });
    };

    // dealing with selection options
    $scope.initNewOption = function (field) {
        $scope.newOption[field] = new models.FormTemplateAcroFieldSelectionOption({ formTemplateAcroFieldInfoID: field.formTemplateAcroFieldInfoID });
    };

    $scope.destroyOption = function (field, option) {
        field.selectionOptions.splice(field.selectionOptions.indexOf(option), 1);
    };

    $scope.destroyField = function (field) {
        $scope.fields.splice($scope.fields.indexOf(field), 1);
        var model = new models.FormTemplateAcroFieldInfo(field);
        model.destroy();
    };

    $scope.addNewOption = function (field) {
        field.selectionOptions.push($scope.newOption[field]);
        $scope.initNewOption(field);
    };

    // dealing with field mappings
    $scope.getFieldMapping = function (field) {
        var existingMapping = $.grep($scope.mappings, function (mapping) {
            return mapping.formTemplateID == $scope.template.formTemplateID
                && mapping.acroFieldName == field.acroFieldName;
        })[0];
        if (existingMapping) {
            return existingMapping;
        } else {
            var newMapping = new models.FormTemplateFieldMapping({
                formTemplateID: $scope.template.formTemplateID,
                acroFieldName: field.acroFieldName,
                sourceFieldName: ''
            });
            $scope.mappings.push(newMapping);
        }
    };

    // adding fields, not used quite yet
    $scope.createField = function (field, auto) {
        return field.save().then(function (apiField) {
            if (field.formTemplateAcroFieldInfoID = '00000000-0000-0000-0000-000000000000') {
                field.formTemplateAcroFieldInfoID = apiField.formTemplateAcroFieldInfoID;
            }
            if (auto) {
                // nothing, it's an auto save
                return field;
            } else {
                flash.success = 'Template saved.';
                return field;
            }
        });
    };

    // fields and pages
    $scope.getFieldPages = function () {
        var distinct = [];
        var found = {};
        angular.forEach($scope.fields, function (field) {
            var key = field.pageNumber;
            if (!found[key]) {
                found[key] = true;
                distinct.push(key);
            }
        });
        distinct.sort(function (a, b) { return a - b });
        return distinct;
    };

    $scope.toInitial = function (field, roleName) {
        var toInitial = new models.ToInitial({ id: field.formTemplateAcroFieldInfoID, role: roleName }).then()
        $scope.fields.splice($scope.fields.indexOf(field), 1);
    };

    $scope.moveFieldUp = function (page, index) {
        if (index == 0) return;
        var fields = $scope.getFieldsOnPage(page);
        swapFields(fields, index, index - 1);
    };

    $scope.moveFieldDown = function (page, index) {
        var fields = $scope.getFieldsOnPage(page);
        if (index === fields.length - 1) return;
        swapFields(fields, index, index + 1);
    };

    var swapFields = function (fields, index, indexToSwap) {
        var field = fields[index];
        var swapWith = fields[indexToSwap];

        var temp = field.tabOrder;

        field.tabOrder = swapWith.tabOrder;
        swapWith.tabOrder = temp;
    };

    $scope.getFieldsOnPage = function (pageNumber) {
        return $.grep($scope.fields, function (field) {
            return field.pageNumber === pageNumber && field.editable;
        }).sort(function (a, b) { return a.tabOrder > b.tabOrder ? 1 : -1 });
    };

    // init/page setup
    var init = function () {
        $scope.fillTemplate();
        models.PersonRole.query().then(function (roles) {
            $scope.roles = roles;
        });
    };

    init();
}]);
///#source 1 1 /Static/App/Controllers/templateSignatures.js
angular.module('main').controller('templateSignaturesController', ['$scope', 'models', '$routeParams','flash', function ($scope, models, $routeParams, flash) {
    $scope.template = false;
    $scope.loadingTemplate = true;
    $scope.page = 1;
    $scope.roles = [];
    $scope.signatureTypes = [{ name: 'Signature' }, { name: 'Initials' }];

    // API calls
    $scope.fillTemplate = function () {
        return models.FormTemplate.query({ id: $routeParams.formTemplateID }).then(function (template) {
            $scope.template = template;
            $scope.page = 1;
            $scope.select($scope.getSigsForPage(1)[0]);
        });
    };

    $scope.save = function () {
        $scope.template.save().then(function () {
            flash.success = "Signatures saved.";
        });
    };

    // push/splice
    $scope.addSignatureField = function () {
        var newSig = new models.SignatureInfo({
            formTemplateID: $scope.template.formTemplateID
        });
        $scope.template.document.signatureFields.push(newSig);
        $scope.select(newSig);
    };

    $scope.trashSelectedSignature = function () {
        var sig = $scope.selectedSig;

        if (sig) {
            var sigIndex = $scope.template.document.signatureFields.indexOf(sig);
            $scope.template.document.signatureFields.splice(sigIndex, 1);
            $scope.select($scope.template.document.signatureFields[0]);
        }
    };

    // signature date
    $scope.addSignatureDate = function (sig) {
        sig.hasDate = true;
        sig.dateX = sig.x + sig.width + 10;
        sig.dateY = sig.y;
        sig.dateWidth = 70;
        sig.dateHeight = 15;
    };

    $scope.removeSignatureDate = function (sig) {
        sig.hasDate = false;
        sig.dateX = '';
        sig.dateY = '';
        sig.dateWidth = '';
        sig.dateHeight = '';
    };

    // selection helpers
    $scope.getSigsForPage = function (page) {
        if (!$scope.template || !$scope.template.document.signatureFields) {
            return [];
        }
        return $.grep($scope.template.document.signatureFields, function (sig) {
            return sig.pageNumber == page;
        });
    };

    $scope.selected = function (sig) {
        return $scope.selectedSig == sig;
    };

    $scope.select = function (sig) {
        $scope.selectedSig = sig;
    };

    // styling
    $scope.getPageContainerDivStyle = function (page) {
        if ($scope.template && $scope.template.document.pages) {
            var index = page - 1;
            var img = $.grep($scope.template.document.pages, function (entity) { return entity.pageNumber == page })[0];
            if (img) {
                return {
                    'border': '1px solid #AAA',
                    'width': (img.pageWidth + 2) + 'px',
                    'height': (img.pageHeight + 2) + 'px',
                    margin: 'auto'
                };
            }
        }
    };

    $scope.getPageImageDivStyle = function () {
        if ($scope.template && $scope.template.document.pages) {
            var index = $scope.page - 1;
            var img = $.grep($scope.template.document.pages, function (entity) { return entity.pageNumber == $scope.page })[0];

            if (img) {
                return {
                    'position': 'relative',
                    'overflow': 'hidden',
                    'background': "url('/api/downloadblobpng/" + img.pageID + "?container=page-image')",
                    'width': img.pageWidth + 'px',
                    'height': img.pageHeight + 'px'
                };
            }
        }
    };

    $scope.getSignatureBoxStyle = function (sig) {
        if ($scope.template && $scope.template.document.pages) {
            var index = $scope.page - 1;
            var img = $.grep($scope.template.document.pages, function (entity) { return entity.pageNumber == $scope.page })[0];
            var background = '#ddd';
            if ($scope.selectedSig == sig) {
                background = '#d5fadd';
            }
            return {
                'position': 'absolute',
                'background': background,
                'width': (sig.width) + 'px',
                'height': (sig.height) + 'px',
                'left': (sig.x) + 'px',
                'top': (img.pageHeight - (sig.y)) + 'px',
                'border': '1px solid #777',
                'line-height': (sig.height - 2) + 'px',
                'font-size': (sig.height - 2) + 'px',
                'overflow': 'hidden'
            };
        }
    };

    $scope.getDateBoxStyle = function (sig) {
        if ($scope.template && $scope.template.document.pages) {
            var index = $scope.page - 1;
            var img = $.grep($scope.template.document.pages, function (entity) { return entity.pageNumber == $scope.page })[0];
            var background = '#ddd';
            if ($scope.selectedSig == sig) {
                background = '#fcf8e3';
            }
            return {
                'position': 'absolute',
                'background': background,
                'width': (sig.dateWidth) + 'px',
                'height': (sig.dateHeight) + 'px',
                'left': (sig.dateX) + 'px',
                'top': (img.pageHeight - (sig.dateY)) + 'px',
                'border': '1px solid #777',
                'line-height': (sig.dateHeight - 2) + 'px',
                'font-size': (sig.dateHeight - 2) + 'px',
                'overflow': 'hidden'
            };
        }
    };

    // drag/resize events
    $scope.dateResized = function (event, ui) {


        var dateID = event.target.id;
        var sig = $scope.selectedSig;
        if (sig) {

            var dw = ui.size.width - ui.originalSize.width;
            var dh = ui.size.height - ui.originalSize.height;
            var dx = ui.position.left - ui.originalPosition.left;
            var dy = ui.position.top - ui.originalPosition.top;

            sig.dateWidth += dw;
            sig.dateHeight += dh;

            sig.dateX += dx;
            sig.dateY -= dy; // y is from the bottom, so it's opposite

            sig.dateWidth = Math.round(sig.dateWidth);
            sig.dateHeight = Math.round(sig.dateHeight);
            sig.dateX = Math.round(sig.dateX);
            sig.dateY = Math.round(sig.dateY);
        }
    };

    $scope.dateMoved = function (event, ui) {
        var dateID = event.target.id;
        var sig = $scope.selectedSig;
        if (sig) {
            var dx = ui.position.left - ui.originalPosition.left;
            var dy = ui.position.top - ui.originalPosition.top;

            sig.dateX += dx;
            sig.dateY -= dy; // y is from the bottom, so it's opposite

            sig.dateX = Math.round(sig.dateX);
            sig.dateY = Math.round(sig.dateY);
        }
    };

    $scope.resized = function (event,ui) {
        

        var sigID = event.target.id;
        var sig = $scope.selectedSig;
        if (sig) {

            var dw = ui.size.width - ui.originalSize.width;
            var dh = ui.size.height - ui.originalSize.height;
            var dx = ui.position.left - ui.originalPosition.left;
            var dy = ui.position.top - ui.originalPosition.top;

            sig.width += dw;
            sig.height += dh;

            sig.x += dx;
            sig.y -= dy; // sig.y is from the bottom, so it's opposite

            sig.width = Math.round(sig.width);
            sig.height = Math.round(sig.height);
            sig.x = Math.round(sig.x);
            sig.y = Math.round(sig.y);
        }
    };
    
    $scope.moved = function (event, ui) {
        var sigID = event.target.id;
        var sig = $scope.selectedSig;
        if (sig) {
            var img = $.grep($scope.template.document.pages, function (entity) { return entity.pageNumber == $scope.page })[0];

            var left = ui.position.left;
            var top = ui.position.top;
            sig.x = left;
            sig.y = img.pageHeight - top;

            sig.x = Math.round(sig.x);
            sig.y = Math.round(sig.y);
        }
    };

    // init/page setup
    var init = function () {
        $scope.fillTemplate();
        models.PersonRole.query().then(function (roles) {
            $scope.roles = roles;
        });
    };

    init();
}]);
///#source 1 1 /Static/App/Controllers/template.js
angular.module('main').controller('templateController', ['$scope', 'models', '$routeParams', 'flash', function ($scope, models, $routeParams, flash) {
    $scope.uploadProcessing = false;
    $scope.template = [];
    $scope.loadingTemplate = true;
    $scope.fillTemplate = function () {
        return models.FormTemplate.query({ id: $routeParams.formTemplateID }).then(function (template) {
            $scope.template = template;
            $scope.loadingTemplate = false;
        });
    };
    $scope.getFieldCount = function () {
        return $scope.template.acroFields.length;
    };

    $scope.getSignatureCount = function () {
        return $scope.template.document.signatureFields.length;
    };

    $scope.saveTemplate = function (auto) {
        return $scope.template.save().then(function (ret) {
            if (auto) {
                // nothing, it's an auto save
            } else {
                flash.success = 'Template saved.';
            }
        });
    }

    $scope.destroyTemplate = function () {
        $scope.template.destroy().then(function () {
            $scope.go('/templates');
        });
    };

    // uploading
    // flow config
    $scope.flowOptions = {
        target: '/api/templatereplace/?templateID=' + $routeParams.formTemplateID,
        permanentErrors: [404, 500, 501],
        generateUniqueIdentifier: Guid.newGuid
    };

    $scope.setUploadProcessing = function () {
        $scope.uploadProcessing = true;
    }

    $scope.uploadComplete = function ($file, $message) {
        flash.success = "Upload complete.";
        $scope.uploadProcessing = false;
    };

    // init/page setup
    var init = function () {
        $scope.fillTemplate();
    };

    init();
}]);
///#source 1 1 /Static/App/Controllers/newTemplate2.js
angular.module('main').controller('newTemplate2Controller', ['$scope', 'models', '$routeParams', 'flash', function ($scope, models, $routeParams, flash) {
    // flow config
    $scope.flowOptions = {
        target: '/api/templateupload/?displayName=' + $routeParams.displayName + '&formType=' + $routeParams.formType + '&jurisdiction=' + $routeParams.jurisdiction,
        permanentErrors: [404, 500, 501],
        generateUniqueIdentifier: Guid.newGuid
    };

    $scope.setProcessing = function () {
        $scope.processing = true;
    }

    $scope.uploadComplete = function ($file, $message) {
        flash.success = "Upload complete.";

        $scope.go('/templates/' + JSON.parse($message)[0].formTemplateID);
    };

    // init/page setup
    var init = function () {

    };

    init();
}]);
///#source 1 1 /Static/App/Controllers/newTemplate.js
angular.module('main').controller('newTemplateController', ['$scope', 'models', '$routeParams', function ($scope, models, $routeParams) {
    $scope.info = {
        displayName: '',
        formType: '',
        jurisdiction: 'US-FL'
    };

    $scope.goNext = function () {
        $scope.go('/new-template/' + $scope.info.displayName + '/' + $scope.info.formType + '/' + $scope.info.jurisdiction);
    }
}]);
///#source 1 1 /Static/App/Controllers/login.js
'use strict';
app.controller('loginController', ['$scope', '$location', 'authService','flash', function ($scope, $location, authService, flash) {
    $scope.loginData = {
        userName: "",
        password: ""
    };

    $scope.message = "";

    $scope.login = function () {
        $scope.loggingOn = true;
        authService.login($scope.loginData).then(function (response) {

            $location.path('/templates');

        }, function (err) {
            $scope.loggingOn = false;
            $scope.message = err.error_description;
        });
    };

    var init = function () {
    };
    init();
}]);
