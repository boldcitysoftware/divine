﻿angular.module('context', [])
    .service('apiService', ['$resource', '$q', function ($resource, $q) {
        var API = function (entity) {
            return $resource('/api/' + entity + '/:id', {}, { getOne: {method: 'GET', isArray: false}, getByID: { method: 'GET', isArray: false }, update: { method: 'PUT' }, insert: { method: 'POST' }, destroy: { method: 'DELETE' } });
        }

        this.query = function (entity, model, params, returnsArray) {
            params = params || { };
            var api = new API(entity);

            if (params.id) {
                return api.getByID(params).$promise.then(function (entity) {
                    var result = new model(entity);
                    return result;
                }, function (res) {
                    return $q.reject(res);
                });
            } else if (returnsArray === false) {
                return api.getOne(params).$promise.then(function (data) {
                    return new model(data);
                }, function (res) {
                    return $q.reject(res);
                });
            } else {
                return api.query(params).$promise.then(function (data) {
                    var result = [];
                    angular.forEach(data, function (element) {
                        result.push(new model(element));
                    });
                    return result;
                }, function (res) {
                    return $q.reject(res);
                });
            }
        };

        this.update = function (entity, id, element) {
            return new API(entity).update({ id: id }, element).$promise;
        };

        this.insert = function (entity, element, params) {
            params = params || {};
            return new API(entity).insert(params, element).$promise;
        };

        this.destroy = function (entity, id, params) {
            if (!params) {
                params = {};
            }
            params.id = id;
            return new API(entity).destroy(params).$promise;
        }
    }]);