﻿angular.module('main').controller('templateController', ['$scope', 'models', '$routeParams', 'flash', function ($scope, models, $routeParams, flash) {
    $scope.uploadProcessing = false;
    $scope.template = [];
    $scope.loadingTemplate = true;
    $scope.fillTemplate = function () {
        return models.FormTemplate.query({ id: $routeParams.formTemplateID }).then(function (template) {
            $scope.template = template;
            $scope.loadingTemplate = false;
        });
    };
    $scope.getFieldCount = function () {
        return $scope.template.acroFields.length;
    };

    $scope.getSignatureCount = function () {
        return $scope.template.document.signatureFields.length;
    };

    $scope.saveTemplate = function (auto) {
        return $scope.template.save().then(function (ret) {
            if (auto) {
                // nothing, it's an auto save
            } else {
                flash.success = 'Template saved.';
            }
        });
    }

    $scope.destroyTemplate = function () {
        $scope.template.destroy().then(function () {
            $scope.go('/templates');
        });
    };

    // uploading
    // flow config
    $scope.flowOptions = {
        target: '/api/templatereplace/?templateID=' + $routeParams.formTemplateID,
        permanentErrors: [404, 500, 501],
        generateUniqueIdentifier: Guid.newGuid
    };

    $scope.setUploadProcessing = function () {
        $scope.uploadProcessing = true;
    }

    $scope.uploadComplete = function ($file, $message) {
        flash.success = "Upload complete.";
        $scope.uploadProcessing = false;
    };

    // init/page setup
    var init = function () {
        $scope.fillTemplate();
    };

    init();
}]);