﻿angular.module('main').controller('templatesController', ['$scope', 'models', function ($scope, models) {
    $scope.templates = [];
    $scope.loadingTemplates = true;
    $scope.fillTemplates = function () {
        return models.FormTemplate.query().then(function (data) {
            $scope.templates = data;
            $scope.loadingTemplates = false;
        });
    };

    // init/page setup
    var init = function () {
        $scope.fillTemplates();
    };

    init();
}]);