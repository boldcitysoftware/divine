﻿angular.module('main').controller('newTemplate2Controller', ['$scope', 'models', '$routeParams', 'flash', function ($scope, models, $routeParams, flash) {
    // flow config
    $scope.flowOptions = {
        target: '/api/templateupload/?displayName=' + $routeParams.displayName + '&formType=' + $routeParams.formType + '&jurisdiction=' + $routeParams.jurisdiction,
        permanentErrors: [404, 500, 501],
        generateUniqueIdentifier: Guid.newGuid
    };

    $scope.setProcessing = function () {
        $scope.processing = true;
    }

    $scope.uploadComplete = function ($file, $message) {
        flash.success = "Upload complete.";

        $scope.go('/templates/' + JSON.parse($message)[0].formTemplateID);
    };

    // init/page setup
    var init = function () {

    };

    init();
}]);