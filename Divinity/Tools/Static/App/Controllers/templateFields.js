﻿angular.module('main').controller('templateFieldsController', ['$q','$scope', 'models', '$routeParams','flash', function ($q,$scope, models, $routeParams, flash) {
    $scope.template = [];
    $scope.loadingTemplate = true;
    $scope.mappings = [];
    $scope.fields = [];
    $scope.selectionOptions = [];
    $scope.newOption = [];
    $scope.previewInfos = [];
    $scope.page = 1;

    // big global save
    $scope.saveWork = function () {
        $scope.template.acroFields = $scope.fields;
        $scope.template.fieldMappings = $scope.mappings;
        $scope.template.save().then(function (ret) {
            flash.success = "Template saved.";
        }); // save everything
    };

    // get from API
    $scope.fillTemplate = function () {
        
        return models.FormTemplate.query({ id: $routeParams.formTemplateID }).then(function (template) {
            $scope.template = template;
            $scope.loadingTemplate = false;
            angular.forEach($scope.template.fieldMappings, function (mapping) {
                $scope.mappings.push(new models.FormTemplateFieldMapping(mapping));
            });
            var calls = [];
            $scope.totalFields = $scope.template.acroFields.length;
            $scope.fieldsLoaded = 0;

            calls.push(models.FieldPreview.query({ formTemplateID: $routeParams.formTemplateID }).then(function (data) {
                angular.forEach(data, function (prev) {
                    $scope.previewInfos[prev.formTemplateAcroFieldInfoID] = prev;
                    $scope.fieldsLoaded += 1;
                    $scope.percentFieldsLoaded = $scope.fieldsLoaded / $scope.totalFields * 100;
                });
            }));

            angular.forEach($scope.template.acroFields, function (field) {
                $scope.initNewOption(field);

                $scope.fields.push(new models.FormTemplateAcroFieldInfo(field));
                angular.forEach(field.selectionOptions, function (option) {
                    $scope.selectionOptions.push(new models.FormTemplateAcroFieldSelectionOption(option));
                });
            });
            $q.all(calls).then(function () {
                $scope.loaded = true;
            });
        });
    };

    // dealing with selection options
    $scope.initNewOption = function (field) {
        $scope.newOption[field] = new models.FormTemplateAcroFieldSelectionOption({ formTemplateAcroFieldInfoID: field.formTemplateAcroFieldInfoID });
    };

    $scope.destroyOption = function (field, option) {
        field.selectionOptions.splice(field.selectionOptions.indexOf(option), 1);
    };

    $scope.destroyField = function (field) {
        $scope.fields.splice($scope.fields.indexOf(field), 1);
        var model = new models.FormTemplateAcroFieldInfo(field);
        model.destroy();
    };

    $scope.addNewOption = function (field) {
        field.selectionOptions.push($scope.newOption[field]);
        $scope.initNewOption(field);
    };

    // dealing with field mappings
    $scope.getFieldMapping = function (field) {
        var existingMapping = $.grep($scope.mappings, function (mapping) {
            return mapping.formTemplateID == $scope.template.formTemplateID
                && mapping.acroFieldName == field.acroFieldName;
        })[0];
        if (existingMapping) {
            return existingMapping;
        } else {
            var newMapping = new models.FormTemplateFieldMapping({
                formTemplateID: $scope.template.formTemplateID,
                acroFieldName: field.acroFieldName,
                sourceFieldName: ''
            });
            $scope.mappings.push(newMapping);
        }
    };

    // adding fields, not used quite yet
    $scope.createField = function (field, auto) {
        return field.save().then(function (apiField) {
            if (field.formTemplateAcroFieldInfoID = '00000000-0000-0000-0000-000000000000') {
                field.formTemplateAcroFieldInfoID = apiField.formTemplateAcroFieldInfoID;
            }
            if (auto) {
                // nothing, it's an auto save
                return field;
            } else {
                flash.success = 'Template saved.';
                return field;
            }
        });
    };

    // fields and pages
    $scope.getFieldPages = function () {
        var distinct = [];
        var found = {};
        angular.forEach($scope.fields, function (field) {
            var key = field.pageNumber;
            if (!found[key]) {
                found[key] = true;
                distinct.push(key);
            }
        });
        distinct.sort(function (a, b) { return a - b });
        return distinct;
    };

    $scope.toInitial = function (field, roleName) {
        var toInitial = new models.ToInitial({ id: field.formTemplateAcroFieldInfoID, role: roleName }).then()
        $scope.fields.splice($scope.fields.indexOf(field), 1);
    };

    $scope.moveFieldUp = function (page, index) {
        if (index == 0) return;
        var fields = $scope.getFieldsOnPage(page);
        swapFields(fields, index, index - 1);
    };

    $scope.moveFieldDown = function (page, index) {
        var fields = $scope.getFieldsOnPage(page);
        if (index === fields.length - 1) return;
        swapFields(fields, index, index + 1);
    };

    var swapFields = function (fields, index, indexToSwap) {
        var field = fields[index];
        var swapWith = fields[indexToSwap];

        var temp = field.tabOrder;

        field.tabOrder = swapWith.tabOrder;
        swapWith.tabOrder = temp;
    };

    $scope.getFieldsOnPage = function (pageNumber) {
        return $.grep($scope.fields, function (field) {
            return field.pageNumber === pageNumber && field.editable;
        }).sort(function (a, b) { return a.tabOrder > b.tabOrder ? 1 : -1 });
    };

    // init/page setup
    var init = function () {
        $scope.fillTemplate();
        models.PersonRole.query().then(function (roles) {
            $scope.roles = roles;
        });
    };

    init();
}]);