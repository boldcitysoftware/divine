﻿'use strict';
app.controller('loginController', ['$scope', '$location', 'authService','flash', function ($scope, $location, authService, flash) {
    $scope.loginData = {
        userName: "",
        password: ""
    };

    $scope.message = "";

    $scope.login = function () {
        $scope.loggingOn = true;
        authService.login($scope.loginData).then(function (response) {

            $location.path('/templates');

        }, function (err) {
            $scope.loggingOn = false;
            $scope.message = err.error_description;
        });
    };

    var init = function () {
    };
    init();
}]);