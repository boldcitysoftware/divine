﻿angular.module('main').controller('newTemplateController', ['$scope', 'models', '$routeParams', function ($scope, models, $routeParams) {
    $scope.info = {
        displayName: '',
        formType: '',
        jurisdiction: 'US-FL'
    };

    $scope.goNext = function () {
        $scope.go('/new-template/' + $scope.info.displayName + '/' + $scope.info.formType + '/' + $scope.info.jurisdiction);
    }
}]);