﻿using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Divinity.Models;
using System.Threading.Tasks;

namespace Divinity.Tools.Controllers.Api {
    [Authorize]
    public class FieldPreviewController : ApiController {
        Divinity.Models.DivinityContext db;
        public FieldPreviewController() {
            db = new Models.DivinityContext();
        }

        // GET api/FieldPreview/5
        public async Task<IEnumerable<AcroFieldPositionOnPageImage>> Get(Guid formTemplateID) {
            return await db.AcroFieldPositionOnPageImage
                .Where(af => af.FormTemplateID == formTemplateID).ToListAsync();
        }

        protected override void Dispose(bool disposing) {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}