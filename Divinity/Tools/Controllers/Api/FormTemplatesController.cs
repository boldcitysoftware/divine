﻿using Divinity.Common.BaseClasses;
using Divinity.Models;
using Divinity.Models.Helpers;
using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Divinity.Tools.Controllers.Api {
    [Authorize]
    public class FormTemplatesController : UserController {
        public FormTemplatesController() {
            db.Configuration.ProxyCreationEnabled = false;
        }
        private DivinityContext db = new DivinityContext();

        // GET api/FormTemplates
        public IEnumerable<FormTemplate> GetFormTemplates() {
            return db.FormTemplate
                .Include(t => t.FieldMappings)
                .Include(t => t.AcroFields)
                .Include(t => t.AcroFields.Select(a => a.SelectionOptions))
                .Include(t => t.Document.SignatureFields)
                .AsEnumerable();
        }

        // GET api/FormTemplates/5
        public FormTemplate GetFormTemplate(Guid id) {
            var template = db.FormTemplate
                .Include(t => t.FieldMappings)
                .Include(t => t.AcroFields)
                .Include(t => t.AcroFields.Select(a => a.SelectionOptions))
                .Include(t => t.Document.Pages)
                .Include(t => t.Document.SignatureFields)
                .SingleOrDefault(t => t.FormTemplateID == id);
            if (template == null) {
                throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.NotFound));
            }
            return template;
        }

        // PUT api/FormTemplates/5
        public HttpResponseMessage PutFormTemplate(Guid id, FormTemplate template) {
            if (!ModelState.IsValid) {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }

            if (id != template.FormTemplateID) {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }

            var dbFormTemplate = db.FormTemplate
                .Include(t => t.AcroFields)
                .Include(t => t.AcroFields.Select(a => a.SelectionOptions))
                .Include(t => t.FieldMappings)
                .Include(t => t.Document.SignatureFields)
                .SingleOrDefault(t => t.FormTemplateID == id);

            if (dbFormTemplate == null) {
                throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.NotFound));
            }

            db.Entry(dbFormTemplate).CurrentValues.SetValues(template);

            var dbAcrosToRemove = new List<FormTemplateAcroFieldInfo>();

            foreach (var dbAcro in dbFormTemplate.AcroFields) {
                var acro = template.AcroFields.SingleOrDefault(af => af.FormTemplateAcroFieldInfoID == dbAcro.FormTemplateAcroFieldInfoID);
                if (acro != null) {
                    db.Entry(dbAcro).CurrentValues.SetValues(acro);

                    var dbOptionsToRemove = new List<FormTemplateAcroFieldSelectionOption>();

                    foreach (var dbOption in dbAcro.SelectionOptions) {
                        var option = acro.SelectionOptions.SingleOrDefault(o => o.FormTemplateAcroFieldSelectionOptionID == dbOption.FormTemplateAcroFieldSelectionOptionID);

                        if (option != null) {
                            db.Entry(dbOption).CurrentValues.SetValues(option);
                        } else {
                            dbOptionsToRemove.Add(dbOption);
                        }
                    }

                    foreach (var dbOption in dbOptionsToRemove) {
                        db.FormTemplateAcroFieldSelectionOption.Remove(dbOption);
                    }
                    
                    foreach (var option in acro.SelectionOptions) {
                        if (option.FormTemplateAcroFieldSelectionOptionID == Guid.Empty) {
                            option.FormTemplateAcroFieldSelectionOptionID = SequentialGuidCreator.Create();
                            dbAcro.SelectionOptions.Add(option);
                        }
                    }
                } else {
                    dbAcrosToRemove.Add(dbAcro);
                }
            }
            foreach (var dbAcro in dbAcrosToRemove) {
                db.FormTemplateAcroFieldInfo.Remove(dbAcro);
            }

            foreach (var dbMapping in dbFormTemplate.FieldMappings) {
                var mapping = template.FieldMappings.SingleOrDefault(af => af.AcroFieldName == dbMapping.AcroFieldName);
                if (mapping != null) {
                    if (mapping.FieldMappingID == Guid.Empty) {
                        mapping.FieldMappingID = dbMapping.FieldMappingID;
                    }
                    db.Entry(dbMapping).CurrentValues.SetValues(mapping);
                } else {
                    db.FormTemplateFieldMapping.Remove(dbMapping);
                }
            }

            foreach (var mapping in template.FieldMappings) {
                if (mapping.FieldMappingID == Guid.Empty) {
                    mapping.FieldMappingID = SequentialGuidCreator.Create();
                    dbFormTemplate.FieldMappings.Add(mapping);
                }
            }

            foreach (var dbSignature in dbFormTemplate.Document.SignatureFields) {
                var sigField = template.Document.SignatureFields.SingleOrDefault(sig => sig.SignatureInfoID == dbSignature.SignatureInfoID);
                if (sigField != null) {
                    db.Entry(dbSignature).CurrentValues.SetValues(sigField);
                }
                else {
                    db.SignatureInfo.Remove(dbSignature);
                }
            }

            foreach (var sigField in template.Document.SignatureFields) {
                if (sigField.SignatureInfoID == Guid.Empty) {
                    sigField.SignatureInfoID = SequentialGuidCreator.Create();
                    dbFormTemplate.Document.SignatureFields.Add(sigField);
                }
            }

            dbFormTemplate.UpdatedAt = DateTimeOffset.Now;

            try {
                db.SaveChanges();
            } catch (DbUpdateConcurrencyException ex) {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, ex);
            }

            return Request.CreateResponse(HttpStatusCode.OK);
        }

        // DELETE api/FormTemplates/5
        public HttpResponseMessage DeleteFormTemplate(Guid id) {
            FormTemplate template = db.FormTemplate.Find(id);
            if (template == null) {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }

            try {
                db.Database.ExecuteSqlCommand("EXEC DeleteFormTemplate @p0", id);
            } catch (DbUpdateConcurrencyException ex) {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, ex);
            }

            return Request.CreateResponse(HttpStatusCode.OK, template);
        }

        protected override void Dispose(bool disposing) {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}