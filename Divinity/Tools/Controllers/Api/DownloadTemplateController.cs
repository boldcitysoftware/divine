﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using Divinity.Storage.Cloud;

namespace Divinity.Tools.Controllers.Api {

    public class DownloadTemplateController : ApiController {
        Divinity.Models.DivinityContext db;
        public DownloadTemplateController() {
            db = new Models.DivinityContext();
        }
        // GET api/downloadtemplate/5
        public async Task<HttpResponseMessage> Get(Guid id) {
            var template = db.FormTemplate.Find(id);
            byte[] templateBytes = await BlobStorage.GetBytesAsync("template", id.ToString());
            if (template == null || templateBytes == null) {
                throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.NotFound));
            }

            HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.OK);
            response.Content = new ByteArrayContent(templateBytes);
            response.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/octet-stream");
            response.Content.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment");
            response.Content.Headers.ContentDisposition.FileName = template.DisplayName + ".pdf";

            return response;
        }
        protected override void Dispose(bool disposing) {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}