﻿using Divinity.Models;
using Divinity.Models.Helpers;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Runtime.Serialization;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Divinity.Tools.Controllers.Api.Helpers;
using Divinity.PdfToPng;
using Divinity.Storage.Cloud;
using System.Data.Entity.Validation;

namespace Divinity.Tools.Controllers.Api
{
    [DataContract]
    public class TemplateFileDesc {
        [DataMember]
        public string name { get; set; }
        [DataMember]
        public long size { get; set; }
        [DataMember]
        public Guid? formTemplateID { get; set; }
        public TemplateFileDesc(string n, long s, Guid? t) {
            name = n;
            size = s;
            formTemplateID = t;
        }
    }

    public class TemplateUploadController : ApiController {
        private readonly string BASEFOLDERNAME = "templates";
        private class FlowMeta {
            public string flowChunkNumber { get; set; }
            public string flowChunkSize { get; set; }
            public string flowCurrentChunkSize { get; set; }
            public string flowTotalSize { get; set; }
            public string flowIdentifier { get; set; }
            public string flowFilename { get; set; }
            public string flowRelativePath { get; set; }
            public string flowTotalChunks { get; set; }
            public byte[] ChunkData { get; set; }

            public string ChunkID {
                get {
                    return (flowTotalChunks == "1") ? flowIdentifier : String.Format("{0}_{1}", flowIdentifier, flowChunkNumber);
                }
            }

            public FlowMeta(Dictionary<string, string> values) {
                flowChunkNumber = values["flowChunkNumber"];
                flowChunkSize = values["flowChunkSize"];
                flowCurrentChunkSize = values["flowCurrentChunkSize"];
                flowTotalSize = values["flowTotalSize"];
                flowIdentifier = values["flowIdentifier"];
                flowFilename = values["flowFilename"];
                flowRelativePath = values["flowRelativePath"];
                flowTotalChunks = values["flowTotalChunks"];
            }

            public FlowMeta(NameValueCollection values) {
                flowChunkNumber = values["flowChunkNumber"];
                flowChunkSize = values["flowChunkSize"];
                flowCurrentChunkSize = values["flowCurrentChunkSize"];
                flowTotalSize = values["flowTotalSize"];
                flowIdentifier = values["flowIdentifier"];
                flowFilename = values["flowFilename"];
                flowRelativePath = values["flowRelativePath"];
                flowTotalChunks = values["flowTotalChunks"];
            }
        }

        public HttpResponseMessage Get([FromUri]string attachmentType = "") {
            var meta = new FlowMeta(Request.GetQueryNameValuePairs().ToDictionary(x => x.Key, x => x.Value));

            List<FlowMeta> flows = (List<FlowMeta>)System.Web.HttpContext.Current.Cache[meta.flowIdentifier];

            if (flows != null && flows.Any(f=>f.flowIdentifier == meta.flowIdentifier && f.flowChunkNumber == meta.flowChunkNumber)) {
                return Request.CreateResponse(HttpStatusCode.OK);
            } else {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }
        }

        public async Task<IEnumerable<TemplateFileDesc>> Post([FromUri]string displayName, [FromUri]string formType, [FromUri]string jurisdiction) {
            var meta = new FlowMeta(HttpContext.Current.Request.Form);
            
            if (!Request.Content.IsMimeMultipartContent()) {
                throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);
            }

            var streamProvider = new MultipartMemoryStreamProvider();
            
            await Request.Content.ReadAsMultipartAsync(streamProvider);

            using (Stream fileChunkStream = await streamProvider.Contents[8].ReadAsStreamAsync()) {
                if (fileChunkStream == null) {
                    throw new HttpResponseException(HttpStatusCode.NotFound);
                }

                meta.ChunkData = new byte[fileChunkStream.Length];
                fileChunkStream.Read(meta.ChunkData, 0, (int)fileChunkStream.Length);
            }

            Guid? templateID = null;

            List<FlowMeta> flows = (List<FlowMeta>)System.Web.HttpContext.Current.Cache[meta.flowIdentifier];

            if (flows == null) {
                flows = new List<FlowMeta>();
                HttpContext.Current.Cache.Add(meta.flowIdentifier, flows, null, System.Web.Caching.Cache.NoAbsoluteExpiration, new TimeSpan(1, 0, 0), System.Web.Caching.CacheItemPriority.Default, null);
            }

            flows.Add(meta);

            if (flows.Count == int.Parse(meta.flowTotalChunks)) {
                // all chunks are up!
                byte[] finalBytes = new byte[flows.Sum(f => f.ChunkData.LongLength)];
                long position = 0;
                
                foreach (byte[] bytes in flows.OrderBy(m => int.Parse(m.flowChunkNumber)).Select(m=>m.ChunkData)) {
                    Array.Copy(bytes, 0, finalBytes, position, bytes.LongLength);
                    position += bytes.LongLength;
                }
                
            
                // final or only chunk: need to save the template
                DivinityContext db = new DivinityContext();

                var document = new Document {
                    DocumentID = SequentialGuidCreator.Create(),
                    CreatedAt = DateTimeOffset.Now,
                    UpdatedAt = DateTimeOffset.Now,
                    DisplayName = displayName,
                    ContentType = "application/pdf",
                    FileName = meta.flowFilename
                };
                document.Pages = new List<Page>();
                document.SignatureFields = new List<SignatureInfo>();

                var template = new FormTemplate {
                    CreatedAt = DateTimeOffset.Now,
                    UpdatedAt = DateTimeOffset.Now,
                    DisplayName = displayName,
                    DocumentID = document.DocumentID,
                    FormTemplateID = SequentialGuidCreator.Create(),
                    FormType = formType,
                    Jurisdiction = jurisdiction,
                    TemplatePath = String.Format("~/templates/{0}/{1}", meta.flowIdentifier, meta.flowFilename)
                };

                template.Document = document;

                TemplateEngine.Utils.AddAcroFieldInfoToTemplate(finalBytes,template);
                db.FormTemplate.Add(template);

                finalBytes = TemplateEngine.Utils.RemoveGarbageFromPDF(finalBytes);

                int pageNum = 0;

                foreach (Converter.PageImageInfo imageForPage in Converter.Convert(finalBytes)) {
                    pageNum++;
                    var page = new Page {
                        DocumentID = document.DocumentID,
                        PageID = SequentialGuidCreator.Create(),
                        PageNumber = pageNum,
                        PageWidth = imageForPage.Width,
                        PageHeight = imageForPage.Height
                    };
                    document.Pages.Add(page);
                    await BlobStorage.StoreBytesAsync("page-image", page.PageID.ToString(), imageForPage.PngBytes);
                }
                template.PageCount = pageNum;
                try {
                    await db.SaveChangesAsync();
                }
                catch (DbEntityValidationException ex) {
                    ex.ToString();
                    throw;
                }
                await BlobStorage.StoreBytesAsync("template", template.FormTemplateID.ToString(), finalBytes);

                var fileInfo = new TemplateFileDesc[]{
                    new TemplateFileDesc(meta.flowFilename, finalBytes.LongLength / 1024, template.FormTemplateID)
                };
                return fileInfo;
            }
            else {
                // multiple files, this isn't the last one(s)...
                return new TemplateFileDesc[]{
                    new TemplateFileDesc(meta.flowFilename, meta.ChunkData.LongLength / 1024, null)
                };
            }
        }
    }
}
