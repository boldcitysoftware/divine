﻿using Divinity.Common.BaseClasses;
using Divinity.Models;
using Divinity.Models.Helpers;
using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Threading.Tasks;

namespace Divinity.Tools.Controllers.Api {
    [Authorize]
    public class FormTemplateFieldsController : UserController {
        public FormTemplateFieldsController() {
            db.Configuration.ProxyCreationEnabled = false;
        }
        private DivinityContext db = new DivinityContext();

        // DELETE api/FormTemplateFields/5
        public async Task<HttpResponseMessage> DeleteFormTemplateField(Guid id) {
            FormTemplateAcroFieldInfo acro = db.FormTemplateAcroFieldInfo.Find(id);
            if (acro == null) {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }
            byte[] file = await Storage.Cloud.BlobStorage.GetBytesAsync("template", acro.FormTemplateID.ToString());

            try {
                byte[] result = TemplateEngine.Utils.RemoveSpecificFieldFromPDF(file, acro.AcroFieldName);
                await Storage.Cloud.BlobStorage.StoreBytesAsync("template", acro.FormTemplateID.ToString(), result);
                db.SaveChanges();
                db.Database.ExecuteSqlCommand("EXEC DeleteFormTemplateAcroFieldInfo @p0",id);
            } catch (DbUpdateConcurrencyException ex) {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, ex);
            }

            return Request.CreateResponse(HttpStatusCode.OK, acro);
        }

        protected override void Dispose(bool disposing) {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}