﻿using Divinity.Common.BaseClasses;
using Divinity.Models;
using Divinity.Models.Helpers;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Divinity.Tools.Controllers.Api {
    public class PersonRolesController : UserController {
        // GET api/Fonts
        public IEnumerable<PersonRole> GetPersonRoles() {
            return db.PersonRole.OrderBy(pr => pr.RoleName).AsEnumerable();
        }
    }
}
