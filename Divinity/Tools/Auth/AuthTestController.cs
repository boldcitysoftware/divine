﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Divinity.Tools.Auth {
    public class AuthTestController : ApiController {
        // GET api/values
        [Authorize]
        public IEnumerable<AuthTest> Get() {
            return new AuthTest[] { new AuthTest { Valid = true } };
        }

        public class AuthTest {
            public bool Valid { get; set; }
        }
    }
}