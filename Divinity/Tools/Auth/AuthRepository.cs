﻿using System.Data.Entity;
using Divinity.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security.DataProtection;
using System.Web.Security;
using Divinity.Tools.Controllers.Api;

namespace Divinity.Tools.Auth {
    public class AuthRepository : IDisposable {

        public class MachineKeyProtectionProvider : IDataProtectionProvider {
            public IDataProtector Create(params string[] purposes) {
                return new MachineKeyDataProtector(purposes);
            }
        }

        public class MachineKeyDataProtector : IDataProtector {
            private readonly string[] _purposes;

            public MachineKeyDataProtector(string[] purposes) {
                _purposes = purposes;
            }

            public byte[] Protect(byte[] userData) {
                return MachineKey.Protect(userData, _purposes);
            }

            public byte[] Unprotect(byte[] protectedData) {
                return MachineKey.Unprotect(protectedData, _purposes);
            }
        }

        private AuthContext _authContext;
        private DivinityContext _context;
        private UserManager<IdentityUser> _userManager;

        public AuthRepository() {
            _authContext = new AuthContext();
            _context = new DivinityContext();

            _userManager = new UserManager<IdentityUser>(new UserStore<IdentityUser>(_authContext));
            var provider = new MachineKeyProtectionProvider();
            _userManager.UserTokenProvider = new DataProtectorTokenProvider<IdentityUser>(provider.Create("EmailConfirmation"));
        }

        public async Task<IdentityUser> FindByEmail(string email) {
            IdentityUser user = await _userManager.FindByEmailAsync(email);

            return user;
        }

        public async Task<IdentityUser> FindUser(string userName, string password) {
            IdentityUser user = await _userManager.FindAsync(userName, password);

            return user;
        }

        public void Dispose() {
            _authContext.Dispose();
            _context.Dispose();
            _userManager.Dispose();

        }
    }
}