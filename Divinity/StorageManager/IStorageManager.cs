﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Divinity.Storage {
    public interface IStorageManager {
        Task StoreBytesAsync(string containerID, string blobID, byte[] file);
        Task<byte[]> GetBytesAsync(string containerID, string blobID);

    }
}
