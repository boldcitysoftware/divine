﻿using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Divinity.Storage {
    public class AzureBlobStorage : IStorageManager {
        public async Task StoreBytesAsync(string containerID, string blobID, byte[] file) {
            blobID = blobID.ToLower();

            var account = CloudStorageAccount.Parse(System.Configuration.ConfigurationManager.AppSettings["StorageConnectionString"]);
            var client = account.CreateCloudBlobClient();
            var container = client.GetContainerReference(containerID);
            
            await container.CreateIfNotExistsAsync();
            await container.SetPermissionsAsync(new BlobContainerPermissions { PublicAccess = BlobContainerPublicAccessType.Off });

            var blob = container.GetBlockBlobReference(blobID);
            await blob.DeleteIfExistsAsync();
            await blob.UploadFromByteArrayAsync(file, 0, file.Length);
        }
        public async Task<byte[]> GetBytesAsync(string containerID, string blobID) {
            blobID = blobID.ToLower();
            var account = CloudStorageAccount.Parse(System.Configuration.ConfigurationManager.AppSettings["StorageConnectionString"]);
            var client = account.CreateCloudBlobClient();
            var container = client.GetContainerReference(containerID);

            try {
                bool exists = await container.ExistsAsync();
                if (!exists) {
                    return null;
                }

                var blob = container.GetBlockBlobReference(blobID);

                exists = await blob.ExistsAsync();
                if (!exists) {
                    return null;
                }
                using (var ms = new MemoryStream()) {
                    await blob.DownloadToStreamAsync(ms);
                    return ms.ToArray();
                }
            }
            catch (Exception ex) {
                ex.ToString();
                throw ex;
            }
        }
    }
}
