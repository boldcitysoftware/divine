﻿using GhostscriptSharp;
using GhostscriptSharp.Settings;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Drawing.Imaging;

namespace Divinity.PdfToPng {
    public class Converter {
        static Converter() {
        }

        public class PageImageInfo {
            public byte[] PngBytes { get; set; }
            public int PageNumber { get; set; }

            public int Width { get; set; }

            public int Height { get; set; }
        }

        /// <summary>
        /// Returns a PNG per page found in pdfStream
        /// </summary>
        /// <param name="pdfStream">A stream containing the PDF to convert</param>
        /// <returns></returns>
        public static IEnumerable<PageImageInfo> Convert(Stream pdfStream) {
            string pdfFile = Path.GetTempFileName();

            using (var fs = new FileStream(pdfFile, FileMode.Create, FileAccess.Write)) {
                pdfStream.CopyTo(fs);
            }
            return Convert(pdfFile);
        }

        /// <summary>
        /// Returns a PNG per page found in pdfStream
        /// </summary>
        /// <param name="pdfBytes">A byte[] containing the PDF to convert</param>
        /// <returns></returns>
        public static IEnumerable<PageImageInfo> Convert(byte[] pdfBytes) {
            string pdfFile = Path.GetTempFileName();
            File.WriteAllBytes(pdfFile, pdfBytes);
            return Convert(pdfFile);
        }

        /// <summary>
        /// Returns a PNG per page found in pdfStream
        /// </summary>
        /// <param name="pdfFile">The file path of the PDF to convert</param>
        /// <returns></returns>
        public static List<PageImageInfo> Convert(string pdfFile) {
            PdfReader reader = new PdfReader(pdfFile);
            var results = new List<PageImageInfo>();
            for (int pageIndex = 0; pageIndex < reader.NumberOfPages; pageIndex++) {
                int page = pageIndex + 1;
                GhostscriptSharp.Settings.GhostscriptPageSizes ghostScriptSize;
                iTextSharp.text.Rectangle size = reader.GetPageSize(page);
                if (size.Equals(iTextSharp.text.PageSize.LETTER)) {
                    ghostScriptSize = GhostscriptSharp.Settings.GhostscriptPageSizes.letter;
                }
                else if (size.Equals(iTextSharp.text.PageSize.LEGAL)) {
                    ghostScriptSize = GhostscriptSharp.Settings.GhostscriptPageSizes.legal;
                }
                else if (size.Equals(iTextSharp.text.PageSize.A4)) {
                    ghostScriptSize = GhostscriptSharp.Settings.GhostscriptPageSizes.a4;
                }
                else {
                    ghostScriptSize = GhostscriptSharp.Settings.GhostscriptPageSizes.UNDEFINED;
                }

                string outputFile = Path.GetTempFileName();
                var settings = new GhostscriptSettings();
                settings.Device = GhostscriptDevices.png16m;
                settings.Page = new GhostscriptPages();
                settings.Page.Start = page;
                settings.Page.End = page;
                settings.Resolution = new Size(96, 96);
                settings.Size = new GhostscriptPageSize();
                settings.Size.Native = ghostScriptSize;

                IntPtr gs = GhostscriptSharp.GhostscriptWrapper.GenerateOutput(pdfFile, outputFile, settings);


                System.Drawing.Image img = Image.FromFile(outputFile);
                using (MemoryStream ms = new MemoryStream()) {
                    img.Save(ms, ImageFormat.Png);
                    results.Add(new PageImageInfo {
                        PngBytes = ms.ToArray(),
                        Width = img.Width,
                        Height = img.Height,
                        PageNumber = page
                    });
                }

            }
            return results;
        }
    }


}