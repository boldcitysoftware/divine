﻿'use strict';
app.controller('nefarContractController', ['$scope', '$routeParams', 'flash', function ($scope, $routeParams, flash) {
    $scope.pageNumber = 1;
    $scope.savingSection = false;
    $scope.supplementalFields = {};

    var financingOptions = [
        'topmostSubform[0].Page1[0].cash_transaction[0]',
        'topmostSubform[0].Page1[0].loan_without_financing_contingency[0]',
        'topmostSubform[0].Page1[0].loan_as_marked_below_with_financing_contingency[0]'
    ];

    var loanTypes = [
        'topmostSubform[0].Page2[0].fha[0]',
        'topmostSubform[0].Page2[0].va[0]',
        'topmostSubform[0].Page2[0].conventional_or_usda[0]',
        'topmostSubform[0].Page2[0].other_financing[0]'
    ];

    var fhaField = 'topmostSubform[0].Page2[0].have_the_privilege_and_option_of_proceeding_with_consummation_of_this_Contract_without_regard_to_the[0]';

    var otherFinancingTypes = [
        'topmostSubform[0].Page2[0].mortgage_assumption[0]',
        'topmostSubform[0].Page2[0].seller_financing[0]'
    ];

    var dateFields = [
        'topmostSubform[0].Page1[0].additional_binder_deposit_deadline[0]',
        'topmostSubform[0].Page9[0].DATE_THIS',
        'topmostSubform[0].Page2[0].Agreement_unless_extended_by_other_conditions_of_this_Agreement_Marketable_title_means_title_which[0]'
    ];

    var deadlineTimeField = 'OFFER_WILL_TERMINATE_THE_TIME_FOR_ACCEPTANCE_OF_ANY_COUNTER_OFFER_SHALL_BE';
    var deadlineAM = 'topmostSubform[0].Page9[0].AM';
    var deadlinePM = 'topmostSubfoSrm[0].Page9[0].PM';

    $scope.initSupplementalFields = function () {
        $scope.convertToSelection(financingOptions, 'financingOption');
        $scope.convertToSelection(loanTypes, 'loanType');
        $scope.convertToSelection(otherFinancingTypes, 'otherFinancingType');

        $scope.convertFieldsToDates(dateFields);

        // deal with the date stuff.
        var hour = $scope.getFormField(deadlineTimeField).fieldValue;
        if (hour) {
            hour = parseInt(hour);
            if ($scope.getFormField(deadlinePM).fieldValue == 'On') {
                hour += 12;
            } else if (hour == 12) {
                hour -= 12;
            }
            
            var newVal = new Date();
            newVal.setHours(hour);

            $scope.supplementalFields.acceptanceDeadlineTime = newVal;
        }
    };

    $scope.commitSupplementalFields = function () {
        $scope.convertSelectionToAcroField(financingOptions, $scope.supplementalFields.financingOption);
        $scope.convertSelectionToAcroField(loanTypes, $scope.supplementalFields.loanType);
        $scope.convertSelectionToAcroField(otherFinancingTypes, $scope.supplementalFields.otherFinancingType);

        $scope.convertDatesToFields(dateFields);

        $scope.getFormField(deadlineAM).fieldValue = 'Off';
        $scope.getFormField(deadlinePM).fieldValue = 'Off';

        var deadlineTimeDate = $scope.supplementalFields.acceptanceDeadlineTime;

        if (deadlineTimeDate) {
            var hour = deadlineTimeDate.getHours();
            if (hour > 12) {
                hour -= 12;
                $scope.getFormField(deadlinePM).fieldValue = 'On';
            } else {
                $scope.getFormField(deadlineAM).fieldValue = 'On';
            }

            if (hour == 0) {
                hour = 12;
            }

            $scope.getFormField(deadlineTimeField).fieldValue = hour.toString();
        }

        if ($scope.getFormField('topmostSubform[0].Page2[0].Agreement_unless_extended_by_other_conditions_of_this_Agreement_Marketable_title_means_title_which[0]').fieldValue) {
            $scope.getFormField('topmostSubform[0].Page2[0].including_legal_access_the_transaction_will_be_closed_and_the_deed_and_other_closing_papers_delivered[0]').fieldValue = 'On';
        } else {
            $scope.getFormField('topmostSubform[0].Page2[0].including_legal_access_the_transaction_will_be_closed_and_the_deed_and_other_closing_papers_delivered[0]').fieldValue = 'Off';
        }

        if ($scope.getFormField('topmostSubform[0].Page2[0].days_after_date_of_acceptance_of_this[0]').fieldValue) {
            $scope.getFormField('topmostSubform[0].Page2[0].including_legal_access_the_transaction_will_be_closed_and_the_deed_and_other_closing_papers_delivered[1]').fieldValue = 'On';
        } else {
            $scope.getFormField('topmostSubform[0].Page2[0].including_legal_access_the_transaction_will_be_closed_and_the_deed_and_other_closing_papers_delivered[1]').fieldValue = 'Off';
        }
    };

    $scope.convertDatesToFields = function (fields) {
        for (var i = 0; i < fields.length; i++) {
            var date = $scope.getFormField(fields[i]).fieldValue;
            if (date) {
                $scope.getFormField(fields[i]).fieldValue = new Date(date).toLocaleDateString();
            }
        }
    };

    $scope.convertFieldsToDates = function (fields) {
        for (var i = 0; i < fields.length; i++) {
            if ($scope.getFormField(fields[i]).fieldValue) {
                $scope.getFormField(fields[i]).fieldValue = new Date($scope.getFormField(fields[i]).fieldValue);
            }
        }
    };

    $scope.financingOptionChanged = function () {
        for (var i = 0; i < loanTypes.length; i++) {
            $scope.getFormField(loanTypes[i]).fieldValue = 'Off';
        }

        for (var i = 0; i < otherFinancingTypes.length; i++) {
            $scope.getFormField(otherFinancingTypes[i]).fieldValue = 'Off';
        }
        
        $scope.getFormField(fhaField).fieldValue = '';
        $scope.supplementalFields.loanType = null;
        $scope.supplementalFields.otherFinancingType = null;
    };

    $scope.loanTypeChanged = function () {
        $scope.getFormField(fhaField).fieldValue = '';
        for (var i = 0; i < otherFinancingTypes.length; i++) {
            $scope.getFormField(otherFinancingTypes[i]).fieldValue = 'Off';
        }
        $scope.supplementalFields.otherFinancingType = null;
    };

    $scope.convertSelectionToAcroField = function (acroFields, selectedValue) {
        $.each(acroFields, function (index, acroField) {
            $scope.getFormField(acroField).fieldValue = 'Off';
        });
        if (selectedValue) {
            $scope.getFormField(selectedValue).fieldValue = 'On';
        }
    };

    $scope.convertToSelection = function (acroFields, supplementalField) {
        for (var i = 0; i < acroFields.length; i++) {
            var val = $scope.getFormField(acroFields[i]).fieldValue;
            if (val === "On") {
                $scope.supplementalFields[supplementalField] = acroFields[i];
                break;
            }
        }
    };

    $scope.goNext = function () {
        $scope.goPage($scope.pageNumber + 1);
    };

    $scope.goBack = function () {
        $scope.goPage($scope.pageNumber - 1);
    };

    $scope.goPage = function (page) {
        $scope.savingSection = true;

        $scope.commitSupplementalFields();

        $scope.saveFields(true).then(function () {
            $scope.savingSection = false;
            $scope.go('/deals/' + $routeParams.dealID + '/forms/' + $routeParams.formID + '/' + page);
        });
    };

    $scope.clearBinderDueWithin = function () {
        $scope.getFormField('topmostSubform[0].Page1[0].binder_deposit_due_within[0]').fieldValue = '';
    };

    var init = function () {
        if ($routeParams.pageNumber) {
            $scope.pageNumber = parseInt($routeParams.pageNumber);
        }
        $scope.initSupplementalFields();
    };

    init();
}]);