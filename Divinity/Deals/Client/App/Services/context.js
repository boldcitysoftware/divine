﻿angular.module('context', [])
    .service('webApiLayer', ['$resource', '$q', function ($resource, $q) {
        var API = function (entity) {
            return $resource('/api/' + entity + '/:id', {}, { getOne: { method: 'GET', isArray: false }, getByID: { method: 'GET', isArray: false }, update: { method: 'PUT' }, insert: { method: 'POST' }, destroy: { method: 'DELETE' } });
        };

        this.find = function (entity, model, id) {
            var api = new API(entity);
            return api.getByID({ id: id }).$promise.then(function (entity) {
                var result = new model(entity);
                return result;
            }, function (res) {
                return $q.reject(res);
            });
        };

        this.query = function (entity, model, params, returnsArray) {
            if (returnsArray !== false) {
                returnsArray = true;
            }
            params = params || { };
            var api = new API(entity);

            if (params.id && !returnsArray) {
                return api.getByID(params).$promise.then(function (entity) {
                    var result = new model(entity);
                    return result;
                }, function (res) {
                    return $q.reject(res);
                });
            } else if (!returnsArray) {
                return api.getOne(params).$promise.then(function (data) {
                    return new model(data);
                }, function (res) {
                    return $q.reject(res);
                });
            } else {
                return api.query(params).$promise.then(function (data) {
                    var result = [];
                    angular.forEach(data, function (element) {
                        result.push(new model(element));
                    });
                    return result;
                }, function (res) {
                    return $q.reject(res);
                });
            }
        };

        this.update = function (entity, id, element, params) {
            if (params) {
                params.id = id;
            } else {
                params = {
                    id: id
                };
            }
            return new API(entity).update(params, element).$promise;
        };

        this.insert = function (entity, element, params) {
            params = params || {};
            return new API(entity).insert(params, element).$promise;
        };

        this.destroy = function (entity, id, params) {
            if (!params) {
                params = {};
            }
            params.id = id;
            return new API(entity).destroy(params).$promise;
        }
    }]);