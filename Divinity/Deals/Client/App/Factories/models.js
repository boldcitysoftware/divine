﻿var mods = angular.module('models', []);

mods.factory('models', ['webApiLayer', '$resource', function (api, $resource) {
    var models = {};

    models.documentSigning = {
        recordEvent: function(signingEvent, requestID){
            var eventApi = $resource('/api/docsigning/' + signingEvent + '/' + requestID, {}, { send: { method: 'PUT' } });
            return eventApi.send().$promise;
        }
    };

    // begin Document
    models.Document = function (extender) {
        this.documentID = '00000000-0000-0000-0000-000000000000';

        angular.extend(this, extender);
    };

    models.Document.entity = function () { return "Documents"; };

    models.Document.find = function (id) {
        return api.find(models.Document.entity(), models.Document, id);
    };

    models.Document.query = function (params) {
        params = params || {};
        return api.query(models.Document.entity(), models.Document, params);
    };

    models.Document.prototype.save = function () {
        return api.update(models.Document.entity(), this.documentID, this);
    };

    models.Document.prototype.destroy = function () { return api.destroy(models.Document.entity(), this.documentID); };
    // end Document

    // begin SignatureInfo
    models.SignatureInfo = function (extender) {
        this.SignatureInfoID = '00000000-0000-0000-0000-000000000000';
        this.formTemplateID = '00000000-0000-0000-0000-000000000000';
        this.roleName = '';
        this.signatureType = 'Signature';

        this.x = 50;
        this.y = 50;
        this.width = 200;
        this.height = 16;

        this.pageNumber = 1;

        this.hasDate = false;
        this.dateX = '';
        this.dateY = '';
        this.dateWidth = '';
        this.dateHeight = '';

        angular.extend(this, extender);
    };
    // end SignatureInfo

    // begin Signature
    models.Signature = function (extender) {
        this.signatureID = '00000000-0000-0000-0000-000000000000';
        this.requestID = '00000000-0000-0000-0000-000000000000';
        this.signatureInfoID = '00000000-0000-0000-0000-000000000000';
        this.fontName = '';
        this.signatureText = '';
        this.executedAt = null;

        angular.extend(this, extender);
    };

    models.Signature.entity = function () { return "Signatures"; };

    models.Signature.saveAll = function (signatures) {
        return api.insert(models.Signature.entity(), signatures);
    };
    // end Signature

    // begin SignatureRequest
    models.SignatureRequest = function (extender) {
        this.requestID = '00000000-0000-0000-0000-000000000000';
        this.dealID = '00000000-0000-0000-0000-000000000000';
        this.documentID = '00000000-0000-0000-0000-000000000000';
        this.signerPersonID = '00000000-0000-0000-0000-000000000000';
        this.signerEmail = '';


        this.revoked = false;
        angular.extend(this, extender);
    };

    models.SignatureRequest.entity = function () { return "SignatureRequests"; };

    models.SignatureRequest.query = function (params) {
        params = params || {};
        return api.query(models.SignatureRequest.entity(), models.SignatureRequest, params, !params.id);
    };

    models.SignatureRequest.prototype.save = function (params) {
        return api.insert(models.SignatureRequest.entity(), this, params);
    };

    models.SignatureRequest.prototype.destroy = function () { return api.destroy(models.SignatureRequest.entity(), this.requestID); };
    // end SignatureRequest

    // begin DealAccess
    models.DealAccess = function (extender) {
        this.accessID = '00000000-0000-0000-0000-000000000000';
        this.dealID = '00000000-0000-0000-0000-000000000000';
        this.personID = '00000000-0000-0000-0000-000000000000';
        this.revoked = false;

        angular.extend(this, extender);
    };

    models.DealAccess.entity = function () { return "DealAccess"; };

    models.DealAccess.query = function (dealID, personID) {
        var params = { dealID: dealID };
        if (personID) {
            params.personID = personID;
        }
        return api.query(models.DealAccess.entity(), models.DealAccess, params, !params.personID);
    };

    models.DealAccess.prototype.save = function () {
        return api.insert(models.DealAccess.entity(), this);
    };
    models.DealAccess.prototype.destroy = function () { return api.destroy(models.DealAccess.entity(), this.accessID); };
    // end DealAccess

    // begin PersonRole
    models.PersonRole = function (extender) {
        this.roleName = '';
        this.roleDescription = '';

        angular.extend(this, extender);
    };

    models.PersonRole.entity = function () { return "PersonRoles"; };

    models.PersonRole.query = function (params) {
        params = params || {};
        return api.query(models.PersonRole.entity(), models.PersonRole, params, !params.id);
    };

    // end PersonRole

    // begin DealAudit
    models.DealAudit = function (extender) {
        this.actionTime = '';
        this.friendlyMessage = '';

        angular.extend(this, extender);
    };

    models.DealAudit.entity = function () { return "DealAudit"; };

    models.DealAudit.query = function (dealID) {
        return api.query(models.DealAudit.entity(), models.DealAudit, {id:dealID});
    };
    // end DealAudit

    // begin Form
    models.Form = function (extender) {
        this.formID = '00000000-0000-0000-0000-000000000000';
        this.formTemplateID = null;
        this.dealID = null;
        this.displayName = '';
        this.formType = '';

        angular.extend(this, extender);
    };

    models.Form.entity = function () { return "Forms"; };

    models.Form.query = function (params) {
        params = params || {};
        return api.query(models.Form.entity(), models.Form, params, !params.id);
    };

    models.Form.prototype.save = function (params) {
        if (this.formID && this.formID != '00000000-0000-0000-0000-000000000000') {
            return api.update(models.Form.entity(), this.formID, this, params);
        } else {
            return api.insert(models.Form.entity(), this);
        }
    };

    models.Form.prototype.destroy = function () { return api.destroy(models.Form.entity(), this.formID); };
    // end Form

    // begin FormTemplate
    models.FormTemplate = function (extender) {
        this.formTemplateID = '00000000-0000-0000-0000-000000000000';
        this.displayName = '';
        this.formType = '';
        this.jurisdiction = '';
        angular.extend(this, extender);
    };

    models.FormTemplate.entity = function () { return "formTemplates"; };

    models.FormTemplate.query = function (params) {
        params = params || {};
        return api.query(models.FormTemplate.entity(), models.FormTemplate, params, !params.id);
    };
    // end FormTemplate

    // begin CouponCheck
    models.CouponCheck = function (extender) {
        this.couponCode = '';
        this.discountPercent = 0;
        this.discountedPrice = 0;

        angular.extend(this, extender);
    };

    models.CouponCheck.entity = function () { return "ValidateCoupon"; };

    models.CouponCheck.query = function (couponCode) { return api.query(models.CouponCheck.entity(), models.CouponCheck, { couponCode: couponCode }, false); };
    // end CouponCheck

    // begin Subscription
    models.Subscription = function (extender) {
        this.cardName = '';
        this.cardNumber = '';
        this.cardCvc = '';
        this.cardExpirationYear = new Date().getFullYear().toString();
        this.cardExpirationMonth = '01';
        this.cardAddressLine1 = '';
        this.cardAddressLine2 = '';
        this.cardAddressCity = '';
        this.cardAddressState = '';
        this.cardAddressZip = '';

        angular.extend(this, extender);
    };

    models.Subscription.entity = function () { return "Subscribe"; };

    models.Subscription.prototype.save = function () {
        return api.insert(models.Subscription.entity(), this);
    };
    // end Subscription

    // begin SubscriptionCancellation
    models.SubscriptionCancellation = function (extender) {
        this.reason = '';

        angular.extend(this, extender);
    };

    models.SubscriptionCancellation.entity = function () { return "Unsubscribe"; };

    models.SubscriptionCancellation.prototype.save = function () {
        return api.insert(models.SubscriptionCancellation.entity(), this);
    };
    // end SubscriptionCancellation

    // begin Font
    models.Font = function (extender) {
        this.name = '';
        this.relativePath = '';
        this.cssFamily = '';

        angular.extend(this, extender);
    };

    models.Font.entity = function () { return "Fonts"; };

    models.Font.query = function () { return api.query(models.Font.entity(), models.Font, {}); };
    // end Font

    // begin Deal
    models.Deal = function (extender) {
        this.dealID = '00000000-0000-0000-0000-000000000000';
        this.displayName = '';
        this.jurisdiction = '';
        this.transactionType = 'Purchase/Sale';
        this.transactionStatus = 'Draft';

        angular.extend(this, extender);
    };

    models.Deal.entity = function () { return "Deals"; };

    models.Deal.query = function (params) {
        params = params || {};
        return api.query(models.Deal.entity(), models.Deal, params, !params.id);
    };

    models.Deal.find = function (id) {
        return api.find(models.Deal.entity(), models.Deal, id);
    };

    models.Deal.prototype.save = function () {
        if (this.dealID && this.dealID != '00000000-0000-0000-0000-000000000000') {
            return api.update(models.Deal.entity(), this.dealID, this);
        } else {
            return api.insert(models.Deal.entity(), this);
        }
    };

    models.Deal.prototype.destroy = function () { return api.destroy(models.Deal.entity(), this.dealID); };
    // end Deal

    // begin Property
    models.Property = function (extender) {
        this.propertyID = '00000000-0000-0000-0000-000000000000';
        this.taxID = '';
        this.address1 = '';
        this.address2 = '';
        this.city = '';
        this.county = '';
        this.state = 'FL';
        this.zipCode = '';
        this.legalDescription = '';

        angular.extend(this, extender);
    };
    models.Property.prototype.getAddressInline = function () {
        var address = this.address1;
        if (this.address2) {
            address += ', ' + this.address2;
        }
        address += ', ' + this.city;
        address += ', ' + this.county;
        address += ', ' + this.state;
        address += ' ' + this.zipCode;
        return address;
    };

    models.Property.prototype.getAddressLine1 = function () {
        var address = this.address1;
        if (this.address2) {
            address += ', ' + this.address2;
        }
        return address;
    };
    models.Property.prototype.getAddressLine2 = function () {
        var address = this.city;
        address += ', ' + this.county;
        address += ', ' + this.state;
        address += ' ' + this.zipCode;
        return address;
    };

    models.Property.entity = function () { return "Properties"; };

    models.Property.find = function (id) {
        return api.find(models.Property.entity(), models.Property, id);
    };

    models.Property.query = function (params) { return api.query(models.Property.entity(), models.Property, params); };

    models.Property.prototype.save = function () {
        if (this.propertyID && this.propertyID != '00000000-0000-0000-0000-000000000000') {
            return api.update(models.Property.entity(), this.propertyID, this);
        } else {
            return api.insert(models.Property.entity(), this);
        }
    };

    models.Property.prototype.destroy = function () { return api.destroy(models.Property.entity(), this.propertyID); };
    // end Property

    // begin Person
    models.Person = function (extender) {
        this.personID = '00000000-0000-0000-0000-000000000000';
        this.firstName = '';
        this.lastName = '';
        this.address1 = '';
        this.address2 = '';
        this.city = '';
        this.state = 'FL';
        this.zipCode = '';
        this.email = '';
        this.mobileNumber = '';
        this.roleName = '';
        this.dealID = '00000000-0000-0000-0000-000000000000';

        angular.extend(this, extender);
    };

    models.Person.entity = function () { return "People"; };
    models.Person.find = function (id) {
        return api.find(models.Person.entity(), models.Person, id);
    };
    models.Person.query = function (params) { return api.query(models.Person.entity(), models.Person, params); };

    models.Person.prototype.save = function () {
        if (this.personID && this.personID != '00000000-0000-0000-0000-000000000000') {
            return api.update(models.Person.entity(), this.personID, this);
        } else {
            return api.insert(models.Person.entity(), this);
        }
    };

    models.Person.prototype.destroy = function () { return api.destroy(models.Person.entity(), this.personID); };
    // end Person

    // begin PersonalInfo
    models.PersonalInfo = function (extender) {
        this.firstName = '';
        this.lastName = '';
        this.email = '';

        angular.extend(this, extender);
    };

    models.PersonalInfo.entity = function () { return "PersonalInfo"; };

    models.PersonalInfo.find = function (params) { return api.find(models.PersonalInfo.entity(), models.PersonalInfo, params); };

    models.PersonalInfo.prototype.save = function () {
        return api.update(models.PersonalInfo.entity(), '0', this);
    };
    // end PersonalInfo

    // begin UserSignatureInfo
    models.UserSignatureInfo = function (extender) {
        this.signatureText = '';
        this.initialsText = '';
        this.signatureFont = '';
        this.initialsFont = '';

        angular.extend(this, extender);
    };

    models.UserSignatureInfo.entity = function () { return "UserSignatureInfo"; };

    models.UserSignatureInfo.query = function () { return api.query(models.UserSignatureInfo.entity(), models.UserSignatureInfo, {}, false); };

    models.UserSignatureInfo.prototype.save = function () {
        return api.update(models.UserSignatureInfo.entity(), '0', this);
    };
    // end UserSignatureInfo

    // begin SubscriptionInfo
    models.SubscriptionInfo = function (extender) {
        angular.extend(this, extender);
    };

    models.SubscriptionInfo.entity = function () { return "SubscriptionStatus"; };

    models.SubscriptionInfo.query = function () { return api.query(models.SubscriptionInfo.entity(), models.SubscriptionInfo, {}, false); };
    // end SubscriptionInfo

    // begin TaskList
    models.TaskList = function (extender) {
        this.taskListID = '00000000-0000-0000-0000-000000000000';
        this.dealID = '00000000-0000-0000-0000-000000000000';
        this.displayName = '';
            
        angular.extend(this, extender);
    };

    models.TaskList.entity = function () { return "TaskLists"; };

    models.TaskList.query = function (params) { return api.query(models.TaskList.entity(), models.TaskList, params, !params.id); };

    models.TaskList.prototype.save = function () {
        if (this.taskListID && this.taskListID != '00000000-0000-0000-0000-000000000000') {
            return api.update(models.TaskList.entity(), this.taskListID, this);
        } else {
            return api.insert(models.TaskList.entity(), this);
        }
    };

    models.TaskList.prototype.destroy = function () { return api.destroy(models.TaskList.entity(), this.taskListID); };
    // end TaskList

    // begin Task
    models.Task = function (extender) {
        this.taskID = '00000000-0000-0000-0000-000000000000';
        this.taskListID = '00000000-0000-0000-0000-000000000000';
        this.displayName = 'New Task';
        this.dueAt = new Date();

        angular.extend(this, extender);
    };

    models.Task.entity = function () { return "Tasks"; };

    models.Task.query = function (params) { return api.query(models.Task.entity(), models.Task, params, !params.id); };

    models.Task.prototype.save = function () {
        if (this.taskID && this.taskID != '00000000-0000-0000-0000-000000000000') {
            return api.update(models.Task.entity(), this.taskID, this);
        } else {
            return api.insert(models.Task.entity(), this);
        }
    };

    models.Task.prototype.destroy = function () { return api.destroy(models.Task.entity(), this.taskID); };
    // end Task

    return models;
}]);