﻿var mods = angular.module('data', []);

mods.factory('api', ['$resource', function ($resource) {
    var api = {};

    api.documentSigning = {
        recordEvent: function(signingEvent, requestID){
            var rest = $resource('/api/docsigning/' + signingEvent + '/' + requestID, {}, { send: { method: 'PUT' } });
            return rest.send().$promise;
        },
    };

    api.documentVerifier = $resource('/api/docsigning/verify/:documentID', { documentID: '@id' });

    api.userSignature = $resource('/api/usersignatureinfo/:userID', { userID: '@id' });
    api.userID = $resource('/api/userid');

    return api;
}]);