﻿'use strict';
app.controller('mySignatureController', ['$scope','models','flash', function ($scope, models, flash) {
    // models
    $scope.fonts = [];
    $scope.userSignatureInfo = {};

    $scope.loaded = false;
    $scope.saving = false;

    // api calls
    $scope.save = function () {
        $scope.saving = true;
        $scope.userSignatureInfo.save().then(function () {
            flash.success = 'Signature saved.';
            $scope.saving = false;
        });
    };

    // helpers
    $scope.getStyle = function (font) {
        if (!font) {
            font = {};
            font.cssFamily = '';
        }
        return {
            'font-family': font.cssFamily,
            'font-size':'24px'
        };
    };
    $scope.getFont = function (name) {
        return $.grep($scope.fonts, function (font) { return name === font.name; })[0];
    };

    $scope.setInitialFont = function (font) {
        $scope.userSignatureInfo.initialsFont = font.name;
    }

    $scope.setSignatureFont = function (font) {
        $scope.userSignatureInfo.signatureFont = font.name;
    }

    // setup
    var init = function () {
        return models.Font.query().then(function (fonts) {
            return $scope.fonts = fonts;
        }).then(function () {
            return models.UserSignatureInfo.query().then(function (userSignatureInfo) {
                if (!userSignatureInfo.userID) {
                    return models.PersonalInfo.find().then(function (personalInfo) {
                        userSignatureInfo = new models.UserSignatureInfo();
                        if (personalInfo.firstName && personalInfo.lastName) {
                            userSignatureInfo.signatureText = personalInfo.firstName + ' ' + personalInfo.lastName;
                            userSignatureInfo.initialsText = personalInfo.firstName.substring(0, 1) + '.' + personalInfo.lastName.substring(0, 1) + '.';
                        } else {
                            userSignatureInfo.signatureText = 'John';
                            userSignatureInfo.initialsText = 'Hancock';
                        }
                        userSignatureInfo.signatureFont = userSignatureInfo.initialsFont = $scope.fonts[0].name;
                        $scope.userSignatureInfo = userSignatureInfo;
                        $scope.loaded = true;
                        return $scope.userSignatureInfo.save();
                    });
                } else {
                    $scope.loaded = true;
                    return $scope.userSignatureInfo = userSignatureInfo;
                }
            });
        });
    };
    init();
}]);