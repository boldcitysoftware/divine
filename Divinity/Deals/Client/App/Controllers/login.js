﻿'use strict';
app.controller('loginController', ['$rootScope', '$scope', 'authService','flash','$routeParams', function ($rootScope, $scope, authService, flash, $routeParams) {
    $rootScope.hideMenu = true;
    $scope.showLogin = false;
    $scope.loginData = {
        userName: "",
        password: ""
    };

    $scope.login = function (dealID) {
        $scope.loggingOn = true;
        return authService.login($scope.loginData).then(function (response) {
            $rootScope.hideMenu = false;
            flash.error = '';

            if (dealID) {
                $scope.go('/deals/' + dealID);
            } else if ($rootScope.forbiddenPath) {
                $scope.go($rootScope.forbiddenPath);
            }
            else {
                $scope.go('/deals');
            }
            
        }, function (err) {
            $scope.loggingOn = false;
            $scope.showLogin = true;
            $scope.showAutoLogin = false;
            flash.error = err.error_description || 'An error occurred.';
        });
    };

    var init = function () {
        if ($routeParams.token) {
            $scope.loginData.userName = $routeParams.email;
            $scope.loginData.password = $routeParams.password;
            var dealID = $routeParams.dealID;
            authService.confirmAccount({ userId: $routeParams.userId, token: $routeParams.token }).then(function () {
                return $scope.login(dealID);
            });
            $scope.showAutoLogin = true;
        } else {
            $scope.showLogin = true;
            
        }
    };
    init();
}]);