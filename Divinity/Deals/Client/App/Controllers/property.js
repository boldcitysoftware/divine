﻿angular.module('main').controller('propertyController', ['$scope', 'models', '$routeParams', 'flash', function ($scope, models, $routeParams, flash) {
    $scope.property = {};
    $scope.savingProperty = false;
    $scope.refreshingProperty = false;
    $scope.dealID = $routeParams.dealID;
    // form events
    $scope.saveProperty = function () {
        $scope.savingProperty = true;
        $scope.property.save().then(function () {
            $scope.savingProperty = false;
            flash.success = 'Property info saved.';
            $scope.$back();
        });
    };

    // init/page setup
    var init = function () {
        return models.Property.find($routeParams.propertyID).then(function (property) {
            $scope.property = property;
        });
    };

    init();
}]);