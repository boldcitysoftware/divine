﻿angular.module('main').controller('newDealController', ['$scope', 'models','flash','$q', function ($scope, models, flash, $q) {
    //models
    $scope.deal = new models.Deal();
    $scope.property = new models.Property();
    $scope.saving = false;

    // form submission
    $scope.saveDealInfo = function () {
        $scope.saving = true;
        // first the deal
        return $scope.deal.save().then(function (dealData) {
            var allSaves = [];
            $scope.deal.dealID = dealData.dealID;
            $scope.property.dealID = $scope.deal.dealID;

            // now all the extras
            allSaves.push($scope.property.save());
            
            return $q.all(allSaves).then(function (b2Data) {
                $scope.saving = false;
                flash.success = 'Deal "' + $scope.deal.displayName + '" created.';
                return $scope.go('/deals/' + $scope.deal.dealID);
            });
            
        }, function (error) {
            flash.error = 'Error while creating deal. Please try again or contact support.';
        });
    };
}]);