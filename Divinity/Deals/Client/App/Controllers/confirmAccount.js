﻿angular.module('main').controller('confirmAccountController', ['$scope', 'authService', '$routeParams', '$rootScope','flash', function ($scope, authService, $routeParams, $rootScope,flash) {
    
    $rootScope.hideMenu = true;
    // init/page setup
    var init = function () {
        authService.confirmAccount({userId: $routeParams.userId, token:$routeParams.token}).then(function (response) {
            $scope.savedSuccessfully = true;
            flash.warn = "Your account has been confirmed! You may now sign in.";
            $scope.go('/login');

        });
    };

    init();
}]);