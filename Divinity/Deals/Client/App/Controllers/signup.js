﻿'use strict';
app.controller('signupController', ['$rootScope', '$scope', 'authService','flash', function ($rootScope, $scope, authService, flash) {
    $rootScope.hideMenu = true;
    $scope.saving = false;

    $scope.registration = {
        userName: "",
        password: "",
        confirmPassword: ""
    };

    $scope.signUp = function () {

        $scope.saving = true;
        authService.saveRegistration($scope.registration).then(function (response) {
            $scope.saving = false;
            $scope.registered = true;

        },
         function (response) {
             var errors = [];
             for (var key in response.data.modelState) {
                 for (var i = 0; i < response.data.modelState[key].length; i++) {
                     errors.push(response.data.modelState[key][i]);
                 }
             }
             flash.error = "Error during registration: " + errors.join(' ');
         });
    };
}]);