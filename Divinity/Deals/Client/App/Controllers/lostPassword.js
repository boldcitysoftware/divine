﻿angular.module('main').controller('lostPasswordController', ['$scope', 'authService', '$routeParams', '$rootScope', function ($scope, authService, $routeParams, $rootScope) {
    $scope.email = '';
    $rootScope.hideMenu = true;
    
    $scope.sendRecoveryEmail = function () {
        $scope.saving = true;
        authService.sendRecoveryEmail({ email: $scope.email }).then(function (result) {
            $scope.saving = false;
            $scope.emailSent = true;
        });
    };
}]);