﻿'use strict';
app.controller('signatureCollectionController', ['$rootScope', '$scope', 'models', 'flash', '$routeParams','$q','api', function ($rootScope,$scope, models, flash, $routeParams,$q,api) {
    // models
    $scope.fonts = [];
    $scope.signatureRequest = [];
    $rootScope.hideMenu = true;
    $scope.signatureAdopted = false;
    $scope.consentGiven = false;
    $scope.documentConfirmed = false;
    $scope.mySignature = [];

    $scope.requestID = $routeParams.requestID;
    $scope.userID = $routeParams.userID;

    $scope.requester = [];
    $scope.loaded = false;

    // signatures
    $scope.signaturePositions = [];
    $scope.signatures = [];
    
    // form submissions
    $scope.giveConsent = function () {
        models.documentSigning.recordEvent('consentGiven', $scope.requestID).then(function () {
            $scope.consentGiven = true;
        }, function () {
            flash.error = 'Something went wrong. Please try again.';
        });
    };

    $scope.adoptSignature = function () {
        models.documentSigning.recordEvent('signatureAdopted', $scope.requestID).then(function () {
            $scope.signatureAdopted = true;
        }, function () {
            flash.error = 'Something went wrong. Please try again.';
        });
    };

    $scope.confirmDocument = function () {
        models.Signature.saveAll($scope.signatures)
            .then(function () {
                $scope.documentConfirmed = true;
            });
    };
    
    $scope.confirmButtonEnabled = function () {
        var unsignedPositions = $.grep($scope.signaturePositions, function (position) {
            return !position.signed;
        });
        return unsignedPositions.length === 0;
    };

    // click events
    $scope.sign = function (position) {
        var sig = new models.Signature();
        sig.requestID = $scope.requestID;
        sig.signatureInfoID = position.signatureInfoID;
        sig.dealID = $scope.signatureRequest.dealID;
        if (position.signatureType == 'Signature') {
            sig.fontName = $scope.mySignature.signatureFont;
            sig.signatureText = $scope.mySignature.signatureText;
        } else {
            sig.fontName = $scope.mySignature.initialsFont;
            sig.signatureText = $scope.mySignature.initialsText;
        }

        sig.executedAt = new Date();

        $scope.signatures.push(sig);
        position.signed = true;
    };

    // helpers
    $scope.getStyle = function (font) {
        if (!font) {
            font = {};
            font.cssFamily = '';
        }
        return {
            'font-family': font.cssFamily,
            'font-size': '24px'
        };
    };

    $scope.getSignatureForPosition = function (position) {
        var sig = $.grep($scope.signatures, function (signature) {
            return signature.signatureInfoID == position.signatureInfoID;
        })[0];
        return sig;
    };

    $scope.getSignatureTextStyle = function (position) {
        if (position.signed) {
            var sig = $scope.getSignatureForPosition(position);
            if (sig) {
                return {
                    'font-family': $scope.getFont(sig.fontName).cssFamily
                };
            }
        }
    };

    $scope.getSignatureText = function (position) {
        if (position.signed) {
            var sig = $scope.getSignatureForPosition(position);
            if (sig) {
                return sig.signatureText;
            }
        }
    };

    $scope.getSignatureDate = function (position) {
        if (position.signed) {
            var sig = $scope.getSignatureForPosition(position);
            if (sig) {
                return sig.executedAt;
            }
        }
    };


    $scope.getFont = function (name) {
        return $.grep($scope.fonts, function (font) { return name === font.name; })[0];
    };

    $scope.setInitialFont = function (font) {
        $scope.mySignature.initialsFont = font.name;
    }

    $scope.setSignatureFont = function (font) {
        $scope.mySignature.signatureFont = font.name;
    }

    $scope.getSignaturesOnPage = function (pageNumber) {
        if ($scope.signaturePositions) {
            return $.grep($scope.signaturePositions, function (position) {
                return position.pageNumber === pageNumber;
            });
        }
    };

    $scope.getContainerStyleForPage = function (pageNumber) {
        if ($scope.pages) {
            var page = $.grep($scope.pages, function (p) {
                return p.pageNumber == pageNumber;
            })[0];
            if (page) {
                return {
                    position:'relative',
                    border:'1px solid #777',
                    margin:'auto',
                    width: (page.pageWidth + 2) + 'px',
                    height: (page.pageHeight + 2) + 'px'
                };
            }
        }
    }

    $scope.getStyleForPage = function (pageNumber) {
        if ($scope.pages) {
            var page = $.grep($scope.pages, function (p) {
                return p.pageNumber == pageNumber;
            })[0];
            if (page) {
                return {
                    position: 'relative',
                    margin: 'auto',
                    width: (page.pageWidth) + 'px',
                    height: (page.pageHeight) + 'px',
                    background: "url('/api/DownloadBlobPng/" + page.pageID + "?container=page-image')"
                };
            }
        }
    }

    $scope.getSignatureStyle = function (position) {
        return {
            position: 'absolute',
            bottom: (position.fieldY - position.fieldHeight)  + 'px',
            left: 0,
            'margin-left': position.fieldX + 'px',
            width: position.fieldWidth + 'px',
            height:position.fieldHeight + 'px',
            'line-height': position.fieldHeight + 'px',
            'font-size': position.fieldHeight + 'px',
            background: '#FFFFC0'
        };
    };

    $scope.getSignatureButtonStyle = function (position, pageNumber) {
        if ($scope.pages) {
            var page = $.grep($scope.pages, function (p) {
                return p.pageNumber == pageNumber;
            })[0];
            if (page) {
                return {
                    position: 'absolute',
                    top: (page.pageHeight - position.fieldY - 7) + 'px',
                    left: (position.fieldX - 60) + 'px',
                    width: position.fieldWidth + 'px',
                    'line-height': position.fieldHeight + 'px',
                    height: position.fieldHeight + 'px'
                };
            }
        }
    };

    $scope.getDateStyle = function (position) {
        if (position.hasDate) {
            return {
                position: 'absolute',
                bottom: (position.dateFieldY - position.dateFieldHeight) + 'px',
                left: 0,
                'margin-left': position.dateFieldX + 'px',
                width: position.dateFieldWidth + 'px',
                height: position.dateFieldHeight + 'px',
                'line-height': position.dateFieldHeight + 'px',
                'font-size': position.dateFieldHeight + 'px',
                background: '#FFFFC0',
                overflow: 'hidden'
            };
        }
    };

    $scope.getChevronStyle = function (position) {
        return {
            position: 'absolute',
            'margin-top': (position.pageHeight - position.fieldY) + 'px',
            left: '-120px',
            width: '100px',
            'text-align':'right',
            'line-height': '14px',
            'font-size': '14px',
            height: '14px'
        };
    };

    $scope.getSignatureUrl = function () {
        return "/api/DownloadBlobPng/" + $scope.signatureRequest.requestID + "?container=signatures";
    };

    // setup
    var init = function () {

        models.SignatureRequest.query({ id: $scope.requestID }).then(function (fsr) {
            $scope.pages = fsr.document.pages;
            $scope.document = fsr.document;
            $scope.signatureRequest = fsr.request;
            $scope.signatureRequest;
            $scope.fonts = fsr.fonts;
            $scope.requester = fsr.requesterInfo;
            $scope.signaturePositions = fsr.signaturePositions;
            $scope.signer = $scope.signatureRequest.signer;

            var defaultSig = {
                signatureFont: $scope.fonts[0].name,
                initialsFont: $scope.fonts[0].name,
                signatureText: $scope.signatureRequest.signer.firstName + ' ' + $scope.signatureRequest.signer.lastName,
                initialsText: $scope.signatureRequest.signer.firstName[0] + '.' + $scope.signatureRequest.signer.lastName[0] + '.'
            };

            $scope.mySignature = defaultSig;

            if ($scope.userID) {
                api.userSignature.get({ userID: $scope.userID }).$promise.then(function (sig) {
                    if (sig.userID) {
                        $scope.mySignature = sig;
                    }
                    $scope.loaded = true;
                });
            } else {
                $scope.loaded = true;
            }
            
        }, function () {
            $scope.loadFailed = true;
        });
        
    };
    init();
}]);