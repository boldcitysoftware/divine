﻿app.controller('passwordResetController', ['$rootScope', '$scope', 'authService', 'flash', '$routeParams', function ($rootScope, $scope, authService, flash, $routeParams) {
    $rootScope.hideMenu = true;

    $scope.saving = false;

    $scope.recovery = {
        newPassword: "",
        confirmPassword: ""
    };

    $scope.resetPassword = function () {

        $scope.saving = true;
        $scope.recovery.userId = $routeParams.userId;
        $scope.recovery.token = $routeParams.token;

        authService.resetPasswordWithToken($scope.recovery).then(function (response) {
            $scope.saving = false;
            flash.success = 'Your password has been reset. You may now sign in.';
            $scope.go('/');
        },
         function (response) {
             $scope.saving = false;
             var errors = [];
             for (var key in response.data.modelState) {
                 for (var i = 0; i < response.data.modelState[key].length; i++) {
                     errors.push(response.data.modelState[key][i]);
                 }
             }
             flash.error = "Error during password reset: " + errors.join(' ');
         });
    };
}]);