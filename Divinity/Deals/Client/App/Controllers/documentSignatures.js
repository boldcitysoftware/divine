﻿angular.module('main').controller('documentSignaturesController', ['$scope', 'models', '$routeParams', 'flash','$q', function ($scope, models, $routeParams, flash, $q) {
    // models
    $scope.document = [];
    $scope.roles = [];
    $scope.signatureTypes = [{ name: 'Signature' }, { name: 'Initials' }];

    // scope vars
    $scope.loading = true;
    $scope.page = 1;
    
    // api interaction
    $scope.save = function () {
        $scope.document.save().then(function () {
            flash.success = "Signatures saved.";
        });
    };

    // push/splice
    $scope.addSignatureField = function () {
        var newSig = new models.SignatureInfo({
            documentID: $scope.document.documentID,
            pageNumber: $scope.page
        });
        $scope.document.signatureFields.push(newSig);
        $scope.select(newSig);
    };

    $scope.trashSelectedSignature = function () {
        var sig = $scope.selectedSig;

        if (sig) {
            var sigIndex = $scope.document.signatureFields.indexOf(sig);
            $scope.document.signatureFields.splice(sigIndex, 1);
            $scope.select($scope.document.signatureFields[0]);
        }
    };

    // signature date
    $scope.addSignatureDate = function (sig) {
        sig.hasDate = true;
        sig.dateX = sig.x + sig.width + 10;
        sig.dateY = sig.y;
        sig.dateWidth = 70;
        sig.dateHeight = 15;
    };

    $scope.removeSignatureDate = function (sig) {
        sig.hasDate = false;
        sig.dateX = '';
        sig.dateY = '';
        sig.dateWidth = '';
        sig.dateHeight = '';
    };

    // selection helpers
    $scope.getSigsForPage = function (page) {
        if (!$scope.document || !$scope.document.signatureFields) {
            return [];
        }
        return $.grep($scope.document.signatureFields, function (sig) {
            return sig.pageNumber == page;
        });
    };

    $scope.selected = function (sig) {
        return $scope.selectedSig == sig;
    };

    $scope.select = function (sig) {
        $scope.selectedSig = sig;
    };

    // styling
    $scope.getPageContainerDivStyle = function (page) {
        if ($scope.document && $scope.document.pages) {
            var index = page - 1;
            var img = $.grep($scope.document.pages, function (entity) { return entity.pageNumber == page })[0];
            if (img) {
                return {
                    'border': '1px solid #AAA',
                    'width': (img.pageWidth + 2) + 'px',
                    'height': (img.pageHeight + 2) + 'px',
                    margin: 'auto'
                };
            }
        }
    };

    $scope.getPageImageDivStyle = function () {
        if ($scope.document && $scope.document.pages) {
            var index = $scope.page - 1;
            var img = $.grep($scope.document.pages, function (entity) { return entity.pageNumber == $scope.page })[0];

            if (img) {
                return {
                    'position': 'relative',
                    'overflow': 'hidden',
                    'background': "url('/api/downloadblobpng/" + img.pageID + "?container=page-image')",
                    'width': img.pageWidth + 'px',
                    'height': img.pageHeight + 'px'
                };
            }
        }
    };

    $scope.getSignatureBoxStyle = function (sig) {
        if ($scope.document && $scope.document.pages) {
            var index = $scope.page - 1;
            var img = $.grep($scope.document.pages, function (entity) { return entity.pageNumber == $scope.page })[0];
            var background = '#ddd';
            if ($scope.selectedSig == sig) {
                background = '#d5fadd';
            }
            return {
                'position': 'absolute',
                'background': background,
                'width': (sig.width) + 'px',
                'height': (sig.height) + 'px',
                'left': (sig.x) + 'px',
                'top': (img.pageHeight - (sig.y)) + 'px',
                'border': '1px solid #777',
                'line-height': (sig.height - 2) + 'px',
                'font-size': (sig.height - 2) + 'px',
                'overflow': 'hidden'
            };
        }
    };

    $scope.getDateBoxStyle = function (sig) {
        if ($scope.document && $scope.document.pages) {
            var index = $scope.page - 1;
            var img = $.grep($scope.document.pages, function (entity) { return entity.pageNumber == $scope.page })[0];
            var background = '#ddd';
            if ($scope.selectedSig == sig) {
                background = '#fcf8e3';
            }
            return {
                'position': 'absolute',
                'background': background,
                'width': (sig.dateWidth) + 'px',
                'height': (sig.dateHeight) + 'px',
                'left': (sig.dateX) + 'px',
                'top': (img.pageHeight - (sig.dateY)) + 'px',
                'border': '1px solid #777',
                'line-height': (sig.dateHeight - 2) + 'px',
                'font-size': (sig.dateHeight - 2) + 'px',
                'overflow': 'hidden'
            };
        }
    };

    // drag/resize events
    $scope.dateResized = function (event, ui) {


        var dateID = event.target.id;
        var sig = $scope.selectedSig;
        if (sig) {

            var dw = ui.size.width - ui.originalSize.width;
            var dh = ui.size.height - ui.originalSize.height;
            var dx = ui.position.left - ui.originalPosition.left;
            var dy = ui.position.top - ui.originalPosition.top;

            sig.dateWidth += dw;
            sig.dateHeight += dh;

            sig.dateX += dx;
            sig.dateY -= dy; // y is from the bottom, so it's opposite

            sig.dateWidth = Math.round(sig.dateWidth);
            sig.dateHeight = Math.round(sig.dateHeight);
            sig.dateX = Math.round(sig.dateX);
            sig.dateY = Math.round(sig.dateY);
        }
    };

    $scope.dateMoved = function (event, ui) {
        var dateID = event.target.id;
        var sig = $scope.selectedSig;
        if (sig) {
            var dx = ui.position.left - ui.originalPosition.left;
            var dy = ui.position.top - ui.originalPosition.top;

            sig.dateX += dx;
            sig.dateY -= dy; // y is from the bottom, so it's opposite

            sig.dateX = Math.round(sig.dateX);
            sig.dateY = Math.round(sig.dateY);
        }
    };

    $scope.resized = function (event, ui) {


        var sigID = event.target.id;
        var sig = $scope.selectedSig;
        if (sig) {

            var dw = ui.size.width - ui.originalSize.width;
            var dh = ui.size.height - ui.originalSize.height;
            var dx = ui.position.left - ui.originalPosition.left;
            var dy = ui.position.top - ui.originalPosition.top;

            sig.width += dw;
            sig.height += dh;

            sig.x += dx;
            sig.y -= dy; // sig.y is from the bottom, so it's opposite

            sig.width = Math.round(sig.width);
            sig.height = Math.round(sig.height);
            sig.x = Math.round(sig.x);
            sig.y = Math.round(sig.y);
        }
    };

    $scope.moved = function (event, ui) {
        var sigID = event.target.id;
        var sig = $scope.selectedSig;
        if (sig) {
            var img = $.grep($scope.document.pages, function (entity) { return entity.pageNumber == $scope.page })[0];

            var left = ui.position.left;
            var top = ui.position.top;
            sig.x = left;
            sig.y = img.pageHeight - top;

            sig.x = Math.round(sig.x);
            sig.y = Math.round(sig.y);
        }
    };

    // init/page setup
    var init = function () {
        proms = [];
        proms.push(models.Document.find($routeParams.documentID).then(function (doc) {
            $scope.document = doc;
        }));
        proms.push(models.PersonRole.query().then(function (roles) {
            $scope.roles = roles;
        }));
        $q.all(proms).then(function () {
            $scope.loading = false;
        });
    };

    init();
}]);