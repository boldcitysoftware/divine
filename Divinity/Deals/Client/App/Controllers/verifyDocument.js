﻿angular.module('main').controller('verifyDocumentController', ['$rootScope', '$scope', '$routeParams', '$q', 'flash', 'api', function ($rootScope, $scope, $routeParams, $q, flash, api) {
    $scope.info = [];
    $rootScope.hideMenu = true;
    $scope.loaded = false;
    // init/page setup
    var init = function () {
        api.documentVerifier.get({ documentID: $routeParams.documentID }).$promise.then(function (info) {
            $scope.info = info;
            $scope.loaded = true;
        });
    };

    init();
}]);