﻿angular.module('main').controller('dealController', ['$scope', 'models','$routeParams', 'flash', function ($scope, models, $routeParams, flash) {
    $scope.deal = {};
    $scope.templates = [];

    // page actions
    $scope.addTaskToList = function (taskList) {
        taskList.tasks.push(new models.Task());
    };

    $scope.saveStatus = function (status) {
        $scope.deal.transactionStatus = status;
        $scope.deal.save();
    };

    $scope.createFormUsingTemplate = function (template) {
        var form = new models.Form({
            formTemplateID: template.formTemplateID,
            displayName: template.displayName,
            formType: template.formType,
            dealID: $routeParams.dealID
        });
        form.save().then(function (savedForm) {
            $scope.deal.forms.push(savedForm);
        });
    };

    /// return true if deletedAt is a false value.
    $scope.validDocument = function(value,index){
        return !value.deletedAt;
    };

    $scope.flowOptions = {
        target: '/api/upload/?dealID=' + $routeParams.dealID,
        permanentErrors: [404, 500, 501, 401, 403],
        generateUniqueIdentifier: Guid.newGuid
    };

    $scope.setProcessing = function () {
        $scope.processing = true;
    }

    $scope.uploadComplete = function ($file, $message) {
        flash.success = "Upload complete.";
        $scope.processing = false;
        init();
    };

    // init/page setup
    var init = function () {
        models.Deal.query({ id: $routeParams.dealID }).then(function (deal) {
            $scope.deal = deal;
            $scope.deal.forms = $.grep($scope.deal.forms, function (form) {
                return !form.trashedAt;
            });
            $scope.deal.property = new models.Property($scope.deal.properties[0]);
            angular.forEach($scope.deal.taskLists, function (taskList, index) {
                $scope.deal.taskLists[index] = new models.TaskList(taskList);
            });
            models.FormTemplate.query({ jurisdiction: deal.jurisdiction }).then(function (templates) {
                $scope.templates = templates;
            });
        });
    };

    init();
}]);