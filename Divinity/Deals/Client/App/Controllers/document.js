﻿angular.module('main').controller('documentController', ['$scope', 'models', '$routeParams','$q', 'flash','api', function ($scope, models, $routeParams, $q, flash,api) {
    $scope.document = [];
    $scope.signerData = {
        signer: []
    };
    $scope.loaded = false;
    // init/page setup
    var init = function () {
        $scope.signatureRequest = new models.SignatureRequest({
            dealID: $routeParams.dealID,
            documentID: $routeParams.documentID
        });
        
        var proms = [];

        proms.push(models.Document.find($routeParams.documentID).then(function (doc) {
            $scope.document = doc;
        }));

        proms.push(models.Deal.find($routeParams.dealID).then(function (deal) {
            $scope.deal = deal;
            $scope.signerData.signer = $scope.getPeople()[0];
            if ($scope.signerData.signer) {
                $scope.signatureRequest.signerEmail = $scope.signerData.signer.email;
            }
        }));

        $q.all(proms).then(function () { $scope.loaded = true; });
    };

    $scope.deleteDocument = function () {
        $scope.document.destroy().then(function () {
            flash.warn = 'Trashed ' + $scope.document.displayName + '.';
            $scope.go('/deals/' + $scope.document.dealID);
        });
    };

    $scope.getDocUri = function (mimeType) {
        
        if ($scope.document
            && $scope.document.contentType
            && !mimeType
            && $scope.document.contentType.indexOf('/pdf') === -1
            && $scope.document.contentType.indexOf('/png') === -1
            && $scope.document.contentType.indexOf('/gif') === -1
            && $scope.document.contentType.indexOf('/svg') === -1
            && $scope.document.contentType.indexOf('/jpg') === -1
            && $scope.document.contentType.indexOf('/jpeg') === -1
            && $scope.document.contentType.indexOf('text/') === -1
            ) {
            return '';
        } else {
            var preview = '';
            if (!mimeType) {
                preview = '?preview=true';
            }
            return '/api/DownloadDocument/' + $scope.document.documentID + preview;
        }
    }

    $scope.sendToSigner = function () {
        $scope.sending = true;
        var isInstant = $scope.signatureRequest.signerEmail === $scope.authentication.userName;

        $scope.signatureRequest.signerPersonID = $scope.signerData.signer.personID;
        $scope.signatureRequest.save({ sendEmail: !isInstant }).then(function (result) {
            $scope.sending = false;
            if (isInstant) {
                api.userID.get().$promise.then(function (info) {
                    $scope.go('/secure-sign/' + result.requestID + '/' + info.userID);
                });
            } else {
                flash.to('form-flash').success = 'Signature request sent.';
                init();
            }
        });
    };

    $scope.recipientSelected = function () {
        $scope.signatureRequest.signerEmail = $scope.signerData.signer.email;
    };

    $scope.getPeople = function () {
        if ($scope.deal && $scope.deal.people) {
            var people = $scope.deal.people.sort(function (a, b) {
                if (a.lastName < b.lastName) {
                    return -1;
                } else if (a.lastName > b.lastName) {
                    return 1;
                } else if (a.firstName < b.firstName) {
                    return -1;
                } else if (a.firstName > b.firstName) {
                    return 1;
                } else {
                    return 0;
                }
            });
            if ($scope.document.signatureFields) {
                people = $.grep(people, function (person) {
                    var matchingSignerRoles = $.grep($scope.document.signatureFields, function (field) {
                        return person.roleName === field.roleName;
                    });
                    return matchingSignerRoles.length > 0;
                });
            }
            if ($scope.document.signatureRequests) {
                people = $.grep(people, function (person) {
                    var matchingSignaturesExecuted = $.grep($scope.document.signatureRequests, function (request) {
                        return request.allSignaturesConfirmedAt && request.signer.personID === person.personID;
                    });
                    return matchingSignaturesExecuted.length == 0;
                });
            }
            return people;
        } else {
            return [];
        }
    };

    init();
}]);