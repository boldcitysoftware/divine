﻿angular.module('main').controller('formController', ['$scope', 'models', '$routeParams', 'flash', function ($scope, models, $routeParams, flash) {
    $scope.loaded = false;
    $scope.saving = false;
    $scope.finishing = false;
    
    $scope.dealID = $routeParams.dealID;
    $scope.formID = $routeParams.formID;

    $scope.form = {};
    $scope.deal = {};
    $scope.template = {};

    // greps and such
    $scope.getFormUri = function () {
        return '/api/DownloadForm/' + $scope.form.formID + '?container=filled-form';
    };

    $scope.getFormField = function (acroname) {
        var results = $.grep($scope.form.fields, function (field) {
            return field.fieldName === acroname;
        });

        if (results.length == 0) {
            var field = {
                formFieldID: '00000000-0000-0000-0000-000000000000',
                formID: '00000000-0000-0000-0000-000000000000',
                fieldName: acroname,
                fieldValue: 'NOT FOUND'
            };
            $scope.form.fields.push(field);
            return field;
        } else {
            return results[0];
        }
    };

    $scope.getFields = function () {
        var result = [];
        if ($scope.template && $scope.template.acroFields) {
            result = $.grep($scope.template.acroFields, function (field) {
                return field.editable;
            });
        }
        return result;
    };

    // form events
    $scope.destroyForm = function () {
        $scope.destroying = true;
        $scope.form.destroy().then(function () {
            $scope.destroying = false;
            flash.warn = "'" + $scope.form.displayName + "' destroyed.";
            $scope.go('/deals/' + $scope.form.dealID);
        });
    };

    $scope.saveFields = function (silent) {
        silent = !!silent;
        
        $scope.saving = !silent && true;
        return $scope.form.save().then(function () {
            flash.to('form-flash').success = 'Form saved.';
            $scope.saving = false;
        });
    };

    $scope.finish = function () {
        $scope.finishing = true;
        $scope.form.save({ finish: true }).then(function (form) {
            flash.success = 'Form saved.';
            $scope.finishing = false;
            $scope.go('/deals/' + $routeParams.dealID + '/documents/' + form.documentID);
        });
    };

    // init/page setup
    $scope.init = function () {
        models.Form.query({ id: $routeParams.formID }).then(function (form) {
            $scope.form = form;
            $scope.template = form.formTemplate;

            if (form.documentID) {
                $scope.go('/deals/' + $routeParams.dealID + '/documents/' + form.documentID);
            }

            $scope.loaded = true;
        });
        
        models.Deal.query({ id: $routeParams.dealID }).then(function (deal) {
            $scope.deal = deal;
        });
    };

    $scope.init();
}]);