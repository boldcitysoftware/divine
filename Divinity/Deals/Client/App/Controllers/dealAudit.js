﻿angular.module('main').controller('dealAuditController', ['$scope', 'models','$routeParams', function ($scope, models, $routeParams) {
    $scope.history = [];
    $scope.deal = {};

    // init/page setup
    var init = function () {
        models.DealAudit.query($routeParams.dealID).then(function (history) {
            $scope.history = history;
        });
        models.Deal.query({ id: $routeParams.dealID }).then(function (deal) {
            $scope.deal = deal;
        });
    };

    init();
}]);