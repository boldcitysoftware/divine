﻿'use strict';
app.controller('billingController', ['$scope','models','flash', function ($scope, models, flash) {
    // models
    $scope.years = [];
    $scope.loaded = false;
    $scope.saving = false;
    $scope.subscription = {};
    $scope.unsub = {};
    $scope.subscriptionStatus = {};

    // api calls
    $scope.subscribe = function () {
        $scope.errorMessage = null;
        $scope.saving = true;
        if ($scope.couponToApply) {
            $scope.subscription.couponCode = $scope.couponToApply.couponCode; // in case they messed with it before hitting subscribe
        }
        $scope.subscription.save().then(function () {
            $scope.getSubscriptionStatus().then(function () {
                $scope.saving = false;
                flash.success = "Your subscription has been activated.";
            });
            init();
        }, function (response) {
            $scope.saving = false;
            if (response.data) {
                $scope.errorMessage = response.data.exceptionMessage;
            } else {
                $scope.errorMessage = "An error occurred.";
            }
        });
    };

    $scope.checkCoupon = function () {
        $scope.couponMessage = "";
        models.CouponCheck.query($scope.subscription.couponCode).then(function (info) {
            info.couponCode = $scope.subscription.couponCode;
            if (info.discountedPrice) {
                $scope.couponToApply = info;
                $scope.couponMessage = "Coupon applied: " + $scope.subscription.couponCode;
            } else {
                $scope.couponMessage = info.Message;
            }
        });
    };

    $scope.unsubscribe = function () {
        $scope.errorMessage = null;
        $scope.saving = true;
        $scope.unsub.save().then(function () {
            flash.warn = "Thank you for your feedback. Your subscription has been cancelled.";
            $scope.saving = false;
            $scope.getSubscriptionStatus();
            init();
        }, function (response) {

            $scope.saving = false;
            if (response.data) {
                $scope.errorMessage = response.data.exceptionMessage;
            } else {
                $scope.errorMessage = "An error occurred.";
            }
        });
    };
    $scope.getSubscriptionStatus = function () {
        models.SubscriptionInfo.query().then(function (info) {
            $scope.subscriptionStatus = info;
        });
    }
    // setup
    $scope.getFullPriceStyle = function () {
        if ($scope.couponToApply) {
            return { 'text-decoration': 'line-through' };
        } else {
            return {};
        }
    };
    var init = function () {
        $scope.getSubscriptionStatus();

        $scope.subscription = new models.Subscription();
        $scope.unsub = new models.SubscriptionCancellation();
        
        models.PersonalInfo.find().then(function (info) {
            if (info.firstName && info.lastName) {
                $scope.subscription.cardName = info.firstName + ' ' + info.lastName;
            }
        });

        var thisYear = new Date().getFullYear();
        for (var i = 0; i < 5; i++) {
            $scope.years.push({ year: (thisYear + i).toString() });
        }

        $scope.loaded = true;
    };
    init();
}]);