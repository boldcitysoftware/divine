﻿'use strict';
app.controller('personalInfoController', ['$scope','models','flash', function ($scope, models, flash) {
    // models
    $scope.personalInfo = {};
    $scope.personalInfoLoaded = false;
    $scope.savingPersonalInfo = false;

    // api calls
    $scope.savePersonalInfo = function () {
        $scope.savingPersonalInfo = true;
        $scope.personalInfo.save().then(function () {
            $scope.savingPersonalInfo = false;
            flash.success = 'Personal information saved.';
        });
    };

    // setup
    var init = function () {
        models.PersonalInfo.find().then(function (personalInfo) {
            $scope.personalInfo = personalInfo;
            $scope.personalInfoLoaded = true;
        });
    };
    init();
}]);