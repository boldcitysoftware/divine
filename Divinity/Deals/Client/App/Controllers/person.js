﻿angular.module('main').controller('personController', ['$scope', 'models', '$routeParams', 'flash', '$q', function ($scope, models, $routeParams, flash, $q) {
    $scope.person = {};
    $scope.savingPerson = false;
    $scope.refreshingPerson = false;
    $scope.dealID = $routeParams.dealID;
    $scope.roles = [];
    $scope.dealAccess = {};
    $scope.shareDeal = false;

    // form events
    $scope.savePerson = function () {
        $scope.savingPerson = true;
        $scope.person.save().then(function (person) {
            var promises = [];
            if ($scope.shareDeal && !$scope.dealWasShared) {
                // share it, it's not currently shared
                var da = new models.DealAccess();
                da.dealID = $routeParams.dealID;
                da.personID = person.personID;
                promises.push(da.save());
            } else if (!$scope.shareDeal && $scope.dealWasShared) {
                // unshare it, it's currently shared
                promises.push($scope.dealAccess.destroy());
            }
            $q.all(promises).then(function () {
                $scope.savingPerson = false;
                flash.success = 'Person saved.';
                
                $scope.go('/deals/' + $routeParams.dealID);
            });
        });
    };

    // init/page setup
    var init = function () {
        models.PersonRole.query().then(function (roles) {
            $scope.roles = roles;
        });

        if (!$routeParams.personID) {
            $scope.person = new models.Person();
            $scope.person.dealID = $routeParams.dealID;
            $scope.person.roleName = "Seller's Agent";
        } else {
            models.DealAccess.query($routeParams.dealID, $routeParams.personID).then(function (da) {
                $scope.dealAccess = da;
                $scope.dealWasShared = $scope.shareDeal = ($scope.dealAccess.accessID !== '00000000-0000-0000-0000-000000000000' && !$scope.dealAccess.revoked);
            });
            models.Person.find($routeParams.personID).then(function (person) {
                $scope.person = person;
            });
        }
    };

    init();
}]);