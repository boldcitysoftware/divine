﻿angular.module('main').controller('homeController', ['$scope', 'models', function ($scope, models) {
    $scope.deals = [];
    $scope.dealFilter = {
        transactionType: '',
        transactionStatus: '!Archived',
        displayName: ''
    };
    $scope.loadingDeals = true;
    $scope.fillDeals = function () {
        return models.Deal.query().then(function (data) {
            $scope.deals = data;
            $scope.loadingDeals = false;
        });
    };

    // init/page setup
    var init = function () {
        $scope.fillDeals();
    };

    init();
}]);