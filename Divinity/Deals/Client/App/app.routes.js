﻿// Main configuration file. Sets up AngularJS module and routes and any other config objects

var app = angular.module('main');

app.config(['$routeProvider', function ($routeProvider) {
    //Setup routes to load partial templates from server. TemplateUrl is the location for the server view (Razor .cshtml view)
    $routeProvider// /account
        .when('/', { templateUrl: 'account/login.html', controller: 'loginController' })
        .when('/login', { templateUrl: 'account/login.html', controller: 'loginController' })
        .when('/login/:userId/:email/:password/:dealID/:token*', { templateUrl: 'account/login.html', controller: 'loginController' })
        .when('/signup', { templateUrl: 'account/signup.html', controller: 'signupController' })
        .when('/confirm-account/:userId/:token*', { templateUrl: 'account/confirm.html', controller: 'confirmAccountController' })
        .when('/lost-password', { templateUrl: 'account/lost-password.html', controller: 'lostPasswordController' })
        .when('/password-reset/:userId/:token*', { templateUrl: 'account/password-reset.html', controller: 'passwordResetController' })

        // /setup
        .when('/personal-info', { templateUrl: 'setup/personal-info.html', controller: 'personalInfoController' })
        .when('/my-signature', { templateUrl: 'setup/my-signature.html', controller: 'mySignatureController' })
        .when('/subscription', { templateUrl: 'setup/billing.html', controller: 'billingController' })

        // /deals
        .when('/deals', { templateUrl: 'deals/index.html', controller: 'homeController' })
        .when('/deals/new', { templateUrl: 'deals/new.html', controller: 'newDealController' })
        .when('/deals/:dealID', { templateUrl: 'deals/detail.html', controller: 'dealController' })
        .when('/deals/:dealID/history', { templateUrl: 'deals/history.html', controller: 'dealAuditController' })
        .when('/deals/:dealID/properties/:propertyID', { templateUrl: 'deals/properties/edit.html', controller: 'propertyController' })
        .when('/deals/:dealID/people/new', { templateUrl: 'deals/people/new.html', controller: 'personController' })
        .when('/deals/:dealID/people/:personID', { templateUrl: 'deals/people/edit.html', controller: 'personController' })
        .when('/deals/:dealID/forms/:formID', { templateUrl: 'deals/forms/detail.html', controller: 'formController' })
        .when('/deals/:dealID/forms/:formID/:pageNumber', { templateUrl: 'deals/forms/detail.html', controller: 'formController' })
        .when('/deals/:dealID/documents/:documentID', { templateUrl: 'deals/documents/detail.html', controller: 'documentController' })
        .when('/deals/:dealID/documents/:documentID/signature-setup', { templateUrl: 'deals/documents/signature-setup.html', controller: 'documentSignaturesController' })

        // e-signing
        .when('/secure-sign/:requestID', { templateUrl: 'e-sign/signature-collection.html', controller: 'signatureCollectionController' })
        .when('/secure-sign/:requestID/:userID', { templateUrl: 'e-sign/signature-collection.html', controller: 'signatureCollectionController' })
        .when('/verify/:documentID', { templateUrl: 'e-sign/verify-document.html', controller: 'verifyDocumentController' })
        
        .otherwise({ redirectTo: '/deals' });
}]);