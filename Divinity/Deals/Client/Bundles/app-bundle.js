// Main configuration file. Sets up AngularJS module and routes and any other config objects

var app = angular.module('main', ['flow', 'ui.bootstrap', 'angular-flash.service', 'angular-flash.flash-alert-directive', 'ngRoute', 'ui.grid', 'ngResource', 'LocalStorageModule', 'models', 'context', 'data']);     //Define the main module

app.config(['flashProvider', 'flowFactoryProvider', '$httpProvider', function (flashProvider, flowFactoryProvider, $httpProvider) {
    

    // setup flash
    flashProvider.errorClassnames.push('alert-danger');
    flashProvider.warnClassnames.push('alert-warning');
    flashProvider.infoClassnames.push('alert-info');
    flashProvider.successClassnames.push('alert-success');

    flowFactoryProvider.defaults = {
        target: '/api/upload',
        permanentErrors: [404, 500, 501, 401, 403]
    };

    $httpProvider.interceptors.push('authInterceptorService');
}]);

app.run(['$location', '$rootScope', 'models', '$q', 'authService', function ($location, $rootScope, models, $q, authService) {
    authService.fillAuthData();

    $rootScope.utils = {
        stringToNumber: function (text) {
            if (this == "") {
                return text;
            }
            var transformedInput = text.toString().replace(/\$|[^0-9]/g, '');

            return transformedInput;
        },
        stringToDecimal: function (text) {
            if (this == "") {
                return text;
            }
            var transformedInput = text.toString().replace(/\$|[^0-9\.\$\-]/g, '');

            transformedInput = parseFloat(Math.round(parseFloat(transformedInput) * 100) / 100).toFixed(2);

            return transformedInput;
        },
        roundUp: function (num) {
            var int = parseInt(num);
            var float = parseFloat(num);

            if (parseFloat(int) === float) {
                // they gave me an integral
                return int;
            } else {
                // floating point detected! add one.
                return int + 1;
            }
        },
        mmddyyyy: function (date) {
            var yyyy = date.getFullYear().toString();
            var mm = (date.getMonth() + 1).toString(); // getMonth() is zero-based
            var dd = date.getDate().toString();
            return (mm[1] ? mm : "0" + mm[0]) + "/" + (dd[1] ? dd : "0" + dd[0]) + "/" + yyyy; // padding
        }
    };
}])

app.controller('RootController', ['$rootScope', '$scope', '$location', 'authService', 'models', '$window', 'flash', function ($rootScope, $scope, $location, authService, models, $window, flash) {
    $scope.appLoaded = true;
    // helpers
    $scope.phoneNumberPattern = (function () {
        var regexp = /^\(?(\d{3})\)?[ .-]?(\d{3})[ .-]?(\d{4})$/;
        return {
            test: function (value) {
                if ($scope.requireTel === false) {
                    return true;
                }
                return regexp.test(value);
            }
        };
    })();
    $scope.go = function (route, params) {
        $location.path(route).search(params || {});
    };

    $scope.$back = function () {
        $window.history.back();
    };

    $scope.$on('$routeChangeStart', function (e, current, previous) {
        $rootScope.hideMenu = false;
    });

    $scope.$on('$routeChangeSuccess', function (e, current, previous) {
        $scope.activeViewPath = $location.path();
    });

    $scope.logOut = function () {
        authService.logOut();
        flash.warn = 'You have been logged out. See you soon!';
        $location.path('/login');
    }

    $scope.defaultGridOptions = {
        multiSelect: false,
        enableSorting: true,
        enableRowSelection: false,
        enableColumnResize: true,
        showColumnMenu: true,
        enableFiltering: true
    };

    $scope.defaultGridScope = {
        go: $scope.go
    };

    $scope.authentication = authService.authentication;

    $scope.usStates =
        [{
            "name": "Alabama",
            "abbreviation": "AL"
        },
        {
            "name": "Alaska",
            "abbreviation": "AK"
        },
        {
            "name": "American Samoa",
            "abbreviation": "AS"
        },
        {
            "name": "Arizona",
            "abbreviation": "AZ"
        },
        {
            "name": "Arkansas",
            "abbreviation": "AR"
        },
        {
            "name": "California",
            "abbreviation": "CA"
        },
        {
            "name": "Colorado",
            "abbreviation": "CO"
        },
        {
            "name": "Connecticut",
            "abbreviation": "CT"
        },
        {
            "name": "Delaware",
            "abbreviation": "DE"
        },
        {
            "name": "District Of Columbia",
            "abbreviation": "DC"
        },
        {
            "name": "Federated States Of Micronesia",
            "abbreviation": "FM"
        },
        {
            "name": "Florida",
            "abbreviation": "FL"
        },
        {
            "name": "Georgia",
            "abbreviation": "GA"
        },
        {
            "name": "Guam",
            "abbreviation": "GU"
        },
        {
            "name": "Hawaii",
            "abbreviation": "HI"
        },
        {
            "name": "Idaho",
            "abbreviation": "ID"
        },
        {
            "name": "Illinois",
            "abbreviation": "IL"
        },
        {
            "name": "Indiana",
            "abbreviation": "IN"
        },
        {
            "name": "Iowa",
            "abbreviation": "IA"
        },
        {
            "name": "Kansas",
            "abbreviation": "KS"
        },
        {
            "name": "Kentucky",
            "abbreviation": "KY"
        },
        {
            "name": "Louisiana",
            "abbreviation": "LA"
        },
        {
            "name": "Maine",
            "abbreviation": "ME"
        },
        {
            "name": "Marshall Islands",
            "abbreviation": "MH"
        },
        {
            "name": "Maryland",
            "abbreviation": "MD"
        },
        {
            "name": "Massachusetts",
            "abbreviation": "MA"
        },
        {
            "name": "Michigan",
            "abbreviation": "MI"
        },
        {
            "name": "Minnesota",
            "abbreviation": "MN"
        },
        {
            "name": "Mississippi",
            "abbreviation": "MS"
        },
        {
            "name": "Missouri",
            "abbreviation": "MO"
        },
        {
            "name": "Montana",
            "abbreviation": "MT"
        },
        {
            "name": "Nebraska",
            "abbreviation": "NE"
        },
        {
            "name": "Nevada",
            "abbreviation": "NV"
        },
        {
            "name": "New Hampshire",
            "abbreviation": "NH"
        },
        {
            "name": "New Jersey",
            "abbreviation": "NJ"
        },
        {
            "name": "New Mexico",
            "abbreviation": "NM"
        },
        {
            "name": "New York",
            "abbreviation": "NY"
        },
        {
            "name": "North Carolina",
            "abbreviation": "NC"
        },
        {
            "name": "North Dakota",
            "abbreviation": "ND"
        },
        {
            "name": "Northern Mariana Islands",
            "abbreviation": "MP"
        },
        {
            "name": "Ohio",
            "abbreviation": "OH"
        },
        {
            "name": "Oklahoma",
            "abbreviation": "OK"
        },
        {
            "name": "Oregon",
            "abbreviation": "OR"
        },
        {
            "name": "Palau",
            "abbreviation": "PW"
        },
        {
            "name": "Pennsylvania",
            "abbreviation": "PA"
        },
        {
            "name": "Puerto Rico",
            "abbreviation": "PR"
        },
        {
            "name": "Rhode Island",
            "abbreviation": "RI"
        },
        {
            "name": "South Carolina",
            "abbreviation": "SC"
        },
        {
            "name": "South Dakota",
            "abbreviation": "SD"
        },
        {
            "name": "Tennessee",
            "abbreviation": "TN"
        },
        {
            "name": "Texas",
            "abbreviation": "TX"
        },
        {
            "name": "Utah",
            "abbreviation": "UT"
        },
        {
            "name": "Vermont",
            "abbreviation": "VT"
        },
        {
            "name": "Virgin Islands",
            "abbreviation": "VI"
        },
        {
            "name": "Virginia",
            "abbreviation": "VA"
        },
        {
            "name": "Washington",
            "abbreviation": "WA"
        },
        {
            "name": "West Virginia",
            "abbreviation": "WV"
        },
        {
            "name": "Wisconsin",
            "abbreviation": "WI"
        },
        {
            "name": "Wyoming",
            "abbreviation": "WY"
        }];
}]);
app.directive('signatureBox', function () {
    return {
        // A = attribute, E = Element, C = Class and M = HTML Comment
        restrict: 'A',
        link: function (scope, element, attrs) {
            $(element.context).draggable({
                containment: 'parent',
                opacity: .7,
                stop: scope.moved
            });
            $(element.context).resizable({
                containment: 'parent',
                handles: 'ne, se, sw, nw',
                stop: scope.resized
            });
        }
    }
});

app.directive('dateBox', function () {
    return {
        // A = attribute, E = Element, C = Class and M = HTML Comment
        restrict: 'A',
        link: function (scope, element, attrs) {
            $(element.context).draggable({
                containment: 'parent',
                opacity: .7,
                stop: scope.dateMoved
            });
            $(element.context).resizable({
                containment: 'parent',
                handles: 'ne, se, sw, nw',
                stop: scope.dateResized
            });
        }
    }
});

app.directive('ngConfirmClick', [
  function () {
      return {
          priority: -1,
          restrict: 'A',
          link: function (scope, element, attrs) {
              element.bind('click', function (e) {
                  var message = attrs.ngConfirmClick;
                  if (message && !confirm(message)) {
                      e.stopImmediatePropagation();
                      e.preventDefault();
                  }
              });
          }
      }
  }
]);
app.directive('decimalInput', function () {
    return {
        require: 'ngModel',
        link: function (scope, element, attr, ngModelCtrl) {
            element.css('text-align', 'right');
            var fromUser = function (text) {
                var transformedInput = scope.utils.stringToDecimal(text);

                if (transformedInput !== text) {
                    ngModelCtrl.$setViewValue(transformedInput);
                    ngModelCtrl.$render();
                }
                return transformedInput; // or return Number(transformedInput)
            };
            ngModelCtrl.$parsers.push(fromUser);
        }
    };
});

app.directive('numericInput', function () {
    return {
        require: 'ngModel',
        link: function (scope, element, attr, ngModelCtrl) {
            //element.css('text-align', 'right');
            var fromUser = function (text) {
                var transformedInput = scope.utils.stringToNumber(text);

                if (transformedInput !== text) {
                    ngModelCtrl.$setViewValue(transformedInput);
                    ngModelCtrl.$render();
                }
                return transformedInput; // or return Number(transformedInput)
            };
            ngModelCtrl.$parsers.push(fromUser);
        }
    };
});

var Guid = Guid || (function () {

    var EMPTY = '00000000-0000-0000-0000-000000000000';

    var _padLeft = function (paddingString, width, replacementChar) {
        return paddingString.length >= width ? paddingString : _padLeft(replacementChar + paddingString, width, replacementChar || ' ');
    };

    var _s4 = function (number) {
        var hexadecimalResult = number.toString(16);
        return _padLeft(hexadecimalResult, 4, '0');
    };

    var _cryptoGuid = function () {
        var buffer = new window.Uint16Array(8);
        window.crypto.getRandomValues(buffer);
        return [_s4(buffer[0]) + _s4(buffer[1]), _s4(buffer[2]), _s4(buffer[3]), _s4(buffer[4]), _s4(buffer[5]) + _s4(buffer[6]) + _s4(buffer[7])].join('-');
    };

    var _guid = function () {
        var currentDateMilliseconds = new Date().getTime();
        return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (currentChar) {
            var randomChar = (currentDateMilliseconds + Math.random() * 16) % 16 | 0;
            currentDateMilliseconds = Math.floor(currentDateMilliseconds / 16);
            return (currentChar === 'x' ? randomChar : (randomChar & 0x7 | 0x8)).toString(16);
        });
    };

    var create = function () {
        var hasCrypto = typeof (window.crypto) != 'undefined',
            hasRandomValues = typeof (window.crypto.getRandomValues) != 'undefined';
        return (hasCrypto && hasRandomValues) ? _cryptoGuid() : _guid();
    };

    return {
        newGuid: create,
        empty: EMPTY
    };
})();

if (typeof String.prototype.startsWith != 'function') {
    String.prototype.startsWith = function (str) {
        return this.slice(0, str.length) == str;
    };
}

if (typeof String.prototype.endsWith != 'function') {
    String.prototype.endsWith = function (str) {
        return this.slice(-str.length) == str;
    };
}
// Main configuration file. Sets up AngularJS module and routes and any other config objects

var app = angular.module('main');

app.config(['$routeProvider', function ($routeProvider) {
    //Setup routes to load partial templates from server. TemplateUrl is the location for the server view (Razor .cshtml view)
    $routeProvider// /account
        .when('/', { templateUrl: 'account/login.html', controller: 'loginController' })
        .when('/login', { templateUrl: 'account/login.html', controller: 'loginController' })
        .when('/login/:userId/:email/:password/:dealID/:token*', { templateUrl: 'account/login.html', controller: 'loginController' })
        .when('/signup', { templateUrl: 'account/signup.html', controller: 'signupController' })
        .when('/confirm-account/:userId/:token*', { templateUrl: 'account/confirm.html', controller: 'confirmAccountController' })
        .when('/lost-password', { templateUrl: 'account/lost-password.html', controller: 'lostPasswordController' })
        .when('/password-reset/:userId/:token*', { templateUrl: 'account/password-reset.html', controller: 'passwordResetController' })

        // /setup
        .when('/personal-info', { templateUrl: 'setup/personal-info.html', controller: 'personalInfoController' })
        .when('/my-signature', { templateUrl: 'setup/my-signature.html', controller: 'mySignatureController' })
        .when('/subscription', { templateUrl: 'setup/billing.html', controller: 'billingController' })

        // /deals
        .when('/deals', { templateUrl: 'deals/index.html', controller: 'homeController' })
        .when('/deals/new', { templateUrl: 'deals/new.html', controller: 'newDealController' })
        .when('/deals/:dealID', { templateUrl: 'deals/detail.html', controller: 'dealController' })
        .when('/deals/:dealID/history', { templateUrl: 'deals/history.html', controller: 'dealAuditController' })
        .when('/deals/:dealID/properties/:propertyID', { templateUrl: 'deals/properties/edit.html', controller: 'propertyController' })
        .when('/deals/:dealID/people/new', { templateUrl: 'deals/people/new.html', controller: 'personController' })
        .when('/deals/:dealID/people/:personID', { templateUrl: 'deals/people/edit.html', controller: 'personController' })
        .when('/deals/:dealID/forms/:formID', { templateUrl: 'deals/forms/detail.html', controller: 'formController' })
        .when('/deals/:dealID/forms/:formID/:pageNumber', { templateUrl: 'deals/forms/detail.html', controller: 'formController' })
        .when('/deals/:dealID/documents/:documentID', { templateUrl: 'deals/documents/detail.html', controller: 'documentController' })
        .when('/deals/:dealID/documents/:documentID/signature-setup', { templateUrl: 'deals/documents/signature-setup.html', controller: 'documentSignaturesController' })

        // e-signing
        .when('/secure-sign/:requestID', { templateUrl: 'e-sign/signature-collection.html', controller: 'signatureCollectionController' })
        .when('/secure-sign/:requestID/:userID', { templateUrl: 'e-sign/signature-collection.html', controller: 'signatureCollectionController' })
        .when('/verify/:documentID', { templateUrl: 'e-sign/verify-document.html', controller: 'verifyDocumentController' })
        
        .otherwise({ redirectTo: '/deals' });
}]);
'use strict';
app.controller('billingController', ['$scope','models','flash', function ($scope, models, flash) {
    // models
    $scope.years = [];
    $scope.loaded = false;
    $scope.saving = false;
    $scope.subscription = {};
    $scope.unsub = {};
    $scope.subscriptionStatus = {};

    // api calls
    $scope.subscribe = function () {
        $scope.errorMessage = null;
        $scope.saving = true;
        if ($scope.couponToApply) {
            $scope.subscription.couponCode = $scope.couponToApply.couponCode; // in case they messed with it before hitting subscribe
        }
        $scope.subscription.save().then(function () {
            $scope.getSubscriptionStatus().then(function () {
                $scope.saving = false;
                flash.success = "Your subscription has been activated.";
            });
            init();
        }, function (response) {
            $scope.saving = false;
            if (response.data) {
                $scope.errorMessage = response.data.exceptionMessage;
            } else {
                $scope.errorMessage = "An error occurred.";
            }
        });
    };

    $scope.checkCoupon = function () {
        $scope.couponMessage = "";
        models.CouponCheck.query($scope.subscription.couponCode).then(function (info) {
            info.couponCode = $scope.subscription.couponCode;
            if (info.discountedPrice) {
                $scope.couponToApply = info;
                $scope.couponMessage = "Coupon applied: " + $scope.subscription.couponCode;
            } else {
                $scope.couponMessage = info.Message;
            }
        });
    };

    $scope.unsubscribe = function () {
        $scope.errorMessage = null;
        $scope.saving = true;
        $scope.unsub.save().then(function () {
            flash.warn = "Thank you for your feedback. Your subscription has been cancelled.";
            $scope.saving = false;
            $scope.getSubscriptionStatus();
            init();
        }, function (response) {

            $scope.saving = false;
            if (response.data) {
                $scope.errorMessage = response.data.exceptionMessage;
            } else {
                $scope.errorMessage = "An error occurred.";
            }
        });
    };
    $scope.getSubscriptionStatus = function () {
        models.SubscriptionInfo.query().then(function (info) {
            $scope.subscriptionStatus = info;
        });
    }
    // setup
    $scope.getFullPriceStyle = function () {
        if ($scope.couponToApply) {
            return { 'text-decoration': 'line-through' };
        } else {
            return {};
        }
    };
    var init = function () {
        $scope.getSubscriptionStatus();

        $scope.subscription = new models.Subscription();
        $scope.unsub = new models.SubscriptionCancellation();
        
        models.PersonalInfo.find().then(function (info) {
            if (info.firstName && info.lastName) {
                $scope.subscription.cardName = info.firstName + ' ' + info.lastName;
            }
        });

        var thisYear = new Date().getFullYear();
        for (var i = 0; i < 5; i++) {
            $scope.years.push({ year: (thisYear + i).toString() });
        }

        $scope.loaded = true;
    };
    init();
}]);
angular.module('main').controller('confirmAccountController', ['$scope', 'authService', '$routeParams', '$rootScope','flash', function ($scope, authService, $routeParams, $rootScope,flash) {
    
    $rootScope.hideMenu = true;
    // init/page setup
    var init = function () {
        authService.confirmAccount({userId: $routeParams.userId, token:$routeParams.token}).then(function (response) {
            $scope.savedSuccessfully = true;
            flash.warn = "Your account has been confirmed! You may now sign in.";
            $scope.go('/login');

        });
    };

    init();
}]);
angular.module('main').controller('dealController', ['$scope', 'models','$routeParams', 'flash', function ($scope, models, $routeParams, flash) {
    $scope.deal = {};
    $scope.templates = [];

    // page actions
    $scope.addTaskToList = function (taskList) {
        taskList.tasks.push(new models.Task());
    };

    $scope.saveStatus = function (status) {
        $scope.deal.transactionStatus = status;
        $scope.deal.save();
    };

    $scope.createFormUsingTemplate = function (template) {
        var form = new models.Form({
            formTemplateID: template.formTemplateID,
            displayName: template.displayName,
            formType: template.formType,
            dealID: $routeParams.dealID
        });
        form.save().then(function (savedForm) {
            $scope.deal.forms.push(savedForm);
        });
    };

    /// return true if deletedAt is a false value.
    $scope.validDocument = function(value,index){
        return !value.deletedAt;
    };

    $scope.flowOptions = {
        target: '/api/upload/?dealID=' + $routeParams.dealID,
        permanentErrors: [404, 500, 501, 401, 403],
        generateUniqueIdentifier: Guid.newGuid
    };

    $scope.setProcessing = function () {
        $scope.processing = true;
    }

    $scope.uploadComplete = function ($file, $message) {
        flash.success = "Upload complete.";
        $scope.processing = false;
        init();
    };

    // init/page setup
    var init = function () {
        models.Deal.query({ id: $routeParams.dealID }).then(function (deal) {
            $scope.deal = deal;
            $scope.deal.forms = $.grep($scope.deal.forms, function (form) {
                return !form.trashedAt;
            });
            $scope.deal.property = new models.Property($scope.deal.properties[0]);
            angular.forEach($scope.deal.taskLists, function (taskList, index) {
                $scope.deal.taskLists[index] = new models.TaskList(taskList);
            });
            models.FormTemplate.query({ jurisdiction: deal.jurisdiction }).then(function (templates) {
                $scope.templates = templates;
            });
        });
    };

    init();
}]);
angular.module('main').controller('dealAuditController', ['$scope', 'models','$routeParams', function ($scope, models, $routeParams) {
    $scope.history = [];
    $scope.deal = {};

    // init/page setup
    var init = function () {
        models.DealAudit.query($routeParams.dealID).then(function (history) {
            $scope.history = history;
        });
        models.Deal.query({ id: $routeParams.dealID }).then(function (deal) {
            $scope.deal = deal;
        });
    };

    init();
}]);
angular.module('main').controller('documentController', ['$scope', 'models', '$routeParams','$q', 'flash','api', function ($scope, models, $routeParams, $q, flash,api) {
    $scope.document = [];
    $scope.signerData = {
        signer: []
    };
    $scope.loaded = false;
    // init/page setup
    var init = function () {
        $scope.signatureRequest = new models.SignatureRequest({
            dealID: $routeParams.dealID,
            documentID: $routeParams.documentID
        });
        
        var proms = [];

        proms.push(models.Document.find($routeParams.documentID).then(function (doc) {
            $scope.document = doc;
        }));

        proms.push(models.Deal.find($routeParams.dealID).then(function (deal) {
            $scope.deal = deal;
            $scope.signerData.signer = $scope.getPeople()[0];
            if ($scope.signerData.signer) {
                $scope.signatureRequest.signerEmail = $scope.signerData.signer.email;
            }
        }));

        $q.all(proms).then(function () { $scope.loaded = true; });
    };

    $scope.deleteDocument = function () {
        $scope.document.destroy().then(function () {
            flash.warn = 'Trashed ' + $scope.document.displayName + '.';
            $scope.go('/deals/' + $scope.document.dealID);
        });
    };

    $scope.getDocUri = function (mimeType) {
        
        if ($scope.document
            && $scope.document.contentType
            && !mimeType
            && $scope.document.contentType.indexOf('/pdf') === -1
            && $scope.document.contentType.indexOf('/png') === -1
            && $scope.document.contentType.indexOf('/gif') === -1
            && $scope.document.contentType.indexOf('/svg') === -1
            && $scope.document.contentType.indexOf('/jpg') === -1
            && $scope.document.contentType.indexOf('/jpeg') === -1
            && $scope.document.contentType.indexOf('text/') === -1
            ) {
            return '';
        } else {
            var preview = '';
            if (!mimeType) {
                preview = '?preview=true';
            }
            return '/api/DownloadDocument/' + $scope.document.documentID + preview;
        }
    }

    $scope.sendToSigner = function () {
        $scope.sending = true;
        var isInstant = $scope.signatureRequest.signerEmail === $scope.authentication.userName;

        $scope.signatureRequest.signerPersonID = $scope.signerData.signer.personID;
        $scope.signatureRequest.save({ sendEmail: !isInstant }).then(function (result) {
            $scope.sending = false;
            if (isInstant) {
                api.userID.get().$promise.then(function (info) {
                    $scope.go('/secure-sign/' + result.requestID + '/' + info.userID);
                });
            } else {
                flash.to('form-flash').success = 'Signature request sent.';
                init();
            }
        });
    };

    $scope.recipientSelected = function () {
        $scope.signatureRequest.signerEmail = $scope.signerData.signer.email;
    };

    $scope.getPeople = function () {
        if ($scope.deal && $scope.deal.people) {
            var people = $scope.deal.people.sort(function (a, b) {
                if (a.lastName < b.lastName) {
                    return -1;
                } else if (a.lastName > b.lastName) {
                    return 1;
                } else if (a.firstName < b.firstName) {
                    return -1;
                } else if (a.firstName > b.firstName) {
                    return 1;
                } else {
                    return 0;
                }
            });
            if ($scope.document.signatureFields) {
                people = $.grep(people, function (person) {
                    var matchingSignerRoles = $.grep($scope.document.signatureFields, function (field) {
                        return person.roleName === field.roleName;
                    });
                    return matchingSignerRoles.length > 0;
                });
            }
            if ($scope.document.signatureRequests) {
                people = $.grep(people, function (person) {
                    var matchingSignaturesExecuted = $.grep($scope.document.signatureRequests, function (request) {
                        return request.allSignaturesConfirmedAt && request.signer.personID === person.personID;
                    });
                    return matchingSignaturesExecuted.length == 0;
                });
            }
            return people;
        } else {
            return [];
        }
    };

    init();
}]);
angular.module('main').controller('documentSignaturesController', ['$scope', 'models', '$routeParams', 'flash','$q', function ($scope, models, $routeParams, flash, $q) {
    // models
    $scope.document = [];
    $scope.roles = [];
    $scope.signatureTypes = [{ name: 'Signature' }, { name: 'Initials' }];

    // scope vars
    $scope.loading = true;
    $scope.page = 1;
    
    // api interaction
    $scope.save = function () {
        $scope.document.save().then(function () {
            flash.success = "Signatures saved.";
        });
    };

    // push/splice
    $scope.addSignatureField = function () {
        var newSig = new models.SignatureInfo({
            documentID: $scope.document.documentID,
            pageNumber: $scope.page
        });
        $scope.document.signatureFields.push(newSig);
        $scope.select(newSig);
    };

    $scope.trashSelectedSignature = function () {
        var sig = $scope.selectedSig;

        if (sig) {
            var sigIndex = $scope.document.signatureFields.indexOf(sig);
            $scope.document.signatureFields.splice(sigIndex, 1);
            $scope.select($scope.document.signatureFields[0]);
        }
    };

    // signature date
    $scope.addSignatureDate = function (sig) {
        sig.hasDate = true;
        sig.dateX = sig.x + sig.width + 10;
        sig.dateY = sig.y;
        sig.dateWidth = 70;
        sig.dateHeight = 15;
    };

    $scope.removeSignatureDate = function (sig) {
        sig.hasDate = false;
        sig.dateX = '';
        sig.dateY = '';
        sig.dateWidth = '';
        sig.dateHeight = '';
    };

    // selection helpers
    $scope.getSigsForPage = function (page) {
        if (!$scope.document || !$scope.document.signatureFields) {
            return [];
        }
        return $.grep($scope.document.signatureFields, function (sig) {
            return sig.pageNumber == page;
        });
    };

    $scope.selected = function (sig) {
        return $scope.selectedSig == sig;
    };

    $scope.select = function (sig) {
        $scope.selectedSig = sig;
    };

    // styling
    $scope.getPageContainerDivStyle = function (page) {
        if ($scope.document && $scope.document.pages) {
            var index = page - 1;
            var img = $.grep($scope.document.pages, function (entity) { return entity.pageNumber == page })[0];
            if (img) {
                return {
                    'border': '1px solid #AAA',
                    'width': (img.pageWidth + 2) + 'px',
                    'height': (img.pageHeight + 2) + 'px',
                    margin: 'auto'
                };
            }
        }
    };

    $scope.getPageImageDivStyle = function () {
        if ($scope.document && $scope.document.pages) {
            var index = $scope.page - 1;
            var img = $.grep($scope.document.pages, function (entity) { return entity.pageNumber == $scope.page })[0];

            if (img) {
                return {
                    'position': 'relative',
                    'overflow': 'hidden',
                    'background': "url('/api/downloadblobpng/" + img.pageID + "?container=page-image')",
                    'width': img.pageWidth + 'px',
                    'height': img.pageHeight + 'px'
                };
            }
        }
    };

    $scope.getSignatureBoxStyle = function (sig) {
        if ($scope.document && $scope.document.pages) {
            var index = $scope.page - 1;
            var img = $.grep($scope.document.pages, function (entity) { return entity.pageNumber == $scope.page })[0];
            var background = '#ddd';
            if ($scope.selectedSig == sig) {
                background = '#d5fadd';
            }
            return {
                'position': 'absolute',
                'background': background,
                'width': (sig.width) + 'px',
                'height': (sig.height) + 'px',
                'left': (sig.x) + 'px',
                'top': (img.pageHeight - (sig.y)) + 'px',
                'border': '1px solid #777',
                'line-height': (sig.height - 2) + 'px',
                'font-size': (sig.height - 2) + 'px',
                'overflow': 'hidden'
            };
        }
    };

    $scope.getDateBoxStyle = function (sig) {
        if ($scope.document && $scope.document.pages) {
            var index = $scope.page - 1;
            var img = $.grep($scope.document.pages, function (entity) { return entity.pageNumber == $scope.page })[0];
            var background = '#ddd';
            if ($scope.selectedSig == sig) {
                background = '#fcf8e3';
            }
            return {
                'position': 'absolute',
                'background': background,
                'width': (sig.dateWidth) + 'px',
                'height': (sig.dateHeight) + 'px',
                'left': (sig.dateX) + 'px',
                'top': (img.pageHeight - (sig.dateY)) + 'px',
                'border': '1px solid #777',
                'line-height': (sig.dateHeight - 2) + 'px',
                'font-size': (sig.dateHeight - 2) + 'px',
                'overflow': 'hidden'
            };
        }
    };

    // drag/resize events
    $scope.dateResized = function (event, ui) {


        var dateID = event.target.id;
        var sig = $scope.selectedSig;
        if (sig) {

            var dw = ui.size.width - ui.originalSize.width;
            var dh = ui.size.height - ui.originalSize.height;
            var dx = ui.position.left - ui.originalPosition.left;
            var dy = ui.position.top - ui.originalPosition.top;

            sig.dateWidth += dw;
            sig.dateHeight += dh;

            sig.dateX += dx;
            sig.dateY -= dy; // y is from the bottom, so it's opposite

            sig.dateWidth = Math.round(sig.dateWidth);
            sig.dateHeight = Math.round(sig.dateHeight);
            sig.dateX = Math.round(sig.dateX);
            sig.dateY = Math.round(sig.dateY);
        }
    };

    $scope.dateMoved = function (event, ui) {
        var dateID = event.target.id;
        var sig = $scope.selectedSig;
        if (sig) {
            var dx = ui.position.left - ui.originalPosition.left;
            var dy = ui.position.top - ui.originalPosition.top;

            sig.dateX += dx;
            sig.dateY -= dy; // y is from the bottom, so it's opposite

            sig.dateX = Math.round(sig.dateX);
            sig.dateY = Math.round(sig.dateY);
        }
    };

    $scope.resized = function (event, ui) {


        var sigID = event.target.id;
        var sig = $scope.selectedSig;
        if (sig) {

            var dw = ui.size.width - ui.originalSize.width;
            var dh = ui.size.height - ui.originalSize.height;
            var dx = ui.position.left - ui.originalPosition.left;
            var dy = ui.position.top - ui.originalPosition.top;

            sig.width += dw;
            sig.height += dh;

            sig.x += dx;
            sig.y -= dy; // sig.y is from the bottom, so it's opposite

            sig.width = Math.round(sig.width);
            sig.height = Math.round(sig.height);
            sig.x = Math.round(sig.x);
            sig.y = Math.round(sig.y);
        }
    };

    $scope.moved = function (event, ui) {
        var sigID = event.target.id;
        var sig = $scope.selectedSig;
        if (sig) {
            var img = $.grep($scope.document.pages, function (entity) { return entity.pageNumber == $scope.page })[0];

            var left = ui.position.left;
            var top = ui.position.top;
            sig.x = left;
            sig.y = img.pageHeight - top;

            sig.x = Math.round(sig.x);
            sig.y = Math.round(sig.y);
        }
    };

    // init/page setup
    var init = function () {
        proms = [];
        proms.push(models.Document.find($routeParams.documentID).then(function (doc) {
            $scope.document = doc;
        }));
        proms.push(models.PersonRole.query().then(function (roles) {
            $scope.roles = roles;
        }));
        $q.all(proms).then(function () {
            $scope.loading = false;
        });
    };

    init();
}]);
angular.module('main').controller('formController', ['$scope', 'models', '$routeParams', 'flash', function ($scope, models, $routeParams, flash) {
    $scope.loaded = false;
    $scope.saving = false;
    $scope.finishing = false;
    
    $scope.dealID = $routeParams.dealID;
    $scope.formID = $routeParams.formID;

    $scope.form = {};
    $scope.deal = {};
    $scope.template = {};

    // greps and such
    $scope.getFormUri = function () {
        return '/api/DownloadForm/' + $scope.form.formID + '?container=filled-form';
    };

    $scope.getFormField = function (acroname) {
        var results = $.grep($scope.form.fields, function (field) {
            return field.fieldName === acroname;
        });

        if (results.length == 0) {
            var field = {
                formFieldID: '00000000-0000-0000-0000-000000000000',
                formID: '00000000-0000-0000-0000-000000000000',
                fieldName: acroname,
                fieldValue: 'NOT FOUND'
            };
            $scope.form.fields.push(field);
            return field;
        } else {
            return results[0];
        }
    };

    $scope.getFields = function () {
        var result = [];
        if ($scope.template && $scope.template.acroFields) {
            result = $.grep($scope.template.acroFields, function (field) {
                return field.editable;
            });
        }
        return result;
    };

    // form events
    $scope.destroyForm = function () {
        $scope.destroying = true;
        $scope.form.destroy().then(function () {
            $scope.destroying = false;
            flash.warn = "'" + $scope.form.displayName + "' destroyed.";
            $scope.go('/deals/' + $scope.form.dealID);
        });
    };

    $scope.saveFields = function (silent) {
        silent = !!silent;
        
        $scope.saving = !silent && true;
        return $scope.form.save().then(function () {
            flash.to('form-flash').success = 'Form saved.';
            $scope.saving = false;
        });
    };

    $scope.finish = function () {
        $scope.finishing = true;
        $scope.form.save({ finish: true }).then(function (form) {
            flash.success = 'Form saved.';
            $scope.finishing = false;
            $scope.go('/deals/' + $routeParams.dealID + '/documents/' + form.documentID);
        });
    };

    // init/page setup
    $scope.init = function () {
        models.Form.query({ id: $routeParams.formID }).then(function (form) {
            $scope.form = form;
            $scope.template = form.formTemplate;

            if (form.documentID) {
                $scope.go('/deals/' + $routeParams.dealID + '/documents/' + form.documentID);
            }

            $scope.loaded = true;
        });
        
        models.Deal.query({ id: $routeParams.dealID }).then(function (deal) {
            $scope.deal = deal;
        });
    };

    $scope.init();
}]);
angular.module('main').controller('homeController', ['$scope', 'models', function ($scope, models) {
    $scope.deals = [];
    $scope.dealFilter = {
        transactionType: '',
        transactionStatus: '!Archived',
        displayName: ''
    };
    $scope.loadingDeals = true;
    $scope.fillDeals = function () {
        return models.Deal.query().then(function (data) {
            $scope.deals = data;
            $scope.loadingDeals = false;
        });
    };

    // init/page setup
    var init = function () {
        $scope.fillDeals();
    };

    init();
}]);
'use strict';
app.controller('loginController', ['$rootScope', '$scope', 'authService','flash','$routeParams', function ($rootScope, $scope, authService, flash, $routeParams) {
    $rootScope.hideMenu = true;
    $scope.showLogin = false;
    $scope.loginData = {
        userName: "",
        password: ""
    };

    $scope.login = function (dealID) {
        $scope.loggingOn = true;
        return authService.login($scope.loginData).then(function (response) {
            $rootScope.hideMenu = false;
            flash.error = '';

            if (dealID) {
                $scope.go('/deals/' + dealID);
            } else if ($rootScope.forbiddenPath) {
                $scope.go($rootScope.forbiddenPath);
            }
            else {
                $scope.go('/deals');
            }
            
        }, function (err) {
            $scope.loggingOn = false;
            $scope.showLogin = true;
            $scope.showAutoLogin = false;
            flash.error = err.error_description || 'An error occurred.';
        });
    };

    var init = function () {
        if ($routeParams.token) {
            $scope.loginData.userName = $routeParams.email;
            $scope.loginData.password = $routeParams.password;
            var dealID = $routeParams.dealID;
            authService.confirmAccount({ userId: $routeParams.userId, token: $routeParams.token }).then(function () {
                return $scope.login(dealID);
            });
            $scope.showAutoLogin = true;
        } else {
            $scope.showLogin = true;
            
        }
    };
    init();
}]);
angular.module('main').controller('lostPasswordController', ['$scope', 'authService', '$routeParams', '$rootScope', function ($scope, authService, $routeParams, $rootScope) {
    $scope.email = '';
    $rootScope.hideMenu = true;
    
    $scope.sendRecoveryEmail = function () {
        $scope.saving = true;
        authService.sendRecoveryEmail({ email: $scope.email }).then(function (result) {
            $scope.saving = false;
            $scope.emailSent = true;
        });
    };
}]);
'use strict';
app.controller('mySignatureController', ['$scope','models','flash', function ($scope, models, flash) {
    // models
    $scope.fonts = [];
    $scope.userSignatureInfo = {};

    $scope.loaded = false;
    $scope.saving = false;

    // api calls
    $scope.save = function () {
        $scope.saving = true;
        $scope.userSignatureInfo.save().then(function () {
            flash.success = 'Signature saved.';
            $scope.saving = false;
        });
    };

    // helpers
    $scope.getStyle = function (font) {
        if (!font) {
            font = {};
            font.cssFamily = '';
        }
        return {
            'font-family': font.cssFamily,
            'font-size':'24px'
        };
    };
    $scope.getFont = function (name) {
        return $.grep($scope.fonts, function (font) { return name === font.name; })[0];
    };

    $scope.setInitialFont = function (font) {
        $scope.userSignatureInfo.initialsFont = font.name;
    }

    $scope.setSignatureFont = function (font) {
        $scope.userSignatureInfo.signatureFont = font.name;
    }

    // setup
    var init = function () {
        return models.Font.query().then(function (fonts) {
            return $scope.fonts = fonts;
        }).then(function () {
            return models.UserSignatureInfo.query().then(function (userSignatureInfo) {
                if (!userSignatureInfo.userID) {
                    return models.PersonalInfo.find().then(function (personalInfo) {
                        userSignatureInfo = new models.UserSignatureInfo();
                        if (personalInfo.firstName && personalInfo.lastName) {
                            userSignatureInfo.signatureText = personalInfo.firstName + ' ' + personalInfo.lastName;
                            userSignatureInfo.initialsText = personalInfo.firstName.substring(0, 1) + '.' + personalInfo.lastName.substring(0, 1) + '.';
                        } else {
                            userSignatureInfo.signatureText = 'John';
                            userSignatureInfo.initialsText = 'Hancock';
                        }
                        userSignatureInfo.signatureFont = userSignatureInfo.initialsFont = $scope.fonts[0].name;
                        $scope.userSignatureInfo = userSignatureInfo;
                        $scope.loaded = true;
                        return $scope.userSignatureInfo.save();
                    });
                } else {
                    $scope.loaded = true;
                    return $scope.userSignatureInfo = userSignatureInfo;
                }
            });
        });
    };
    init();
}]);
angular.module('main').controller('newDealController', ['$scope', 'models','flash','$q', function ($scope, models, flash, $q) {
    //models
    $scope.deal = new models.Deal();
    $scope.property = new models.Property();
    $scope.saving = false;

    // form submission
    $scope.saveDealInfo = function () {
        $scope.saving = true;
        // first the deal
        return $scope.deal.save().then(function (dealData) {
            var allSaves = [];
            $scope.deal.dealID = dealData.dealID;
            $scope.property.dealID = $scope.deal.dealID;

            // now all the extras
            allSaves.push($scope.property.save());
            
            return $q.all(allSaves).then(function (b2Data) {
                $scope.saving = false;
                flash.success = 'Deal "' + $scope.deal.displayName + '" created.';
                return $scope.go('/deals/' + $scope.deal.dealID);
            });
            
        }, function (error) {
            flash.error = 'Error while creating deal. Please try again or contact support.';
        });
    };
}]);
app.controller('passwordResetController', ['$rootScope', '$scope', 'authService', 'flash', '$routeParams', function ($rootScope, $scope, authService, flash, $routeParams) {
    $rootScope.hideMenu = true;

    $scope.saving = false;

    $scope.recovery = {
        newPassword: "",
        confirmPassword: ""
    };

    $scope.resetPassword = function () {

        $scope.saving = true;
        $scope.recovery.userId = $routeParams.userId;
        $scope.recovery.token = $routeParams.token;

        authService.resetPasswordWithToken($scope.recovery).then(function (response) {
            $scope.saving = false;
            flash.success = 'Your password has been reset. You may now sign in.';
            $scope.go('/');
        },
         function (response) {
             $scope.saving = false;
             var errors = [];
             for (var key in response.data.modelState) {
                 for (var i = 0; i < response.data.modelState[key].length; i++) {
                     errors.push(response.data.modelState[key][i]);
                 }
             }
             flash.error = "Error during password reset: " + errors.join(' ');
         });
    };
}]);
angular.module('main').controller('personController', ['$scope', 'models', '$routeParams', 'flash', '$q', function ($scope, models, $routeParams, flash, $q) {
    $scope.person = {};
    $scope.savingPerson = false;
    $scope.refreshingPerson = false;
    $scope.dealID = $routeParams.dealID;
    $scope.roles = [];
    $scope.dealAccess = {};
    $scope.shareDeal = false;

    // form events
    $scope.savePerson = function () {
        $scope.savingPerson = true;
        $scope.person.save().then(function (person) {
            var promises = [];
            if ($scope.shareDeal && !$scope.dealWasShared) {
                // share it, it's not currently shared
                var da = new models.DealAccess();
                da.dealID = $routeParams.dealID;
                da.personID = person.personID;
                promises.push(da.save());
            } else if (!$scope.shareDeal && $scope.dealWasShared) {
                // unshare it, it's currently shared
                promises.push($scope.dealAccess.destroy());
            }
            $q.all(promises).then(function () {
                $scope.savingPerson = false;
                flash.success = 'Person saved.';
                
                $scope.go('/deals/' + $routeParams.dealID);
            });
        });
    };

    // init/page setup
    var init = function () {
        models.PersonRole.query().then(function (roles) {
            $scope.roles = roles;
        });

        if (!$routeParams.personID) {
            $scope.person = new models.Person();
            $scope.person.dealID = $routeParams.dealID;
            $scope.person.roleName = "Seller's Agent";
        } else {
            models.DealAccess.query($routeParams.dealID, $routeParams.personID).then(function (da) {
                $scope.dealAccess = da;
                $scope.dealWasShared = $scope.shareDeal = ($scope.dealAccess.accessID !== '00000000-0000-0000-0000-000000000000' && !$scope.dealAccess.revoked);
            });
            models.Person.find($routeParams.personID).then(function (person) {
                $scope.person = person;
            });
        }
    };

    init();
}]);
'use strict';
app.controller('personalInfoController', ['$scope','models','flash', function ($scope, models, flash) {
    // models
    $scope.personalInfo = {};
    $scope.personalInfoLoaded = false;
    $scope.savingPersonalInfo = false;

    // api calls
    $scope.savePersonalInfo = function () {
        $scope.savingPersonalInfo = true;
        $scope.personalInfo.save().then(function () {
            $scope.savingPersonalInfo = false;
            flash.success = 'Personal information saved.';
        });
    };

    // setup
    var init = function () {
        models.PersonalInfo.find().then(function (personalInfo) {
            $scope.personalInfo = personalInfo;
            $scope.personalInfoLoaded = true;
        });
    };
    init();
}]);
angular.module('main').controller('propertyController', ['$scope', 'models', '$routeParams', 'flash', function ($scope, models, $routeParams, flash) {
    $scope.property = {};
    $scope.savingProperty = false;
    $scope.refreshingProperty = false;
    $scope.dealID = $routeParams.dealID;
    // form events
    $scope.saveProperty = function () {
        $scope.savingProperty = true;
        $scope.property.save().then(function () {
            $scope.savingProperty = false;
            flash.success = 'Property info saved.';
            $scope.$back();
        });
    };

    // init/page setup
    var init = function () {
        return models.Property.find($routeParams.propertyID).then(function (property) {
            $scope.property = property;
        });
    };

    init();
}]);
'use strict';
app.controller('signatureCollectionController', ['$rootScope', '$scope', 'models', 'flash', '$routeParams','$q','api', function ($rootScope,$scope, models, flash, $routeParams,$q,api) {
    // models
    $scope.fonts = [];
    $scope.signatureRequest = [];
    $rootScope.hideMenu = true;
    $scope.signatureAdopted = false;
    $scope.consentGiven = false;
    $scope.documentConfirmed = false;
    $scope.mySignature = [];

    $scope.requestID = $routeParams.requestID;
    $scope.userID = $routeParams.userID;

    $scope.requester = [];
    $scope.loaded = false;

    // signatures
    $scope.signaturePositions = [];
    $scope.signatures = [];
    
    // form submissions
    $scope.giveConsent = function () {
        models.documentSigning.recordEvent('consentGiven', $scope.requestID).then(function () {
            $scope.consentGiven = true;
        }, function () {
            flash.error = 'Something went wrong. Please try again.';
        });
    };

    $scope.adoptSignature = function () {
        models.documentSigning.recordEvent('signatureAdopted', $scope.requestID).then(function () {
            $scope.signatureAdopted = true;
        }, function () {
            flash.error = 'Something went wrong. Please try again.';
        });
    };

    $scope.confirmDocument = function () {
        models.Signature.saveAll($scope.signatures)
            .then(function () {
                $scope.documentConfirmed = true;
            });
    };
    
    $scope.confirmButtonEnabled = function () {
        var unsignedPositions = $.grep($scope.signaturePositions, function (position) {
            return !position.signed;
        });
        return unsignedPositions.length === 0;
    };

    // click events
    $scope.sign = function (position) {
        var sig = new models.Signature();
        sig.requestID = $scope.requestID;
        sig.signatureInfoID = position.signatureInfoID;
        sig.dealID = $scope.signatureRequest.dealID;
        if (position.signatureType == 'Signature') {
            sig.fontName = $scope.mySignature.signatureFont;
            sig.signatureText = $scope.mySignature.signatureText;
        } else {
            sig.fontName = $scope.mySignature.initialsFont;
            sig.signatureText = $scope.mySignature.initialsText;
        }

        sig.executedAt = new Date();

        $scope.signatures.push(sig);
        position.signed = true;
    };

    // helpers
    $scope.getStyle = function (font) {
        if (!font) {
            font = {};
            font.cssFamily = '';
        }
        return {
            'font-family': font.cssFamily,
            'font-size': '24px'
        };
    };

    $scope.getSignatureForPosition = function (position) {
        var sig = $.grep($scope.signatures, function (signature) {
            return signature.signatureInfoID == position.signatureInfoID;
        })[0];
        return sig;
    };

    $scope.getSignatureTextStyle = function (position) {
        if (position.signed) {
            var sig = $scope.getSignatureForPosition(position);
            if (sig) {
                return {
                    'font-family': $scope.getFont(sig.fontName).cssFamily
                };
            }
        }
    };

    $scope.getSignatureText = function (position) {
        if (position.signed) {
            var sig = $scope.getSignatureForPosition(position);
            if (sig) {
                return sig.signatureText;
            }
        }
    };

    $scope.getSignatureDate = function (position) {
        if (position.signed) {
            var sig = $scope.getSignatureForPosition(position);
            if (sig) {
                return sig.executedAt;
            }
        }
    };


    $scope.getFont = function (name) {
        return $.grep($scope.fonts, function (font) { return name === font.name; })[0];
    };

    $scope.setInitialFont = function (font) {
        $scope.mySignature.initialsFont = font.name;
    }

    $scope.setSignatureFont = function (font) {
        $scope.mySignature.signatureFont = font.name;
    }

    $scope.getSignaturesOnPage = function (pageNumber) {
        if ($scope.signaturePositions) {
            return $.grep($scope.signaturePositions, function (position) {
                return position.pageNumber === pageNumber;
            });
        }
    };

    $scope.getContainerStyleForPage = function (pageNumber) {
        if ($scope.pages) {
            var page = $.grep($scope.pages, function (p) {
                return p.pageNumber == pageNumber;
            })[0];
            if (page) {
                return {
                    position:'relative',
                    border:'1px solid #777',
                    margin:'auto',
                    width: (page.pageWidth + 2) + 'px',
                    height: (page.pageHeight + 2) + 'px'
                };
            }
        }
    }

    $scope.getStyleForPage = function (pageNumber) {
        if ($scope.pages) {
            var page = $.grep($scope.pages, function (p) {
                return p.pageNumber == pageNumber;
            })[0];
            if (page) {
                return {
                    position: 'relative',
                    margin: 'auto',
                    width: (page.pageWidth) + 'px',
                    height: (page.pageHeight) + 'px',
                    background: "url('/api/DownloadBlobPng/" + page.pageID + "?container=page-image')"
                };
            }
        }
    }

    $scope.getSignatureStyle = function (position) {
        return {
            position: 'absolute',
            bottom: (position.fieldY - position.fieldHeight)  + 'px',
            left: 0,
            'margin-left': position.fieldX + 'px',
            width: position.fieldWidth + 'px',
            height:position.fieldHeight + 'px',
            'line-height': position.fieldHeight + 'px',
            'font-size': position.fieldHeight + 'px',
            background: '#FFFFC0'
        };
    };

    $scope.getSignatureButtonStyle = function (position, pageNumber) {
        if ($scope.pages) {
            var page = $.grep($scope.pages, function (p) {
                return p.pageNumber == pageNumber;
            })[0];
            if (page) {
                return {
                    position: 'absolute',
                    top: (page.pageHeight - position.fieldY - 7) + 'px',
                    left: (position.fieldX - 60) + 'px',
                    width: position.fieldWidth + 'px',
                    'line-height': position.fieldHeight + 'px',
                    height: position.fieldHeight + 'px'
                };
            }
        }
    };

    $scope.getDateStyle = function (position) {
        if (position.hasDate) {
            return {
                position: 'absolute',
                bottom: (position.dateFieldY - position.dateFieldHeight) + 'px',
                left: 0,
                'margin-left': position.dateFieldX + 'px',
                width: position.dateFieldWidth + 'px',
                height: position.dateFieldHeight + 'px',
                'line-height': position.dateFieldHeight + 'px',
                'font-size': position.dateFieldHeight + 'px',
                background: '#FFFFC0',
                overflow: 'hidden'
            };
        }
    };

    $scope.getChevronStyle = function (position) {
        return {
            position: 'absolute',
            'margin-top': (position.pageHeight - position.fieldY) + 'px',
            left: '-120px',
            width: '100px',
            'text-align':'right',
            'line-height': '14px',
            'font-size': '14px',
            height: '14px'
        };
    };

    $scope.getSignatureUrl = function () {
        return "/api/DownloadBlobPng/" + $scope.signatureRequest.requestID + "?container=signatures";
    };

    // setup
    var init = function () {

        models.SignatureRequest.query({ id: $scope.requestID }).then(function (fsr) {
            $scope.pages = fsr.document.pages;
            $scope.document = fsr.document;
            $scope.signatureRequest = fsr.request;
            $scope.signatureRequest;
            $scope.fonts = fsr.fonts;
            $scope.requester = fsr.requesterInfo;
            $scope.signaturePositions = fsr.signaturePositions;
            $scope.signer = $scope.signatureRequest.signer;

            var defaultSig = {
                signatureFont: $scope.fonts[0].name,
                initialsFont: $scope.fonts[0].name,
                signatureText: $scope.signatureRequest.signer.firstName + ' ' + $scope.signatureRequest.signer.lastName,
                initialsText: $scope.signatureRequest.signer.firstName[0] + '.' + $scope.signatureRequest.signer.lastName[0] + '.'
            };

            $scope.mySignature = defaultSig;

            if ($scope.userID) {
                api.userSignature.get({ userID: $scope.userID }).$promise.then(function (sig) {
                    if (sig.userID) {
                        $scope.mySignature = sig;
                    }
                    $scope.loaded = true;
                });
            } else {
                $scope.loaded = true;
            }
            
        }, function () {
            $scope.loadFailed = true;
        });
        
    };
    init();
}]);
'use strict';
app.controller('signupController', ['$rootScope', '$scope', 'authService','flash', function ($rootScope, $scope, authService, flash) {
    $rootScope.hideMenu = true;
    $scope.saving = false;

    $scope.registration = {
        userName: "",
        password: "",
        confirmPassword: ""
    };

    $scope.signUp = function () {

        $scope.saving = true;
        authService.saveRegistration($scope.registration).then(function (response) {
            $scope.saving = false;
            $scope.registered = true;

        },
         function (response) {
             var errors = [];
             for (var key in response.data.modelState) {
                 for (var i = 0; i < response.data.modelState[key].length; i++) {
                     errors.push(response.data.modelState[key][i]);
                 }
             }
             flash.error = "Error during registration: " + errors.join(' ');
         });
    };
}]);
angular.module('main').controller('verifyDocumentController', ['$rootScope', '$scope', '$routeParams', '$q', 'flash', 'api', function ($rootScope, $scope, $routeParams, $q, flash, api) {
    $scope.info = [];
    $rootScope.hideMenu = true;
    $scope.loaded = false;
    // init/page setup
    var init = function () {
        api.documentVerifier.get({ documentID: $routeParams.documentID }).$promise.then(function (info) {
            $scope.info = info;
            $scope.loaded = true;
        });
    };

    init();
}]);
angular.module('context', [])
    .service('webApiLayer', ['$resource', '$q', function ($resource, $q) {
        var API = function (entity) {
            return $resource('/api/' + entity + '/:id', {}, { getOne: { method: 'GET', isArray: false }, getByID: { method: 'GET', isArray: false }, update: { method: 'PUT' }, insert: { method: 'POST' }, destroy: { method: 'DELETE' } });
        };

        this.find = function (entity, model, id) {
            var api = new API(entity);
            return api.getByID({ id: id }).$promise.then(function (entity) {
                var result = new model(entity);
                return result;
            }, function (res) {
                return $q.reject(res);
            });
        };

        this.query = function (entity, model, params, returnsArray) {
            if (returnsArray !== false) {
                returnsArray = true;
            }
            params = params || { };
            var api = new API(entity);

            if (params.id && !returnsArray) {
                return api.getByID(params).$promise.then(function (entity) {
                    var result = new model(entity);
                    return result;
                }, function (res) {
                    return $q.reject(res);
                });
            } else if (!returnsArray) {
                return api.getOne(params).$promise.then(function (data) {
                    return new model(data);
                }, function (res) {
                    return $q.reject(res);
                });
            } else {
                return api.query(params).$promise.then(function (data) {
                    var result = [];
                    angular.forEach(data, function (element) {
                        result.push(new model(element));
                    });
                    return result;
                }, function (res) {
                    return $q.reject(res);
                });
            }
        };

        this.update = function (entity, id, element, params) {
            if (params) {
                params.id = id;
            } else {
                params = {
                    id: id
                };
            }
            return new API(entity).update(params, element).$promise;
        };

        this.insert = function (entity, element, params) {
            params = params || {};
            return new API(entity).insert(params, element).$promise;
        };

        this.destroy = function (entity, id, params) {
            if (!params) {
                params = {};
            }
            params.id = id;
            return new API(entity).destroy(params).$promise;
        }
    }]);
var mods = angular.module('data', []);

mods.factory('api', ['$resource', function ($resource) {
    var api = {};

    api.documentSigning = {
        recordEvent: function(signingEvent, requestID){
            var rest = $resource('/api/docsigning/' + signingEvent + '/' + requestID, {}, { send: { method: 'PUT' } });
            return rest.send().$promise;
        },
    };

    api.documentVerifier = $resource('/api/docsigning/verify/:documentID', { documentID: '@id' });

    api.userSignature = $resource('/api/usersignatureinfo/:userID', { userID: '@id' });
    api.userID = $resource('/api/userid');

    return api;
}]);
'use strict';
app.factory('authInterceptorService', ['$q', '$location', 'localStorageService', 'flash', '$rootScope', function ($q, $location, localStorageService, flash, $rootScope) {

    var authInterceptorServiceFactory = {};

    var _request = function (config) {

        config.headers = config.headers || {};

        var authData = localStorageService.get('authorizationData');
        if (authData) {
            config.headers.Authorization = 'Bearer ' + authData.token;
        }

        return config;
    }

    var _responseError = function (rejection) {
        
        if (rejection.status === 401) {
            flash.warn = 'Please sign in to continue.';
            $rootScope.forbiddenPath = $location.path();
            $location.path('/');

        } else if (rejection.status >= 500) {
            flash.error = 'Connection error. Please try again in a few moments.';
        }

        return $q.reject(rejection);
    }

    authInterceptorServiceFactory.request = _request;
    authInterceptorServiceFactory.responseError = _responseError;

    return authInterceptorServiceFactory;
}]);
'use strict';
app.factory('authService', ['$http', '$q', 'localStorageService', function ($http, $q, localStorageService) {

    var serviceBase = '/';
    var authServiceFactory = {};

    var _authentication = {
        isAuth: false,
        userName: ""
    };

    var _saveRegistration = function (registration) {

        _logOut();

        return $http.post(serviceBase + 'api/register/signup', registration).then(function (response) {
            return response;
        });

    };

    var _confirmAccount = function (confirmation) {
        return $http.put(serviceBase + 'api/register/confirm', confirmation).then(function (response) {
            return response;
        });
    };

    var _sendRecoveryEmail = function (recovery) {
        return $http.post(serviceBase + 'api/recovery/send', recovery).then(function (response) {
            return response;
        });
    };

    var _resetPasswordWithToken = function (resetInfo) {
        return $http.put(serviceBase + 'api/recovery/resetpassword', resetInfo).then(function (response) {
            return response;
        });
    };

    var _login = function (loginData) {

        var data = "grant_type=password&username=" + loginData.userName + "&password=" + loginData.password;

        var deferred = $q.defer();

        $http.post(serviceBase + 'token', data, { headers: { 'Content-Type': 'application/x-www-form-urlencoded' } }).success(function (response) {

            localStorageService.set('authorizationData', { token: response.access_token, userName: loginData.userName });



            _authentication.isAuth = true;
            _authentication.userName = loginData.userName;

            deferred.resolve(response);

        }).error(function (err, status) {
            _logOut();
            deferred.reject(err);
        });

        return deferred.promise;

    };

    var _logOut = function () {

        localStorageService.remove('authorizationData');

        _authentication.isAuth = false;
        _authentication.userName = "";

    };

    var _fillAuthData = function () {
        var authData = localStorageService.get('authorizationData');
        if (authData) {
            _authentication.isAuth = true;
            _authentication.userName = authData.userName;
        }
    }

    authServiceFactory.saveRegistration = _saveRegistration;
    authServiceFactory.login = _login;
    authServiceFactory.logOut = _logOut;
    authServiceFactory.fillAuthData = _fillAuthData;
    authServiceFactory.authentication = _authentication;
    authServiceFactory.confirmAccount = _confirmAccount;
    authServiceFactory.sendRecoveryEmail = _sendRecoveryEmail;
    authServiceFactory.resetPasswordWithToken = _resetPasswordWithToken;

    return authServiceFactory;
}]);
var mods = angular.module('models', []);

mods.factory('models', ['webApiLayer', '$resource', function (api, $resource) {
    var models = {};

    models.documentSigning = {
        recordEvent: function(signingEvent, requestID){
            var eventApi = $resource('/api/docsigning/' + signingEvent + '/' + requestID, {}, { send: { method: 'PUT' } });
            return eventApi.send().$promise;
        }
    };

    // begin Document
    models.Document = function (extender) {
        this.documentID = '00000000-0000-0000-0000-000000000000';

        angular.extend(this, extender);
    };

    models.Document.entity = function () { return "Documents"; };

    models.Document.find = function (id) {
        return api.find(models.Document.entity(), models.Document, id);
    };

    models.Document.query = function (params) {
        params = params || {};
        return api.query(models.Document.entity(), models.Document, params);
    };

    models.Document.prototype.save = function () {
        return api.update(models.Document.entity(), this.documentID, this);
    };

    models.Document.prototype.destroy = function () { return api.destroy(models.Document.entity(), this.documentID); };
    // end Document

    // begin SignatureInfo
    models.SignatureInfo = function (extender) {
        this.SignatureInfoID = '00000000-0000-0000-0000-000000000000';
        this.formTemplateID = '00000000-0000-0000-0000-000000000000';
        this.roleName = '';
        this.signatureType = 'Signature';

        this.x = 50;
        this.y = 50;
        this.width = 200;
        this.height = 16;

        this.pageNumber = 1;

        this.hasDate = false;
        this.dateX = '';
        this.dateY = '';
        this.dateWidth = '';
        this.dateHeight = '';

        angular.extend(this, extender);
    };
    // end SignatureInfo

    // begin Signature
    models.Signature = function (extender) {
        this.signatureID = '00000000-0000-0000-0000-000000000000';
        this.requestID = '00000000-0000-0000-0000-000000000000';
        this.signatureInfoID = '00000000-0000-0000-0000-000000000000';
        this.fontName = '';
        this.signatureText = '';
        this.executedAt = null;

        angular.extend(this, extender);
    };

    models.Signature.entity = function () { return "Signatures"; };

    models.Signature.saveAll = function (signatures) {
        return api.insert(models.Signature.entity(), signatures);
    };
    // end Signature

    // begin SignatureRequest
    models.SignatureRequest = function (extender) {
        this.requestID = '00000000-0000-0000-0000-000000000000';
        this.dealID = '00000000-0000-0000-0000-000000000000';
        this.documentID = '00000000-0000-0000-0000-000000000000';
        this.signerPersonID = '00000000-0000-0000-0000-000000000000';
        this.signerEmail = '';


        this.revoked = false;
        angular.extend(this, extender);
    };

    models.SignatureRequest.entity = function () { return "SignatureRequests"; };

    models.SignatureRequest.query = function (params) {
        params = params || {};
        return api.query(models.SignatureRequest.entity(), models.SignatureRequest, params, !params.id);
    };

    models.SignatureRequest.prototype.save = function (params) {
        return api.insert(models.SignatureRequest.entity(), this, params);
    };

    models.SignatureRequest.prototype.destroy = function () { return api.destroy(models.SignatureRequest.entity(), this.requestID); };
    // end SignatureRequest

    // begin DealAccess
    models.DealAccess = function (extender) {
        this.accessID = '00000000-0000-0000-0000-000000000000';
        this.dealID = '00000000-0000-0000-0000-000000000000';
        this.personID = '00000000-0000-0000-0000-000000000000';
        this.revoked = false;

        angular.extend(this, extender);
    };

    models.DealAccess.entity = function () { return "DealAccess"; };

    models.DealAccess.query = function (dealID, personID) {
        var params = { dealID: dealID };
        if (personID) {
            params.personID = personID;
        }
        return api.query(models.DealAccess.entity(), models.DealAccess, params, !params.personID);
    };

    models.DealAccess.prototype.save = function () {
        return api.insert(models.DealAccess.entity(), this);
    };
    models.DealAccess.prototype.destroy = function () { return api.destroy(models.DealAccess.entity(), this.accessID); };
    // end DealAccess

    // begin PersonRole
    models.PersonRole = function (extender) {
        this.roleName = '';
        this.roleDescription = '';

        angular.extend(this, extender);
    };

    models.PersonRole.entity = function () { return "PersonRoles"; };

    models.PersonRole.query = function (params) {
        params = params || {};
        return api.query(models.PersonRole.entity(), models.PersonRole, params, !params.id);
    };

    // end PersonRole

    // begin DealAudit
    models.DealAudit = function (extender) {
        this.actionTime = '';
        this.friendlyMessage = '';

        angular.extend(this, extender);
    };

    models.DealAudit.entity = function () { return "DealAudit"; };

    models.DealAudit.query = function (dealID) {
        return api.query(models.DealAudit.entity(), models.DealAudit, {id:dealID});
    };
    // end DealAudit

    // begin Form
    models.Form = function (extender) {
        this.formID = '00000000-0000-0000-0000-000000000000';
        this.formTemplateID = null;
        this.dealID = null;
        this.displayName = '';
        this.formType = '';

        angular.extend(this, extender);
    };

    models.Form.entity = function () { return "Forms"; };

    models.Form.query = function (params) {
        params = params || {};
        return api.query(models.Form.entity(), models.Form, params, !params.id);
    };

    models.Form.prototype.save = function (params) {
        if (this.formID && this.formID != '00000000-0000-0000-0000-000000000000') {
            return api.update(models.Form.entity(), this.formID, this, params);
        } else {
            return api.insert(models.Form.entity(), this);
        }
    };

    models.Form.prototype.destroy = function () { return api.destroy(models.Form.entity(), this.formID); };
    // end Form

    // begin FormTemplate
    models.FormTemplate = function (extender) {
        this.formTemplateID = '00000000-0000-0000-0000-000000000000';
        this.displayName = '';
        this.formType = '';
        this.jurisdiction = '';
        angular.extend(this, extender);
    };

    models.FormTemplate.entity = function () { return "formTemplates"; };

    models.FormTemplate.query = function (params) {
        params = params || {};
        return api.query(models.FormTemplate.entity(), models.FormTemplate, params, !params.id);
    };
    // end FormTemplate

    // begin CouponCheck
    models.CouponCheck = function (extender) {
        this.couponCode = '';
        this.discountPercent = 0;
        this.discountedPrice = 0;

        angular.extend(this, extender);
    };

    models.CouponCheck.entity = function () { return "ValidateCoupon"; };

    models.CouponCheck.query = function (couponCode) { return api.query(models.CouponCheck.entity(), models.CouponCheck, { couponCode: couponCode }, false); };
    // end CouponCheck

    // begin Subscription
    models.Subscription = function (extender) {
        this.cardName = '';
        this.cardNumber = '';
        this.cardCvc = '';
        this.cardExpirationYear = new Date().getFullYear().toString();
        this.cardExpirationMonth = '01';
        this.cardAddressLine1 = '';
        this.cardAddressLine2 = '';
        this.cardAddressCity = '';
        this.cardAddressState = '';
        this.cardAddressZip = '';

        angular.extend(this, extender);
    };

    models.Subscription.entity = function () { return "Subscribe"; };

    models.Subscription.prototype.save = function () {
        return api.insert(models.Subscription.entity(), this);
    };
    // end Subscription

    // begin SubscriptionCancellation
    models.SubscriptionCancellation = function (extender) {
        this.reason = '';

        angular.extend(this, extender);
    };

    models.SubscriptionCancellation.entity = function () { return "Unsubscribe"; };

    models.SubscriptionCancellation.prototype.save = function () {
        return api.insert(models.SubscriptionCancellation.entity(), this);
    };
    // end SubscriptionCancellation

    // begin Font
    models.Font = function (extender) {
        this.name = '';
        this.relativePath = '';
        this.cssFamily = '';

        angular.extend(this, extender);
    };

    models.Font.entity = function () { return "Fonts"; };

    models.Font.query = function () { return api.query(models.Font.entity(), models.Font, {}); };
    // end Font

    // begin Deal
    models.Deal = function (extender) {
        this.dealID = '00000000-0000-0000-0000-000000000000';
        this.displayName = '';
        this.jurisdiction = '';
        this.transactionType = 'Purchase/Sale';
        this.transactionStatus = 'Draft';

        angular.extend(this, extender);
    };

    models.Deal.entity = function () { return "Deals"; };

    models.Deal.query = function (params) {
        params = params || {};
        return api.query(models.Deal.entity(), models.Deal, params, !params.id);
    };

    models.Deal.find = function (id) {
        return api.find(models.Deal.entity(), models.Deal, id);
    };

    models.Deal.prototype.save = function () {
        if (this.dealID && this.dealID != '00000000-0000-0000-0000-000000000000') {
            return api.update(models.Deal.entity(), this.dealID, this);
        } else {
            return api.insert(models.Deal.entity(), this);
        }
    };

    models.Deal.prototype.destroy = function () { return api.destroy(models.Deal.entity(), this.dealID); };
    // end Deal

    // begin Property
    models.Property = function (extender) {
        this.propertyID = '00000000-0000-0000-0000-000000000000';
        this.taxID = '';
        this.address1 = '';
        this.address2 = '';
        this.city = '';
        this.county = '';
        this.state = 'FL';
        this.zipCode = '';
        this.legalDescription = '';

        angular.extend(this, extender);
    };
    models.Property.prototype.getAddressInline = function () {
        var address = this.address1;
        if (this.address2) {
            address += ', ' + this.address2;
        }
        address += ', ' + this.city;
        address += ', ' + this.county;
        address += ', ' + this.state;
        address += ' ' + this.zipCode;
        return address;
    };

    models.Property.prototype.getAddressLine1 = function () {
        var address = this.address1;
        if (this.address2) {
            address += ', ' + this.address2;
        }
        return address;
    };
    models.Property.prototype.getAddressLine2 = function () {
        var address = this.city;
        address += ', ' + this.county;
        address += ', ' + this.state;
        address += ' ' + this.zipCode;
        return address;
    };

    models.Property.entity = function () { return "Properties"; };

    models.Property.find = function (id) {
        return api.find(models.Property.entity(), models.Property, id);
    };

    models.Property.query = function (params) { return api.query(models.Property.entity(), models.Property, params); };

    models.Property.prototype.save = function () {
        if (this.propertyID && this.propertyID != '00000000-0000-0000-0000-000000000000') {
            return api.update(models.Property.entity(), this.propertyID, this);
        } else {
            return api.insert(models.Property.entity(), this);
        }
    };

    models.Property.prototype.destroy = function () { return api.destroy(models.Property.entity(), this.propertyID); };
    // end Property

    // begin Person
    models.Person = function (extender) {
        this.personID = '00000000-0000-0000-0000-000000000000';
        this.firstName = '';
        this.lastName = '';
        this.address1 = '';
        this.address2 = '';
        this.city = '';
        this.state = 'FL';
        this.zipCode = '';
        this.email = '';
        this.mobileNumber = '';
        this.roleName = '';
        this.dealID = '00000000-0000-0000-0000-000000000000';

        angular.extend(this, extender);
    };

    models.Person.entity = function () { return "People"; };
    models.Person.find = function (id) {
        return api.find(models.Person.entity(), models.Person, id);
    };
    models.Person.query = function (params) { return api.query(models.Person.entity(), models.Person, params); };

    models.Person.prototype.save = function () {
        if (this.personID && this.personID != '00000000-0000-0000-0000-000000000000') {
            return api.update(models.Person.entity(), this.personID, this);
        } else {
            return api.insert(models.Person.entity(), this);
        }
    };

    models.Person.prototype.destroy = function () { return api.destroy(models.Person.entity(), this.personID); };
    // end Person

    // begin PersonalInfo
    models.PersonalInfo = function (extender) {
        this.firstName = '';
        this.lastName = '';
        this.email = '';

        angular.extend(this, extender);
    };

    models.PersonalInfo.entity = function () { return "PersonalInfo"; };

    models.PersonalInfo.find = function (params) { return api.find(models.PersonalInfo.entity(), models.PersonalInfo, params); };

    models.PersonalInfo.prototype.save = function () {
        return api.update(models.PersonalInfo.entity(), '0', this);
    };
    // end PersonalInfo

    // begin UserSignatureInfo
    models.UserSignatureInfo = function (extender) {
        this.signatureText = '';
        this.initialsText = '';
        this.signatureFont = '';
        this.initialsFont = '';

        angular.extend(this, extender);
    };

    models.UserSignatureInfo.entity = function () { return "UserSignatureInfo"; };

    models.UserSignatureInfo.query = function () { return api.query(models.UserSignatureInfo.entity(), models.UserSignatureInfo, {}, false); };

    models.UserSignatureInfo.prototype.save = function () {
        return api.update(models.UserSignatureInfo.entity(), '0', this);
    };
    // end UserSignatureInfo

    // begin SubscriptionInfo
    models.SubscriptionInfo = function (extender) {
        angular.extend(this, extender);
    };

    models.SubscriptionInfo.entity = function () { return "SubscriptionStatus"; };

    models.SubscriptionInfo.query = function () { return api.query(models.SubscriptionInfo.entity(), models.SubscriptionInfo, {}, false); };
    // end SubscriptionInfo

    // begin TaskList
    models.TaskList = function (extender) {
        this.taskListID = '00000000-0000-0000-0000-000000000000';
        this.dealID = '00000000-0000-0000-0000-000000000000';
        this.displayName = '';
            
        angular.extend(this, extender);
    };

    models.TaskList.entity = function () { return "TaskLists"; };

    models.TaskList.query = function (params) { return api.query(models.TaskList.entity(), models.TaskList, params, !params.id); };

    models.TaskList.prototype.save = function () {
        if (this.taskListID && this.taskListID != '00000000-0000-0000-0000-000000000000') {
            return api.update(models.TaskList.entity(), this.taskListID, this);
        } else {
            return api.insert(models.TaskList.entity(), this);
        }
    };

    models.TaskList.prototype.destroy = function () { return api.destroy(models.TaskList.entity(), this.taskListID); };
    // end TaskList

    // begin Task
    models.Task = function (extender) {
        this.taskID = '00000000-0000-0000-0000-000000000000';
        this.taskListID = '00000000-0000-0000-0000-000000000000';
        this.displayName = 'New Task';
        this.dueAt = new Date();

        angular.extend(this, extender);
    };

    models.Task.entity = function () { return "Tasks"; };

    models.Task.query = function (params) { return api.query(models.Task.entity(), models.Task, params, !params.id); };

    models.Task.prototype.save = function () {
        if (this.taskID && this.taskID != '00000000-0000-0000-0000-000000000000') {
            return api.update(models.Task.entity(), this.taskID, this);
        } else {
            return api.insert(models.Task.entity(), this);
        }
    };

    models.Task.prototype.destroy = function () { return api.destroy(models.Task.entity(), this.taskID); };
    // end Task

    return models;
}]);
'use strict';
app.controller('nefarContractController', ['$scope', '$routeParams', 'flash', function ($scope, $routeParams, flash) {
    $scope.pageNumber = 1;
    $scope.savingSection = false;
    $scope.supplementalFields = {};

    var financingOptions = [
        'topmostSubform[0].Page1[0].cash_transaction[0]',
        'topmostSubform[0].Page1[0].loan_without_financing_contingency[0]',
        'topmostSubform[0].Page1[0].loan_as_marked_below_with_financing_contingency[0]'
    ];

    var loanTypes = [
        'topmostSubform[0].Page2[0].fha[0]',
        'topmostSubform[0].Page2[0].va[0]',
        'topmostSubform[0].Page2[0].conventional_or_usda[0]',
        'topmostSubform[0].Page2[0].other_financing[0]'
    ];

    var fhaField = 'topmostSubform[0].Page2[0].have_the_privilege_and_option_of_proceeding_with_consummation_of_this_Contract_without_regard_to_the[0]';

    var otherFinancingTypes = [
        'topmostSubform[0].Page2[0].mortgage_assumption[0]',
        'topmostSubform[0].Page2[0].seller_financing[0]'
    ];

    var dateFields = [
        'topmostSubform[0].Page1[0].additional_binder_deposit_deadline[0]',
        'topmostSubform[0].Page9[0].DATE_THIS',
        'topmostSubform[0].Page2[0].Agreement_unless_extended_by_other_conditions_of_this_Agreement_Marketable_title_means_title_which[0]'
    ];

    var deadlineTimeField = 'OFFER_WILL_TERMINATE_THE_TIME_FOR_ACCEPTANCE_OF_ANY_COUNTER_OFFER_SHALL_BE';
    var deadlineAM = 'topmostSubform[0].Page9[0].AM';
    var deadlinePM = 'topmostSubfoSrm[0].Page9[0].PM';

    $scope.initSupplementalFields = function () {
        $scope.convertToSelection(financingOptions, 'financingOption');
        $scope.convertToSelection(loanTypes, 'loanType');
        $scope.convertToSelection(otherFinancingTypes, 'otherFinancingType');

        $scope.convertFieldsToDates(dateFields);

        // deal with the date stuff.
        var hour = $scope.getFormField(deadlineTimeField).fieldValue;
        if (hour) {
            hour = parseInt(hour);
            if ($scope.getFormField(deadlinePM).fieldValue == 'On') {
                hour += 12;
            } else if (hour == 12) {
                hour -= 12;
            }
            
            var newVal = new Date();
            newVal.setHours(hour);

            $scope.supplementalFields.acceptanceDeadlineTime = newVal;
        }
    };

    $scope.commitSupplementalFields = function () {
        $scope.convertSelectionToAcroField(financingOptions, $scope.supplementalFields.financingOption);
        $scope.convertSelectionToAcroField(loanTypes, $scope.supplementalFields.loanType);
        $scope.convertSelectionToAcroField(otherFinancingTypes, $scope.supplementalFields.otherFinancingType);

        $scope.convertDatesToFields(dateFields);

        $scope.getFormField(deadlineAM).fieldValue = 'Off';
        $scope.getFormField(deadlinePM).fieldValue = 'Off';

        var deadlineTimeDate = $scope.supplementalFields.acceptanceDeadlineTime;

        if (deadlineTimeDate) {
            var hour = deadlineTimeDate.getHours();
            if (hour > 12) {
                hour -= 12;
                $scope.getFormField(deadlinePM).fieldValue = 'On';
            } else {
                $scope.getFormField(deadlineAM).fieldValue = 'On';
            }

            if (hour == 0) {
                hour = 12;
            }

            $scope.getFormField(deadlineTimeField).fieldValue = hour.toString();
        }

        if ($scope.getFormField('topmostSubform[0].Page2[0].Agreement_unless_extended_by_other_conditions_of_this_Agreement_Marketable_title_means_title_which[0]').fieldValue) {
            $scope.getFormField('topmostSubform[0].Page2[0].including_legal_access_the_transaction_will_be_closed_and_the_deed_and_other_closing_papers_delivered[0]').fieldValue = 'On';
        } else {
            $scope.getFormField('topmostSubform[0].Page2[0].including_legal_access_the_transaction_will_be_closed_and_the_deed_and_other_closing_papers_delivered[0]').fieldValue = 'Off';
        }

        if ($scope.getFormField('topmostSubform[0].Page2[0].days_after_date_of_acceptance_of_this[0]').fieldValue) {
            $scope.getFormField('topmostSubform[0].Page2[0].including_legal_access_the_transaction_will_be_closed_and_the_deed_and_other_closing_papers_delivered[1]').fieldValue = 'On';
        } else {
            $scope.getFormField('topmostSubform[0].Page2[0].including_legal_access_the_transaction_will_be_closed_and_the_deed_and_other_closing_papers_delivered[1]').fieldValue = 'Off';
        }
    };

    $scope.convertDatesToFields = function (fields) {
        for (var i = 0; i < fields.length; i++) {
            var date = $scope.getFormField(fields[i]).fieldValue;
            if (date) {
                $scope.getFormField(fields[i]).fieldValue = new Date(date).toLocaleDateString();
            }
        }
    };

    $scope.convertFieldsToDates = function (fields) {
        for (var i = 0; i < fields.length; i++) {
            if ($scope.getFormField(fields[i]).fieldValue) {
                $scope.getFormField(fields[i]).fieldValue = new Date($scope.getFormField(fields[i]).fieldValue);
            }
        }
    };

    $scope.financingOptionChanged = function () {
        for (var i = 0; i < loanTypes.length; i++) {
            $scope.getFormField(loanTypes[i]).fieldValue = 'Off';
        }

        for (var i = 0; i < otherFinancingTypes.length; i++) {
            $scope.getFormField(otherFinancingTypes[i]).fieldValue = 'Off';
        }
        
        $scope.getFormField(fhaField).fieldValue = '';
        $scope.supplementalFields.loanType = null;
        $scope.supplementalFields.otherFinancingType = null;
    };

    $scope.loanTypeChanged = function () {
        $scope.getFormField(fhaField).fieldValue = '';
        for (var i = 0; i < otherFinancingTypes.length; i++) {
            $scope.getFormField(otherFinancingTypes[i]).fieldValue = 'Off';
        }
        $scope.supplementalFields.otherFinancingType = null;
    };

    $scope.convertSelectionToAcroField = function (acroFields, selectedValue) {
        $.each(acroFields, function (index, acroField) {
            $scope.getFormField(acroField).fieldValue = 'Off';
        });
        if (selectedValue) {
            $scope.getFormField(selectedValue).fieldValue = 'On';
        }
    };

    $scope.convertToSelection = function (acroFields, supplementalField) {
        for (var i = 0; i < acroFields.length; i++) {
            var val = $scope.getFormField(acroFields[i]).fieldValue;
            if (val === "On") {
                $scope.supplementalFields[supplementalField] = acroFields[i];
                break;
            }
        }
    };

    $scope.goNext = function () {
        $scope.goPage($scope.pageNumber + 1);
    };

    $scope.goBack = function () {
        $scope.goPage($scope.pageNumber - 1);
    };

    $scope.goPage = function (page) {
        $scope.savingSection = true;

        $scope.commitSupplementalFields();

        $scope.saveFields(true).then(function () {
            $scope.savingSection = false;
            $scope.go('/deals/' + $routeParams.dealID + '/forms/' + $routeParams.formID + '/' + page);
        });
    };

    $scope.clearBinderDueWithin = function () {
        $scope.getFormField('topmostSubform[0].Page1[0].binder_deposit_due_within[0]').fieldValue = '';
    };

    var init = function () {
        if ($routeParams.pageNumber) {
            $scope.pageNumber = parseInt($routeParams.pageNumber);
        }
        $scope.initSupplementalFields();
    };

    init();
}]);