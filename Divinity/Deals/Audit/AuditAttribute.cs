﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Net.Http;
using System.Web.Http.Filters;
using Divinity.Common;
using Divinity.Common.BaseClasses;
using Divinity.Models;
using Microsoft.AspNet.Identity;
using Microsoft.Azure.Documents.Client;
using Newtonsoft.Json;
using SmartFormat;

namespace Divinity.Deals.Audit {
    public class AuditAttribute : ActionFilterAttribute {
        private string _friendlyMessage { get; set; }
        public AuditAttribute(string friendlyMessage = "User {UserDisplayName} {ActionPastTense} {EntityTypeLowerCase}: {EntityDisplayName}") {
            _friendlyMessage = friendlyMessage;
        }

        public override void OnActionExecuted(HttpActionExecutedContext exContext) {
            var actionContext = exContext.ActionContext;

            if (actionContext.Response != null && actionContext.Response.IsSuccessStatusCode) {
                var info = new AuditInfo { };
                
                var controller = exContext.ActionContext.ControllerContext.Controller as UserController;
                if (controller != null) {
                    info.UserID = controller.User.Identity.GetUserId();
                    info.UserDisplayName = controller.User.Identity.GetUserName();
                }

                if (actionContext.ActionArguments.ContainsKey("id")) {
                    info.EntityID = (Guid)actionContext.ActionArguments["id"];
                }
                else if (actionContext.Response.Headers.Location != null) {
                    Guid temp;

                    if (Guid.TryParse(actionContext.Response.Headers.Location.Segments.Last(), out temp)) {
                        info.EntityID = temp;
                    }
                }
                using (DivinityContext db = new DivinityContext()) {
                    db.Configuration.ProxyCreationEnabled = false;
                    db.Configuration.LazyLoadingEnabled = false;
                    if (info.EntityID.HasValue) {

                        var lookup = db.DisplayNameLookup.SingleOrDefault(lu => lu.ID == info.EntityID);
                        if (lookup != null) {
                            info.EntityType = lookup.EntityType;
                            info.EntityDisplayName = lookup.DisplayName;
                        }
                    }

                    var content = actionContext.Response.Content as ObjectContent;
                    if (content != null && content.Value != null) {
                        var entity = content.Value;

                        if (entity is Deal) {
                            info.DealSnapshot = entity as Deal;
                        }
                        else {
                            var dealIDProp = entity.GetType().GetProperty("DealID");
                            if (dealIDProp != null) {
                                Guid dealID = (Guid)dealIDProp.GetValue(entity);
                                if (dealIDProp != null) {
                                    info.DealSnapshot = db.Deal
                                        .AsNoTracking()
                                        .Include(d => d.TaskLists)
                                        .Include(d => d.TaskLists.Select(tl => tl.Tasks))
                                        .Include(d => d.Properties)
                                        .Include(d => d.Forms)
                                        .Include(d => d.Forms.Select(f => f.Fields))
                                        .Include(d => d.Documents)
                                        .Include(d => d.Documents.Select(doc => doc.Pages))
                                        .Include(d => d.Documents.Select(doc => doc.SignatureFields))
                                        .Include(d => d.Documents.Select(doc => doc.SignatureRequests))
                                        .Include(d => d.Documents.Select(doc => doc.SignatureRequests.Select(r => r.Signatures)))
                                        .Include(d => d.AccessGrants)
                                        .SingleOrDefault(d => d.DealID == dealID);
                                }
                            }
                        }

                        if (info.DealSnapshot != null) {
                            // need to refresh the audit next time they come
                            Caching.Remove(String.Format("GetDealAudit-{0}", info.DealSnapshot.DealID));
                        }
                    }
                }

                switch (actionContext.Request.Method.Method) {
                    case "GET":
                        info.Action = "retrieve";
                        info.ActionPastTense = "retrieved";
                        break;
                    case "POST":
                        info.Action = "add";
                        info.ActionPastTense = "added";
                        break;
                    case "PUT":
                        info.Action = "update";
                        info.ActionPastTense = "updated";
                        break;
                    case "DELETE":
                        info.Action = "delete";
                        info.ActionPastTense = "deleted";
                        break;
                }
                info.FriendlyMessage = Smart.Format(_friendlyMessage, info);

                var docDB = new DocumentClient(new Uri("https://divinity.documents.azure.com:443/"), "YHYQwuG2tiKCfnVgb3WOoIdCm5LFEO0M/PtQbWIwI1zFpKF5h2JclLsMDIZkMhKYA/mBWu7WCdLQX5l9VQiXTA==");
                
                var task = docDB.CreateDocumentAsync("https://divinity.documents.azure.com:443/dbs/77h-AA==/colls/77h-AJN6ogA=", info);
                task.Wait();
            }
        }

        public class AuditInfo {
            public string UserDisplayName { get; set; }
            public string UserID { get; set; }
            public string FriendlyMessage { get; set; }
            public Guid? EntityID { get; set; }
            public string EntityType { get; set; }
            public string EntityDisplayName { get; set; }
            public string Action { get; set; }
            public string ActionPastTense { get; set; }
            public string EntityTypeLowerCase { get { return (this.EntityType ?? "").ToLower(); } }
            public Deal DealSnapshot { get; set; }
            public DateTimeOffset ActionTime { get { return DateTimeOffset.Now; } }
        }
    }
}