﻿using System.Data.Entity;
using Divinity.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security.DataProtection;
using System.Web.Security;
using Divinity.Deals.Controllers.Api;

namespace Divinity.Deals.Auth {
    public class AuthRepository : IDisposable {

        public class MachineKeyProtectionProvider : IDataProtectionProvider {
            public IDataProtector Create(params string[] purposes) {
                return new MachineKeyDataProtector(purposes);
            }
        }

        public class MachineKeyDataProtector : IDataProtector {
            private readonly string[] _purposes;

            public MachineKeyDataProtector(string[] purposes) {
                _purposes = purposes;
            }

            public byte[] Protect(byte[] userData) {
                return MachineKey.Protect(userData, _purposes);
            }

            public byte[] Unprotect(byte[] protectedData) {
                return MachineKey.Unprotect(protectedData, _purposes);
            }
        }

        private AuthContext _authContext;
        private DivinityContext _context;
        private UserManager<IdentityUser> _userManager;

        public AuthRepository() {
            _authContext = new AuthContext();
            _context = new DivinityContext();

            _userManager = new UserManager<IdentityUser>(new UserStore<IdentityUser>(_authContext));
            _userManager.EmailService = new EmailService();
            var provider = new MachineKeyProtectionProvider();
            _userManager.UserTokenProvider = new DataProtectorTokenProvider<IdentityUser>(provider.Create("EmailConfirmation"));
        }

        public async Task<IdentityUser> ShareDealWithRegisteredUser(string sharedByEmail, IdentityUser user, Deal deal) {

            var callback = String.Format("https://deals.divinity.io/#/deals/{0}", deal.DealID);

            if (!user.EmailConfirmed) {
                // they haven't confirmed.. let's just treat this like a new account then.
                Guid newPassword = Guid.NewGuid();
                var resetToken = await _userManager.GeneratePasswordResetTokenAsync(user.Id);
                await _userManager.ResetPasswordAsync(user.Id, resetToken, newPassword.ToString());

                var code = await _userManager.GenerateEmailConfirmationTokenAsync(user.Id);
                callback = String.Format("https://deals.divinity.io/#/login/{0}/{1}/{2}/{3}/{4}", user.Id, user.Email, newPassword, deal.DealID, code);
            }

            await _userManager.SendEmailAsync(
                user.Id,
                String.Format("Message from {0} via Divine Forms", sharedByEmail),
                String.Format(@"{0} has shared a deal with you: {1}. Please click the link below to access the information.<br/><br/>{2}", sharedByEmail, deal.DisplayName, callback));

            return user;
        }

        public async Task<IdentityUser> RegisterUserViaShare(string sharedByEmail, Deal deal, Person person, Guid password) {

            IdentityUser user = new IdentityUser {
                UserName = person.Email,
                Email = person.Email
            };

            var result = await _userManager.CreateAsync(user, password.ToString());
            if (result.Succeeded) {
                _context.UserProfile.Add(new UserProfile {
                    FirstName = "",
                    LastName = "",
                    UserID = user.Id
                });
                _context.UserSubscription.Add(new UserSubscription {
                    FreeDeals = 3,
                    StripeToken = null,
                    UserID = user.Id
                });
                await _context.SaveChangesAsync();

                var code = await _userManager.GenerateEmailConfirmationTokenAsync(user.Id);
                var callback = String.Format("https://deals.divinity.io/#/login/{0}/{1}/{2}/{3}/{4}", user.Id, user.Email, password, deal.DealID, code);
                await _userManager.SendEmailAsync(
                    user.Id,
                    String.Format("Message from {0} via Divine Forms", sharedByEmail),
                    String.Format(@"{0} has shared a deal with you: {1}. Please click the link below to access the information.<br/><br/>{2}", sharedByEmail, deal.DisplayName, callback));
            }

            return user;
        }

        public async Task<IdentityResult> RegisterUser(UserModel userModel) {
            IdentityUser user = new IdentityUser {
                UserName = userModel.UserName,
                Email = userModel.UserName
            };

            var result = await _userManager.CreateAsync(user, userModel.Password);
            if (result.Succeeded) {
                _context.UserProfile.Add(new UserProfile {
                    FirstName = "",
                    LastName = "",
                    UserID = user.Id
                });
                _context.UserSubscription.Add(new UserSubscription {
                    FreeDeals = 3,
                    StripeToken = null,
                    UserID = user.Id
                });
                await _context.SaveChangesAsync();

                var code = await _userManager.GenerateEmailConfirmationTokenAsync(user.Id);
                var callback = "https://deals.divinity.io/#/confirm-account/" + user.Id + "/" + code;
                await _userManager.SendEmailAsync(user.Id, "Confirm your account", "Please confirm your account by clicking this link: <a href=\"" + callback + "\">link</a>");
            }

            return result;
        }

        public async Task<IdentityUser> SendPasswordResetToken(string email) {
            var user = await _userManager.FindByEmailAsync(email);

            if (user != null) {
                var code = await _userManager.GeneratePasswordResetTokenAsync(user.Id);
                var callback = "https://deals.divinity.io/#/password-reset/" + user.Id + "/" + code;
                await _userManager.SendEmailAsync(user.Id, "Account Recovery", "Please follow the link to recover your account: <a href=\"" + callback + "\">link</a>");
            }

            return user;
        }

        public async Task<IdentityResult> ResetPasswordWithToken(string userId, string token, string newPassword) {
            return await _userManager.ResetPasswordAsync(userId, token, newPassword);
        }

        public async Task<object> ConfirmUser(string userId, string token) {
            var user = _userManager.FindById(userId);
            if (user == null) {
                return null;
            }
            return await _userManager.ConfirmEmailAsync(userId, token);
        }

        public async Task<IdentityUser> FindByEmail(string email) {
            IdentityUser user = await _userManager.FindByEmailAsync(email);

            return user;
        }

        public async Task<IdentityUser> FindUser(string userName, string password) {
            //return _userManager.Users.Where(u => u.Email == "adamjlocklear@gmail.com").Single();

            IdentityUser user = await _userManager.FindAsync(userName, password);
            return user;
        }

        public void Dispose() {
            _authContext.Dispose();
            _context.Dispose();
            _userManager.Dispose();

        }
    }
}