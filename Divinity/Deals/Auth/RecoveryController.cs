﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Microsoft.AspNet.Identity.EntityFramework;
using System.ComponentModel.DataAnnotations;

namespace Divinity.Deals.Auth {
    public class RecoveryController : ApiController {
        private AuthRepository _repo = null;

        public RecoveryController() {
            _repo = new AuthRepository();
        }

        // POST api/Account/Register
        [AllowAnonymous]
        [HttpPost]
        public async Task<IHttpActionResult> Send(RecoveryModel model) {
            if (!ModelState.IsValid) {
                return BadRequest(ModelState);
            }
            
            var result = await _repo.SendPasswordResetToken(model.Email);

            if (result == null) {
                return NotFound();
            }
            else { 
                return Ok();
            }
        }

        public class RecoveryModel {
            [Required]
            public string Email { get; set; }
        }

        [AllowAnonymous]
        [HttpPut]
        public async Task<IHttpActionResult> ResetPassword(ResetPasswordModel resetModel) {
            if (!ModelState.IsValid) {
                return BadRequest(ModelState);
            }

            IdentityResult result = await _repo.ResetPasswordWithToken(resetModel.UserId, resetModel.Token, resetModel.NewPassword);
            IHttpActionResult errorResult = GetErrorResult(result);

            if (errorResult != null) {
                return errorResult;
            }

            return Ok();
        }

        public class ResetPasswordModel {
            [Required]
            public string UserId { get; set; }
            
            [Required]
            public string Token { get; set; }
            
            [Required]
            [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
            [DataType(DataType.Password)]
            [Display(Name = "New Password")]
            public string NewPassword { get; set; }
            
            [DataType(DataType.Password)]
            [Display(Name = "Confirm password")]
            [Compare("NewPassword", ErrorMessage = "The password and confirmation password do not match.")]
            public string ConfirmPassword { get; set; }
        }

        protected override void Dispose(bool disposing) {
            if (disposing) {
                _repo.Dispose();
            }

            base.Dispose(disposing);
        }

        private IHttpActionResult GetErrorResult(IdentityResult result) {
            if (result == null) {
                return InternalServerError();
            }

            if (!result.Succeeded) {
                if (result.Errors != null) {
                    foreach (string error in result.Errors) {
                        ModelState.AddModelError("", error);
                    }
                }

                if (ModelState.IsValid) {
                    // No ModelState errors are available to send, so just return an empty BadRequest.
                    return BadRequest();
                }

                return BadRequest(ModelState);
            }

            return null;
        }
    }
}