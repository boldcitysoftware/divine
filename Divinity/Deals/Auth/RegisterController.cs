﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Microsoft.AspNet.Identity.EntityFramework;

namespace Divinity.Deals.Auth {
    public class RegisterController : ApiController {
        private AuthRepository _repo = null;

        public RegisterController() {
            _repo = new AuthRepository();
        }

        // POST api/Account/Register
        [AllowAnonymous]
        [HttpPost]
        public async Task<IHttpActionResult> SignUp(UserModel userModel) {
            if (!ModelState.IsValid) {
                return BadRequest(ModelState);
            }

            IdentityResult result = await _repo.RegisterUser(userModel);

            IHttpActionResult errorResult = GetErrorResult(result);

            if (errorResult != null) {
                return errorResult;
            }

            return Ok();
        }

        [AllowAnonymous]
        [HttpPut]
        public async Task<IHttpActionResult> Confirm(ConfirmModel confirmModel) {
            await _repo.ConfirmUser(confirmModel.UserId, confirmModel.Token);
            return Ok();
        }

        public class ConfirmModel {
            public string UserId { get; set; }
            public string Token { get; set; }
        }

        protected override void Dispose(bool disposing) {
            if (disposing) {
                _repo.Dispose();
            }

            base.Dispose(disposing);
        }

        private IHttpActionResult GetErrorResult(IdentityResult result) {
            if (result == null) {
                return InternalServerError();
            }

            if (!result.Succeeded) {
                if (result.Errors != null) {
                    foreach (string error in result.Errors) {
                        ModelState.AddModelError("", error);
                    }
                }

                if (ModelState.IsValid) {
                    // No ModelState errors are available to send, so just return an empty BadRequest.
                    return BadRequest();
                }

                return BadRequest(ModelState);
            }

            return null;
        }
    }
}