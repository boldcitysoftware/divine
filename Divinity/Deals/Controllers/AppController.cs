﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Divinity.Deals.Controllers {
    
    public class AppController : Controller {
#if !DEBUG
        [RequireHttps]
#endif
        public ActionResult Index() { return View(); }
    }
}
