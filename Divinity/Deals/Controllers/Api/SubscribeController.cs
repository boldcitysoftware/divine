﻿using Divinity.Deals.Audit;
using Divinity.Common.BaseClasses;
using Divinity.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Stripe;

namespace Divinity.Deals.Controllers.Api {
    [Authorize]
    public class SubscribeController : UserController {
        [Audit("User {UserDisplayName} started a subscription.")]
        public HttpResponseMessage Post(Subscription info) {
            var user = db.AspNetUsers.Single(u => u.Id == this.UserID);
            var subscription = db.UserSubscription.Single(u => u.UserID == user.Id);
            var profile = db.UserProfile.Single(u => u.UserID == user.Id);

            var customerService = new StripeCustomerService();
            var subService = new StripeSubscriptionService();

            if (String.IsNullOrWhiteSpace(subscription.StripeToken)) {
                // new customer

                var customer = customerService.Create(new StripeCustomerCreateOptions {
                    Email = user.Email,

                    Description = String.Format("{0} ({1})", info.CardName, user.Email),
                    CardNumber = info.CardNumber,
                    CardExpirationYear = info.CardExpirationYear,
                    CardExpirationMonth = info.CardExpirationMonth,
                    CardAddressCountry = "US",
                    CardAddressLine1 = info.CardAddressLine1,
                    CardAddressLine2 = info.CardAddressLine2,
                    CardAddressCity = info.CardAddressCity,
                    CardAddressState = info.CardAddressState,
                    CardAddressZip = info.CardAddressZip,
                    CardName = info.CardName,
                    CardCvc = info.CardCvc,
                    PlanId = "deals",
                    CouponId = info.CouponCode
                });
                subscription.StripeToken = customer.Id;
                db.SaveChanges();
            }
            else {
                // ooh, there's an existing customer. I might have to cancel an existing subscription and start a new one.
                var customer = customerService.Get(subscription.StripeToken);

                customer = customerService.Update(customer.Id, new StripeCustomerUpdateOptions {
                    CardNumber = info.CardNumber,
                    CardExpirationYear = info.CardExpirationYear,
                    CardExpirationMonth = info.CardExpirationMonth,
                    CardAddressCountry = "US",
                    CardAddressLine1 = info.CardAddressLine1,
                    CardAddressLine2 = info.CardAddressLine2,
                    CardAddressCity = info.CardAddressCity,
                    CardAddressState = info.CardAddressState,
                    CardAddressZip = info.CardAddressZip,
                    CardName = info.CardName,
                    CardCvc = info.CardCvc
                });

                if (customer.StripeSubscriptionList != null && customer.StripeSubscriptionList.TotalCount > 0) {
                    customer.StripeSubscriptionList.StripeSubscriptions.ForEach(sub => {
                        subService.Cancel(customer.Id, sub.Id);
                    });
                }

                StripeSubscriptionCreateOptions options = null;
                if (!String.IsNullOrWhiteSpace(info.CouponCode)) {
                    options = new StripeSubscriptionCreateOptions { CouponId = info.CouponCode };
                }

                var stripeSubscription = subService.Create(customer.Id, "deals", options);
            }
            return Request.CreateResponse<string>(HttpStatusCode.Accepted, info.CouponCode);
        }

        public class Subscription {
            public string CardName { get; set; }
            public string CardNumber { get; set; }
            public string CardCvc { get; set; }
            public string CardExpirationYear { get; set; }
            public string CardExpirationMonth { get; set; }
            public string CardAddressLine1 { get; set; }
            public string CardAddressLine2 { get; set; }
            public string CardAddressCity { get; set; }
            public string CardAddressState { get; set; }
            public string CardAddressZip { get; set; }
            public string CouponCode { get; set; }
        }
    }
}