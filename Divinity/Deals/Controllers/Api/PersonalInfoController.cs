﻿using Divinity.Common.BaseClasses;
using Divinity.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Divinity.Deals.Controllers.Api {
    [Authorize]
    public class PersonalInfoController : UserController {
        public PersonalInfo Get() {
            var user = db.AspNetUsers.Single(u => u.Id == this.UserID);

            return db.UserProfile
                .Where(u => u.UserID == user.Id)
                .Select(u => new PersonalInfo { 
                    FirstName = u.FirstName,
                    LastName = u.LastName,
                    Email = user.Email
                }).FirstOrDefault();

        }

        public PersonalInfo Get(string id) {
            var user = db.AspNetUsers.Single(u => u.Id == id);

            return db.UserProfile
                .Where(u => u.UserID == user.Id)
                .Select(u => new PersonalInfo {
                    FirstName = u.FirstName,
                    LastName = u.LastName,
                    Email = user.Email
                }).FirstOrDefault();
        }

        public HttpResponseMessage Put(string id, PersonalInfo personalInfo) {
            if (!ModelState.IsValid) {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }

            var user = db.AspNetUsers.Single(u => u.Id == this.UserID);
            var profile = db.UserProfile.Single(u => u.UserID == user.Id);

            user.Email = personalInfo.Email;
            profile.FirstName = personalInfo.FirstName;
            profile.LastName = personalInfo.LastName;
            
            db.SaveChanges();
            

            return Request.CreateResponse(HttpStatusCode.OK);
        }

        public class PersonalInfo {
            public string FirstName { get; set; }
            public string LastName { get; set; }

            [Required]
            public string Email { get; set; }
        }
    }
}