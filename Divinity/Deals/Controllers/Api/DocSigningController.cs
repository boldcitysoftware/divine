﻿using Divinity.Common.BaseClasses;
using Divinity.Models;
using Divinity.Models.Helpers;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Divinity.TemplateEngine;
using System.Threading.Tasks;
using Divinity.Common.Notifications;

namespace Divinity.Deals.Controllers.Api {
    
    public class DocSigningController : UserController {

        [HttpGet]
        public async Task<VerificationData> Verify(Guid id) {
            var document = await db.Document.SingleOrDefaultAsync(d => d.DocumentID == id);

            if (document == null) {
                throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.NotFound));
            }

            var signatures = await db.Signature.Include(s => s.SignatureRequest).Where(s => s.SignatureRequest.DocumentID == id).ToListAsync();

            var lastUpdate = document.UpdatedAt;

            if (signatures.Any(s => s.ExecutedAt > document.UpdatedAt)) {
                lastUpdate = signatures.Max(s => s.ExecutedAt);
            }

            return new VerificationData { 
                DocumentID = id,
                DisplayName = document.DisplayName,
                LastActionTime = lastUpdate.UtcDateTime
            };
        }

        public class VerificationData {
            public Guid DocumentID { get; set; }
            public string DisplayName { get; set; }
            public DateTime LastActionTime { get; set; }

            public string DownloadUrl { get { return String.Format("/api/DownloadDocument/{0}", DocumentID); } }
            
        }

        [HttpPut]
        public async Task<HttpResponseMessage> ConsentGiven(Guid id) {
            SignatureRequest request = await db.SignatureRequest.SingleOrDefaultAsync(r => r.RequestID == id);

            if (request == null) {
                throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.NotFound));
            }

            request.ConsentGivenAt = request.ConsentGivenAt ?? DateTimeOffset.UtcNow;
            await db.SaveChangesAsync();

            return new HttpResponseMessage(HttpStatusCode.OK);
        }

        [HttpPut]
        public async Task<HttpResponseMessage> SignatureAdopted(Guid id) {
            SignatureRequest request = await db.SignatureRequest.SingleOrDefaultAsync(r => r.RequestID == id);

            if (request == null) {
                throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.NotFound));
            }

            request.SignatureAdoptedAt = request.SignatureAdoptedAt ?? DateTimeOffset.UtcNow;
            await db.SaveChangesAsync();

            return new HttpResponseMessage(HttpStatusCode.OK);
        }
    }
}
