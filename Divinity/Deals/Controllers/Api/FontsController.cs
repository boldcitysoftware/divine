﻿using Divinity.Common.BaseClasses;
using Divinity.Models;
using Divinity.Models.Helpers;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Divinity.Deals.Controllers.Api {
    public class FontsController : UserController {
        // GET api/Fonts
        public IEnumerable<Font> GetFonts() {
            return db.Font.AsEnumerable();
        }

        // GET api/Fonts/5
        public Font GetFont(string id) {
            var font = db.Font.Find(id);
            if (font == null) {
                throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.NotFound));
            }
            return font;
        }
    }
}
