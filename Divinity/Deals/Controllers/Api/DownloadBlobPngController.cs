﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace Divinity.Tools.Controllers.Api {
    
    public class DownloadBlobPngController : ApiController {
        // GET api/downloadtemplate/5
        public async Task<HttpResponseMessage> Get(string id, [FromUri]string container) {
            byte[] png = await new Storage.AzureBlobStorage().GetBytesAsync(container, id);
            if (png == null) { 
                throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.NotFound));
            }
            HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.OK);
            response.Content = new ByteArrayContent(png);
            response.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("image/png");
            return response;
        }
    }
}