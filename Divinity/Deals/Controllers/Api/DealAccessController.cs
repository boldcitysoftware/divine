﻿using System.Threading.Tasks;
using Divinity.Deals.Audit;
using Divinity.Common.BaseClasses;
using Divinity.Models;
using Divinity.Models.Helpers;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Filters;
using System.ComponentModel.DataAnnotations;
using Divinity.Deals.Auth;

namespace Divinity.Deals.Controllers.Api {
    [Authorize]
    public class DealAccessController : UserController {
        
        private AuthRepository _auth = new AuthRepository();

        public async Task<IEnumerable<DealAccess>> GetDealAccess(Guid dealID) {
            return await db.DealAccess.Where(d => d.DealID == dealID).ToListAsync();
        }

        public async Task<DealAccess> GetDealAccess(Guid dealID, Guid personID) {
            return await db.DealAccess.Where(d => d.DealID == dealID && d.PersonID == personID).SingleOrDefaultAsync();
        }

        // POST api/DealShare
        [Audit("User {UserDisplayName} shared the deal with: {EntityDisplayName}")]
        public async Task<HttpResponseMessage> PostDealAccess(DealAccess info) {
            if (ModelState.IsValid) {
                var deal = db.Deal.Find(info.DealID);
                var person = db.Person.Find(info.PersonID);

                if (deal == null || person == null) {
                    return Request.CreateResponse(HttpStatusCode.NotFound);
                }

                if (deal.UserID != this.UserID) {
                    throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.Forbidden));
                }

                
                var shareWithUser = await _auth.FindByEmail(person.Email);
                if (shareWithUser == null) {
                    Guid accessToken = SequentialGuidCreator.Create();
                    shareWithUser = await _auth.RegisterUserViaShare(this.UserEmail, deal, person, accessToken);
                }
                else {
                    shareWithUser = await _auth.ShareDealWithRegisteredUser(this.UserEmail, shareWithUser, deal);
                }

                if (info.AccessID == Guid.Empty) {
                    info.AccessID = SequentialGuidCreator.Create();
                    db.DealAccess.Add(info);
                }

                info.AccessGrantedAt = DateTimeOffset.Now;
                info.AccessGrantedBy = this.UserID;
                info.UserID = shareWithUser.Id;
                info.Revoked = false;

                db.SaveChanges();

                var response = Request.CreateResponse<DealAccess>(HttpStatusCode.Created, info);
                response.Headers.Location = new Uri(Url.Link("DefaultApi", new { id = info.AccessID }));
                return response;
            }
            else {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
        }

        // DELETE api/Deals/5
        [Audit("User {UserDisplayName} revoked deal access for: {EntityDisplayName}")]
        public HttpResponseMessage DeleteDealAccess(Guid id) {
            DealAccess da = db.DealAccess.SingleOrDefault(d=>d.AccessID == id && !d.Revoked);

            if (da == null) {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }

            if (da.AccessGrantedBy != this.UserID) {
                throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.Forbidden));
            }

            da.Revoked = true;

            try {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException ex) {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, ex);
            }

            return Request.CreateResponse(HttpStatusCode.OK, da);
        }

        protected override void Dispose(bool disposing) {
            db.Dispose();
            _auth.Dispose();
            base.Dispose(disposing);
        }
    }
}