﻿using Divinity.Common.BaseClasses;
using Divinity.Models;
using Divinity.Models.Helpers;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Divinity.TemplateEngine;
using System.Threading.Tasks;

namespace Divinity.Deals.Controllers.Api {
    
    public class DownloadDocumentController : UserController {

        // GET api/downloadtemplate/5
        public async Task<HttpResponseMessage> Get(Guid id, [FromUri]bool preview = false) {

            var doc = await db.Document.SingleOrDefaultAsync(f => f.DocumentID == id && !f.DeletedAt.HasValue);

            if (doc == null){
                NotFound();
            }

            byte[] pdf = await new Storage.AzureBlobStorage().GetBytesAsync("signed-document", id.ToString());
            if (pdf == null) {
                pdf = await PdfCreator.CreateSignedSnapshotAsync(id, db);
            }
            
            HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.OK);
            response.Content = new ByteArrayContent(pdf);
            response.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue(doc.ContentType);
            
            if (!preview) {
                response.Content.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment");
                response.Content.Headers.ContentDisposition.FileName = doc.FileName;
            }

            return response;
        }

    }
}
