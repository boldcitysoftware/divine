﻿using Divinity.Common.BaseClasses;
using Divinity.Models;
using Divinity.Models.Helpers;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Divinity.TemplateEngine;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using System.Threading.Tasks;

namespace Divinity.Deals.Controllers.Api {
    [Authorize]
    public class DocumentsController : UserController {
        // GET api/Documents/5
        public async Task<Document> GetDocument(Guid id) {
            var doc = await db.Document
                .Include(d => d.Pages)
                .Include(d => d.SignatureFields)
                .Include(d => d.SignatureRequests)
                .Include(d => d.SignatureRequests.Select(r => r.Signatures))
                .Include(d => d.SignatureRequests.Select(r => r.Signer))
                .SingleOrDefaultAsync(f => f.DocumentID == id && !f.DeletedAt.HasValue);
            if (doc == null) {
                throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.NotFound));
            }
            return doc;
        }

        // PUT api/Documents/5
        public HttpResponseMessage PutDocument(Guid id, Document doc) {
            if (!ModelState.IsValid) {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }

            if (id != doc.DocumentID) {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }

            var dbDocument = db.Document
                .Include(t => t.SignatureFields)
                .SingleOrDefault(t => t.DocumentID == id && !t.DeletedAt.HasValue);

            if (dbDocument == null) {
                throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.NotFound));
            }

            db.Entry(dbDocument).CurrentValues.SetValues(doc);
            List<SignatureInfo> toRemove = new List<SignatureInfo>();
            foreach (var dbSignature in dbDocument.SignatureFields) {
                var sigField = doc.SignatureFields.SingleOrDefault(sig => sig.SignatureInfoID == dbSignature.SignatureInfoID);
                if (sigField != null) {
                    db.Entry(dbSignature).CurrentValues.SetValues(sigField);
                }
                else {
                    toRemove.Add(dbSignature);
                }
            }

            db.SignatureInfo.RemoveRange(toRemove);

            foreach (var sigField in doc.SignatureFields) {
                if (sigField.SignatureInfoID == Guid.Empty) {
                    sigField.SignatureInfoID = SequentialGuidCreator.Create();
                    dbDocument.SignatureFields.Add(sigField);
                }
            }

            dbDocument.UpdatedAt = DateTimeOffset.Now;

            try {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException ex) {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, ex);
            }

            return Request.CreateResponse(HttpStatusCode.OK);
        }

        // DELETE api/Documents/5
        [Audit.Audit]
        public async Task<HttpResponseMessage> DeleteDocument(Guid id) {

            Document doc = await db.Document.SingleOrDefaultAsync(t => t.DocumentID == id && !t.DeletedAt.HasValue);
            
            if (doc == null) {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }

            doc.DeletedAt = DateTimeOffset.Now;

            try {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException ex) {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, ex);
            }

            return Request.CreateResponse(HttpStatusCode.OK, doc);
        }
    }
}
