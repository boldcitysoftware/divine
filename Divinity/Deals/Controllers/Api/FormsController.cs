﻿using Divinity.Common.BaseClasses;
using Divinity.Models;
using Divinity.Models.Helpers;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Divinity.TemplateEngine;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using System.Threading.Tasks;

namespace Divinity.Deals.Controllers.Api {
    [Authorize]
    public class FormsController : UserController {
        
        // GET api/Forms
        public async Task<IEnumerable<Form>> GetForms(Guid dealID) {
            return await db.Form
                .Where(d => d.DealID == dealID)
                .Include(f => f.FormTemplate)
                .Include(f => f.FormTemplate.AcroFields)
                .Include(f => f.Fields)
                .ToListAsync();
        }

        // GET api/Forms/5
        public Form GetForm(Guid id) {
            var Form = db.Form
                .Include(f => f.FormTemplate)
                .Include(f => f.FormTemplate.AcroFields)
                .Include(f => f.Fields)
                .SingleOrDefault(f => f.FormID == id);
            if (Form == null) {
                throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.NotFound));
            }
            return Form;
        }

        // PUT api/Forms/5
        [Audit.Audit]
        public async Task<HttpResponseMessage> PutForm([FromUri]Guid id, [FromBody]Form form, [FromUri]bool finish = false) {
            if (!ModelState.IsValid) {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }

            if (id != form.FormID) {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }

            var dbForm = await db.Form
                .Include(f => f.FormTemplate.Document.Pages)
                .Include(f => f.FormTemplate.Document.SignatureFields)
                .Include(f => f.FormTemplate.Document.SignatureRequests.Select(r => r.Signatures))
                .Include(f => f.FormTemplate.Document.SignatureRequests.Select(r => r.Signer))
                .Include(f => f.Fields)
                .Include(f => f.Document.SignatureFields)
                .Include(f => f.Document.Pages)
                .Include(f => f.Document.SignatureRequests.Select(r => r.Signatures))
                .Include(f => f.Document.SignatureRequests.Select(r => r.Signer))
                .SingleOrDefaultAsync(f => f.FormID == id);

            if (dbForm == null) {
                throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.NotFound));
            }

            if (dbForm.DocumentID.HasValue) {
                // document's already been created, can't edit this anymore
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }

            foreach (var fieldToSave in form.Fields) {
                var dbField = dbForm.Fields.SingleOrDefault(ff => ff.FormFieldID == fieldToSave.FormFieldID);
                fieldToSave.UpdatedAt = DateTimeOffset.Now;
                if (dbField != null) {
                    dbField.FieldValue = fieldToSave.FieldValue;
                }
                else {
                    fieldToSave.FormFieldID = SequentialGuidCreator.Create();
                    fieldToSave.CreatedAt = DateTimeOffset.Now;
                    dbForm.Fields.Add(fieldToSave);
                }
            }

            db.Entry(dbForm).CurrentValues.SetValues(form);
            dbForm.UpdatedAt = DateTimeOffset.Now;

            if (finish) {
                // 1. instantiate the doc
                var doc = new Document { 
                    ContentType = "application/pdf",
                    CreatedAt = DateTimeOffset.Now,
                    UpdatedAt = DateTimeOffset.Now,
                    DealID = dbForm.DealID,
                    DisplayName = dbForm.DisplayName,
                    DocumentID = SequentialGuidCreator.Create(),
                    FileName = dbForm.FormTemplate.Document.FileName
                };
                
                //2. clone signatures and pages
                doc.SignatureFields = new List<SignatureInfo>();
                var templateSignatureFields = dbForm.FormTemplate.Document.SignatureFields.ToList();

                foreach (var signatureField in templateSignatureFields) {
                    var newField = new SignatureInfo();
                    db.Entry(newField).State = EntityState.Added;
                    db.Entry(newField).CurrentValues.SetValues(signatureField);

                    newField.SignatureInfoID = SequentialGuidCreator.Create();
                    newField.DocumentID = doc.DocumentID;
                    doc.SignatureFields.Add(newField);
                }

                doc.Pages = new List<Page>();
                var templatePages = dbForm.FormTemplate.Document.Pages.ToList();
                foreach (var page in templatePages) {
                    var newPage = new Page();
                    db.Entry(newPage).State = EntityState.Added;
                    db.Entry(newPage).CurrentValues.SetValues(page);
                    newPage.PageID = SequentialGuidCreator.Create();
                    newPage.DocumentID = doc.DocumentID;
                    doc.Pages.Add(newPage);
                }

                // 4. associate
                dbForm.Document = doc;
                dbForm.DocumentID = doc.DocumentID;

                // 5. fill the PDF and save the initial copy/pages
                await PdfCreator.CreateSignedSnapshotAsync(dbForm, db);
            }

            try {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException ex) {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, ex);
            }

            return Request.CreateResponse<Form>(HttpStatusCode.OK, dbForm);
        }

        // POST api/Forms
        [Audit.Audit]
        public async Task<HttpResponseMessage> PostForm(Form form) {
            var creator = new FormCreator(form.FormTemplateID.Value, form.DealID.Value, db);
            var formToInsert = await creator.Create(false);
            
            db.Form.Add(formToInsert);
            db.SaveChanges();

            byte[] filledFormBytes = await PdfCreator.CreateFormPdfAsync(formToInsert, db);

            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.Created, formToInsert);

            response.Headers.Location = new Uri(Url.Link("DefaultApi", new { id = formToInsert.FormID }));
            return response;
            
        }

        // DELETE api/Forms/5
        [Audit.Audit]
        public HttpResponseMessage DeleteForm(Guid id) {
            Form form = db.Form.Find(id);
            if (form == null) {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }

            db.Database.ExecuteSqlCommand("EXEC DeleteForm @p0", id);

            try {
                db.SaveChanges();
            } catch (DbUpdateConcurrencyException ex) {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, ex);
            }

            return Request.CreateResponse(HttpStatusCode.OK, form);
        }
    }
}
