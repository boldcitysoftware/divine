﻿using Divinity.Common.BaseClasses;
using Divinity.Models;
using Divinity.Models.Helpers;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Divinity.Deals.Controllers.Api {
    [Authorize]
    public class PropertiesController : UserController {
        // GET api/Properties
        public IEnumerable<Property> GetProperties() {
            return db.Property
                .AsEnumerable();
        }

        // GET api/Properties/5
        public Property GetProperty(Guid id) {
            var property = db.Property.Find(id);
            if (property == null) {
                throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.NotFound));
            }
            return property;
        }

        // PUT api/Properties/5
        [Audit.Audit]
        public HttpResponseMessage PutProperty(Guid id, Property property) {
            if (!ModelState.IsValid) {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }

            var dbProperty = db.Property.Find(id);

            if (id != property.PropertyID) {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }

            db.Entry(dbProperty).CurrentValues.SetValues(property);
            dbProperty.UpdatedAt = DateTimeOffset.Now;

            try {
                db.SaveChanges();
            } catch (DbUpdateConcurrencyException ex) {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, ex);
            }

            return Request.CreateResponse<Property>(HttpStatusCode.OK, dbProperty);
        }

        // POST api/Properties
        [Audit.Audit]
        public HttpResponseMessage PostProperty(Property property) {
            if (ModelState.IsValid) {
                property.PropertyID = SequentialGuidCreator.Create();
                property.CreatedAt = property.UpdatedAt = DateTimeOffset.Now;

                db.Property.Add(property);
                db.SaveChanges();

                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.Created, property);
                response.Headers.Location = new Uri(Url.Link("DefaultApi", new { id = property.PropertyID }));
                return response;
            } else {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
        }

        // DELETE api/Properties/5
        [Audit.Audit]
        public HttpResponseMessage DeleteProperty(Guid id) {
            Property property = db.Property.Find(id);
            if (property == null) {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }

            db.Property.Remove(property);

            try {
                db.SaveChanges();
            } catch (DbUpdateConcurrencyException ex) {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, ex);
            }

            return Request.CreateResponse(HttpStatusCode.OK, property);
        }
    }
}
