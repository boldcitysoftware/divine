﻿using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using Divinity.Common.BaseClasses;
using Divinity.TemplateEngine;
using System.Security.Principal;
using System.Security.Claims;

namespace Divinity.Tools.Controllers.Api {

    public class IdentityController : UserController {
        // GET api/downloadtemplate/5
        public string Get() {
            return ClaimsPrincipal.Current.Identity.Name;
        }
    }
}