﻿using Divinity.Models;
using Divinity.Models.Helpers;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Runtime.Serialization;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Divinity.PdfToPng;
using Divinity.TemplateEngine;

namespace Divinity.Tools.Controllers.Api
{
    [DataContract]
    public class UploadFileDesc {
        [DataMember]
        public string name { get; set; }
        [DataMember]
        public long size { get; set; }
        [DataMember]
        public Guid? documentID { get; set; }
        public UploadFileDesc(string n, long s, Guid? t) {
            name = n;
            size = s;
            documentID = t;
        }
    }

    public class UploadController : ApiController {
        private readonly string BASEFOLDERNAME = "templates";
        private class FlowMeta {
            public string flowChunkNumber { get; set; }
            public string flowChunkSize { get; set; }
            public string flowCurrentChunkSize { get; set; }
            public string flowTotalSize { get; set; }
            public string flowIdentifier { get; set; }
            public string flowFilename { get; set; }
            public string flowRelativePath { get; set; }
            public string flowTotalChunks { get; set; }
            public byte[] ChunkData { get; set; }

            public string ChunkID {
                get {
                    return (flowTotalChunks == "1") ? flowIdentifier : String.Format("{0}_{1}", flowIdentifier, flowChunkNumber);
                }
            }

            public FlowMeta(Dictionary<string, string> values) {
                flowChunkNumber = values["flowChunkNumber"];
                flowChunkSize = values["flowChunkSize"];
                flowCurrentChunkSize = values["flowCurrentChunkSize"];
                flowTotalSize = values["flowTotalSize"];
                flowIdentifier = values["flowIdentifier"];
                flowFilename = values["flowFilename"];
                flowRelativePath = values["flowRelativePath"];
                flowTotalChunks = values["flowTotalChunks"];
            }

            public FlowMeta(NameValueCollection values) {
                flowChunkNumber = values["flowChunkNumber"];
                flowChunkSize = values["flowChunkSize"];
                flowCurrentChunkSize = values["flowCurrentChunkSize"];
                flowTotalSize = values["flowTotalSize"];
                flowIdentifier = values["flowIdentifier"];
                flowFilename = values["flowFilename"];
                flowRelativePath = values["flowRelativePath"];
                flowTotalChunks = values["flowTotalChunks"];
            }
        }

        public HttpResponseMessage Get([FromUri]string attachmentType = "") {
            var meta = new FlowMeta(Request.GetQueryNameValuePairs().ToDictionary(x => x.Key, x => x.Value));

            List<FlowMeta> flows = (List<FlowMeta>)System.Web.HttpContext.Current.Cache[meta.flowIdentifier];

            if (flows != null && flows.Any(f=>f.flowIdentifier == meta.flowIdentifier && f.flowChunkNumber == meta.flowChunkNumber)) {
                return Request.CreateResponse(HttpStatusCode.OK);
            } else {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }
        }

        public async Task<IEnumerable<UploadFileDesc>> Post([FromUri]Guid dealID) {
            var meta = new FlowMeta(HttpContext.Current.Request.Form);
            
            if (!Request.Content.IsMimeMultipartContent()) {
                throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);
            }

            var streamProvider = new MultipartMemoryStreamProvider();
            
            await Request.Content.ReadAsMultipartAsync(streamProvider);

            using (Stream fileChunkStream = await streamProvider.Contents[8].ReadAsStreamAsync()) {
                if (fileChunkStream == null) {
                    throw new HttpResponseException(HttpStatusCode.NotFound);
                }

                meta.ChunkData = new byte[fileChunkStream.Length];
                fileChunkStream.Read(meta.ChunkData, 0, (int)fileChunkStream.Length);
            }

            List<FlowMeta> flows = (List<FlowMeta>)System.Web.HttpContext.Current.Cache[meta.flowIdentifier];

            if (flows == null) {
                flows = new List<FlowMeta>();
                HttpContext.Current.Cache.Add(meta.flowIdentifier, flows, null, System.Web.Caching.Cache.NoAbsoluteExpiration, new TimeSpan(1, 0, 0), System.Web.Caching.CacheItemPriority.Default, null);
            }

            flows.Add(meta);

            if (flows.Count == int.Parse(meta.flowTotalChunks)) {
                // all chunks are up!
                byte[] finalBytes = new byte[flows.Sum(f => f.ChunkData.LongLength)];
                long position = 0;
                
                foreach (byte[] bytes in flows.OrderBy(m => int.Parse(m.flowChunkNumber)).Select(m=>m.ChunkData)) {
                    Array.Copy(bytes, 0, finalBytes, position, bytes.LongLength);
                    position += bytes.LongLength;
                }

            
                // final or only chunk: need to save the doc
                DivinityContext db = new DivinityContext();
                var doc = new Divinity.Models.Document {
                    CreatedAt = DateTimeOffset.Now,
                    UpdatedAt = DateTimeOffset.Now,
                    DealID = dealID,
                    DocumentID = SequentialGuidCreator.Create(),
                    FileName = meta.flowFilename,
                    DisplayName = meta.flowFilename,
                    ContentType = MimeMapping.GetMimeMapping(meta.flowFilename)
                };
                db.Document.Add(doc);
                try {
                    await db.SaveChangesAsync();
                }
                catch (Exception ex) {
                    ex.ToString();
                }

                if (doc.ContentType == "application/pdf") {
                    finalBytes = Utils.RemoveAllFieldsFromPDF(finalBytes);
                    // store pages, take snapshots, might need them later
                    int pageNum = 0;
                    foreach (Converter.PageImageInfo image in Converter.Convert(finalBytes)) {
                        pageNum++;
                        var page = new Page {
                            DocumentID = doc.DocumentID,
                            PageID = SequentialGuidCreator.Create(),
                            PageNumber = pageNum,
                            PageWidth = image.Width,
                            PageHeight = image.Height
                        };
                        db.Page.Add(page);
                        await new Storage.AzureBlobStorage().StoreBytesAsync("page-image", page.PageID.ToString(), image.PngBytes);
                    }
                    await db.SaveChangesAsync();
                }

                // store the file up on azure
                await new Storage.AzureBlobStorage().StoreBytesAsync("document", doc.DocumentID.ToString(), finalBytes);

                var fileInfo = new UploadFileDesc[]{
                    new UploadFileDesc(meta.flowFilename, finalBytes.LongLength / 1024, doc.DocumentID)
                };
                return fileInfo;
            }
            else {
                // multiple files, this isn't the last one(s)...
                return new UploadFileDesc[]{
                    new UploadFileDesc(meta.flowFilename, meta.ChunkData.LongLength / 1024, null)
                };
            }
        }
    }
}
