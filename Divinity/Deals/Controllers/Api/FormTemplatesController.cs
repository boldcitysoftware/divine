﻿using Divinity.Common.BaseClasses;
using Divinity.Models;
using Divinity.Models.Helpers;
using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Divinity.Deals.Controllers.Api {
    [Authorize]
    public class FormTemplatesController : UserController {
        // GET api/FormTemplates
        public IEnumerable<FormTemplate> GetFormTemplates(string jurisdiction) {
            return db.FormTemplate
                .Include(t => t.FieldMappings)
                .Include(t => t.AcroFields)
                .Include(t => t.AcroFields.Select(a => a.SelectionOptions))
                // only want publically available templates
                .Where(t => t.Public)
                // this will grab a direct match or a parent jurisdiction.
                // for instance if 'US-FL-NEFAR' is passed in, we'll also include 'US-FL' and 'US'
                .Where(t => t.Jurisdiction == jurisdiction || jurisdiction.StartsWith(t.Jurisdiction))
                .AsEnumerable();
        }

        // GET api/FormTemplates/5
        public FormTemplate GetFormTemplate(Guid id) {
            var template = db.FormTemplate
                .Include(t => t.FieldMappings)
                .Include(t => t.AcroFields)
                .Include(t => t.AcroFields.Select(a => a.SelectionOptions))
                .SingleOrDefault(t => t.FormTemplateID == id);
            if (template == null) {
                throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.NotFound));
            }
            return template;
        }
    }
}