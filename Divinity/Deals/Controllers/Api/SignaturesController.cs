﻿using System;
using System.Data.Entity;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using Divinity.Common.BaseClasses;
using Divinity.Common.Notifications;
using Divinity.Models;
using Divinity.Models.Helpers;
using Divinity.TemplateEngine;

namespace Divinity.Deals.Controllers.Api {
    
    public class SignaturesController : UserController {
        // POST api/Forms
        [Audit.Audit("Signature executed on document: {EntityType} by {EntityDisplayName}")]
        public async Task<HttpResponseMessage> PostSignatures(Signature[] signatures) {
            if (signatures.Length == 0){
                throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.NotFound));
            }
            var requestID = signatures[0].RequestID;
            var request = await db.SignatureRequest.Include(r => r.Signer).SingleOrDefaultAsync(r => r.RequestID == requestID);
            if (request == null || request.AllSignaturesConfirmedAt.HasValue) {
                throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.NotFound));
            }
            request.AllSignaturesConfirmedAt = DateTimeOffset.Now;
            
            foreach (var signature in signatures) {
                signature.SignatureID = SequentialGuidCreator.Create();
                db.Signature.Add(signature);
            }

            await db.SaveChangesAsync();
            
            // need to build a new snapshot of the document
            await PdfCreator.CreateSignedSnapshotAsync(request.DocumentID, db);

            // send the requester a notification
            var confirmationEmail = new SignatureConfirmation(request, db);
            await confirmationEmail.SendNotificationAsync();

            var signatureForBitmap = signatures.FirstOrDefault(s => s.SignatureText.Length > 6);
            if (signatureForBitmap == null) {
                signatureForBitmap = signatures.Last();
            }
            Bitmap signatureImage = TemplateEngine.PdfCreator.CreateSignatureBitmap(signatureForBitmap.SignatureText, 24, await db.Font.FindAsync(signatureForBitmap.FontName));
            using (var ms = new MemoryStream()) {
                signatureImage.Save(ms, ImageFormat.Png);
                await new Storage.AzureBlobStorage().StoreBytesAsync("signatures", request.RequestID.ToString(), ms.ToArray());
            }
            
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.Created, request);

            response.Headers.Location = new Uri(Url.Link("DefaultApi", new { id = request.RequestID }));
            return response;
        }
    }
}
