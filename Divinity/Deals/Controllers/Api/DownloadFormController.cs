﻿using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using Divinity.Common.BaseClasses;
using Divinity.TemplateEngine;

namespace Divinity.Tools.Controllers.Api {

    public class DownloadFormController : UserController {
        // GET api/downloadtemplate/5
        public async Task<HttpResponseMessage> Get(Guid id) {
            var dbForm = await db.Form.Include(f => f.Fields).SingleOrDefaultAsync(f => f.FormID == id);
            if (dbForm == null) {
                throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.NotFound));
            }
            byte[] pdf = await PdfCreator.CreateFormPdfAsync(dbForm, db, false);

            if (pdf == null) {
                throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.BadRequest));
            }

            HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.OK);
            response.Content = new ByteArrayContent(pdf);
            response.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/octet-stream");
            response.Content.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment");
            response.Content.Headers.ContentDisposition.FileName = String.Format("{0}.pdf", id);
            return response;
        }
    }
}