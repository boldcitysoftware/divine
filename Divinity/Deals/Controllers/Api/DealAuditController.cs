﻿using Divinity.Deals.Audit;
using Divinity.Common.BaseClasses;
using Divinity.Models;
using Divinity.Models.Helpers;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Filters;
using Microsoft.Azure.Documents.Client;
using Microsoft.Azure.Documents;
using Microsoft.Azure.Documents.Linq;
using Newtonsoft.Json.Linq;
using Divinity.Common;
using System.Threading.Tasks;

namespace Divinity.Deals.Controllers.Api {
    [Authorize]
    public class DealAuditController : UserController {
        public DealAuditController() {

        }

        // GET api/Deals/5
        public async Task<IEnumerable<DealAudit>> GetDealAudit(Guid id) {
            string cacheKey = String.Format("GetDealAudit-{0}", id);
            return Caching.Get<Guid, IEnumerable<DealAudit>>(cacheKey, id, GetAuditAsync);
        }

        Func<Guid, IEnumerable<DealAudit>> GetAuditAsync = (id) => {
            var docDB = new DocumentClient(new Uri("https://divinity.documents.azure.com:443/"), "YHYQwuG2tiKCfnVgb3WOoIdCm5LFEO0M/PtQbWIwI1zFpKF5h2JclLsMDIZkMhKYA/mBWu7WCdLQX5l9VQiXTA==");
            string query = String.Format("SELECT c.DealSnapshot, c.ActionTime, c.FriendlyMessage FROM c WHERE c.DealSnapshot.DealID = '{0}'", id);
            var results = docDB.CreateDocumentQuery<DealAudit>("https://divinity.documents.azure.com:443/dbs/77h-AA==/colls/77h-AJN6ogA=", query).ToList().OrderBy(r => r.ActionTime);
            return results;
        };

        public class DealAudit {
            public DateTimeOffset? ActionTime { get; set; }
            public string FriendlyMessage { get; set; }
            public Deal DealSnapshot { get; set; }
        }
    }
}