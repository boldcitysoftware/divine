﻿using Divinity.Common.BaseClasses;
using Divinity.Models;
using Divinity.Models.Helpers;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Divinity.TemplateEngine;
using System.Threading.Tasks;
using Divinity.Common.Notifications;

namespace Divinity.Deals.Controllers.Api {
    
    public class SignatureRequestsController : UserController {
        
        // GET api/Forms
        [Authorize]
        public async Task<IEnumerable<SignatureRequest>> GetRequests(Guid documentID) {

            return await db.SignatureRequest
                .AsNoTracking()
                .Include(r=>r.Signatures)
                .Include(r => r.Signer)
                .Where(r => !r.Revoked && r.DocumentID == documentID)
                .ToListAsync();
        }

        public class SignatureRequestContainer {
            public SignatureRequest Request { get; set; }
            public IEnumerable<Font> Fonts { get; set; }
            public PersonalInfoController.PersonalInfo RequesterInfo { get; set; }
            public IEnumerable<SignaturePosition> SignaturePositions { get; set; }
            public Document Document { get; set; }
        }

        // GET api/Forms/5
        public async Task<SignatureRequestContainer> GetRequest(Guid id) {
            
            var request = await db.SignatureRequest
                .AsNoTracking()
                .Include(r => r.Signatures)
                .Include(r => r.Signer)
                .SingleOrDefaultAsync(r => r.RequestID == id);

            if (request == null) {
                throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.NotFound));
            }

            var fonts = await db.Font.ToListAsync();
            
            var requesterProfile = await db.UserProfile.SingleAsync(p => p.UserID == request.RequesterUserID);
            var requesterEmail = (await db.AspNetUsers.SingleAsync(u => u.Id == request.RequesterUserID)).Email;

            IEnumerable<SignaturePosition> positions = db.SignaturePosition.Where(p => p.DocumentID == request.DocumentID && request.Signer.RoleName == p.RoleName);

            var document = await db.Document
                .Include(d=>d.Pages)
                .SingleAsync(d => d.DocumentID == request.DocumentID);


            return new SignatureRequestContainer {
                Request = request,
                Fonts = fonts,
                Document = document,
                RequesterInfo = new PersonalInfoController.PersonalInfo {
                    Email = requesterEmail,
                    FirstName = requesterProfile.FirstName,
                    LastName = requesterProfile.LastName
                },
                SignaturePositions = positions
            };
        }

        // POST api/Forms
        [Audit.Audit("{UserDisplayName} created an e-sign request for '{EntityType}' to: {EntityDisplayName}")]
        [Authorize]
        public async Task<HttpResponseMessage> PostRequest(SignatureRequest request, bool sendEmail = true) {
            request.RequestID = SequentialGuidCreator.Create();
            request.RequesterUserID = this.UserID;
            request.CreatedAt = DateTimeOffset.Now;
            db.SignatureRequest.Add(request);
            
            await db.SaveChangesAsync();

            await db.SignatureRequest
                .Where(r => r.SignerPersonID == request.SignerPersonID && r.DocumentID == request.DocumentID && r.RequestID != request.RequestID)
                .ForEachAsync((r) => { 
                    r.Revoked = true; 
                });

            await db.SaveChangesAsync();

            request = await db.SignatureRequest
                .Include(r => r.Signer)
                .SingleOrDefaultAsync(r => r.RequestID == request.RequestID);

            if (sendEmail) {
                var document = await db.Document
                    .Include(d => d.Pages)
                    .SingleAsync(d => d.DocumentID == request.DocumentID);

                var notification = new SignerNotification(request, document, db);
                await notification.SendNotificationAsync();
            }

            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.Created, request);

            response.Headers.Location = new Uri(Url.Link("DefaultApi", new { id = request.RequestID }));
            return response;
        }

        // DELETE api/Forms/5
        [Audit.Audit]
        [Authorize]
        public async Task<HttpResponseMessage> DeleteRequest(Guid id) {
            SignatureRequest request = await db.SignatureRequest.SingleOrDefaultAsync(r => r.RequestID == id && !r.Revoked);
            if (request == null) {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }
            
            request.Revoked = true;
            
            try {
                await db.SaveChangesAsync();
            } catch (DbUpdateConcurrencyException ex) {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, ex);
            }

            return Request.CreateResponse(HttpStatusCode.OK, request);
        }
    }
}
