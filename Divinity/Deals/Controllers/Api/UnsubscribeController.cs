﻿using Divinity.Common.BaseClasses;
using Divinity.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Stripe;

namespace Divinity.Deals.Controllers.Api {
    [Authorize]
    public class UnsubscribeController : UserController {
        [Audit.Audit("User {UserDisplayName} cancelled their account with a reason of: {ObjectContentValue.Reason}.")]
        public HttpResponseMessage Post(SubscriptionCancellation info) {
            var user = db.AspNetUsers.Single(u => u.Id == this.UserID);
            var subscription = db.UserSubscription.Single(u => u.UserID == user.Id);

            var customerService = new StripeCustomerService();
            var subService = new StripeSubscriptionService();

            if (String.IsNullOrWhiteSpace(subscription.StripeToken)) {
                // nothing to do. wonder how they got here.
            }
            else {
                // gotta find the customer then cancel their subscription
                var customer = customerService.Get(subscription.StripeToken);

                if (customer.StripeSubscriptionList != null && customer.StripeSubscriptionList.TotalCount > 0) {
                    customer.StripeSubscriptionList.StripeSubscriptions.ForEach(sub => {
                        subService.Cancel(customer.Id, sub.Id);
                    });
                }
            }
            return Request.CreateResponse<SubscriptionCancellation>(HttpStatusCode.Accepted, info);
        }

        public class SubscriptionCancellation {
            public string Reason { get; set; }
        }
    }
}