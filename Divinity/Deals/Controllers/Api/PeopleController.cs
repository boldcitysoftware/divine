﻿using Divinity.Common.BaseClasses;
using Divinity.Models;
using Divinity.Models.Helpers;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Divinity.Deals.Controllers.Api {
    [Authorize]
    public class PeopleController : UserController {
        // GET api/Persons/5
        public Person GetPerson(Guid id) {
            var person = db.Person.Find(id);
            if (person == null) {
                throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.NotFound));
            }
            return person;
        }

        // PUT api/Persons/5
        [Audit.Audit]
        public HttpResponseMessage PutPerson(Guid id, Person person) {
            if (!ModelState.IsValid) {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }

            if (id != person.PersonID) {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }

            var dbPerson = db.Person.Find(id);

            if (dbPerson == null) {
                throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.NotFound));
            }

            db.Entry(dbPerson).CurrentValues.SetValues(person);
            dbPerson.UpdatedAt = DateTimeOffset.Now;

            try {
                db.SaveChanges();
            } catch (DbUpdateConcurrencyException ex) {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, ex);
            }

            return Request.CreateResponse<Person>(HttpStatusCode.OK, dbPerson);
        }

        // POST api/Persons
        [Audit.Audit]
        public HttpResponseMessage PostPerson(Person person) {
            if (ModelState.IsValid) {
                person.PersonID = SequentialGuidCreator.Create();
                person.CreatedAt = person.UpdatedAt = DateTimeOffset.Now;

                db.Person.Add(person);
                db.SaveChanges();

                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.Created, person);
                response.Headers.Location = new Uri(Url.Link("DefaultApi", new { id = person.PersonID }));
                return response;
            } else {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
        }

        // DELETE api/Persons/5
        [Audit.Audit]
        public HttpResponseMessage DeletePerson(Guid id) {
            Person person = db.Person.Find(id);
            if (person == null) {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }

            db.Person.Remove(person);

            try {
                db.SaveChanges();
            } catch (DbUpdateConcurrencyException ex) {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, ex);
            }

            return Request.CreateResponse(HttpStatusCode.OK, person);
        }
    }
}
