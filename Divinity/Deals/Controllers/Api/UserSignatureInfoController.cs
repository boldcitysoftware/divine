﻿using System.Data.Entity;
using Divinity.Common.BaseClasses;
using Divinity.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Threading.Tasks;

namespace Divinity.Deals.Controllers.Api {
    
    public class UserSignatureInfoController : UserController {
        public async Task<UserSignature> Get() {
            var user = await db.AspNetUsers.SingleAsync(u => u.Id == this.UserID);

            return await db.UserSignature
                .Where(u => u.UserID == user.Id)
                .FirstOrDefaultAsync();
        }

        public async Task<UserSignature> Get(string id) {
            return await db.UserSignature
                .Where(u => u.UserID == id)
                .FirstOrDefaultAsync();
        }

        [Authorize]
        public async Task<HttpResponseMessage> Put(string id, UserSignature sig) {
            sig.UserID = this.UserID; // they probably didn't submit the user ID, so I'll deal with that

            if (!ModelState.IsValid) {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }

            var info = await db.UserSignature.SingleOrDefaultAsync(u => u.UserID == this.UserID);

            if (info == null) {
                db.UserSignature.Add(sig);
            }
            else {
                db.Entry(info).CurrentValues.SetValues(sig);
            }

            await db.SaveChangesAsync();
            

            return Request.CreateResponse(HttpStatusCode.OK);
        }
    }
}