﻿using Divinity.Common.BaseClasses;
using Divinity.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Stripe;
using System.Web.Http.Filters;

namespace Divinity.Deals.Controllers.Api {
    [Authorize]
    public class SubscriptionStatusController : UserController {
        public SubscriptionInfo Get() {
            var user = db.AspNetUsers.Single(u => u.Id == this.UserID);
            var reg = db.UserSubscription.SingleOrDefault(u => u.UserID == user.Id);

            var info = new SubscriptionInfo {
                DealsCreated = db.Deal.Count(d=>d.UserID == this.UserID),
                FreeDeals = reg.FreeDeals
            };

            info.StripeSubscriptionStatus = "free";

            if (!String.IsNullOrWhiteSpace(reg.StripeToken)) {

                // guess I better check on a subscription
                var customerService = new StripeCustomerService();
                var customer = customerService.Get(reg.StripeToken);

                info.StripeMemberSince = customer.Created;

                var sub = customer.StripeSubscriptionList.StripeSubscriptions.FirstOrDefault();
                if (sub != null) {
                    info.StripeSubscriptionStatus = sub.Status;
                    info.NextPaymentDate = sub.PeriodEnd;
                    info.StatementDescription = sub.StripePlan.StatementDescription;
                    info.CardType = customer.StripeCardList.StripeCards[0].Brand;
                    info.CardLast4 = customer.StripeCardList.StripeCards[0].Last4;
                    info.FullPrice = (decimal)sub.StripePlan.Amount / 100M;

                    info.Discounted = sub.StripeDiscount != null;

                    if (info.Discounted) {
                        info.DiscountPercent = sub.StripeDiscount.StripeCoupon.PercentOff ?? 0;
                        DateTime start = sub.StripeDiscount.Start.Value;
                        int monthsLeft = sub.StripeDiscount.StripeCoupon.DurationInMonths.Value;
                        while (start < DateTime.Now) {
                            monthsLeft--;
                            start = start.AddMonths(1);
                        }
                        info.DiscountedMonthsLeft = monthsLeft;
                        info.DiscountedPrice = info.FullPrice - (info.FullPrice * (decimal)info.DiscountPercent / 100M);
                    }
                }
            }
            
            return info;
        }
    }

    public class SubscriptionInfo {
        public int DealsCreated { get; set; }
        public string StripeSubscriptionStatus { get; set; }
        public DateTime? NextPaymentDate { get; set; }
        public DateTime? StripeMemberSince { get; set; }
        public bool Discounted { get; set; }

        public string StatementDescription { get; set; }

        public string CardType { get; set; }

        public string CardLast4 { get; set; }

        public int DiscountPercent { get; set; }

        public int DiscountedMonthsLeft { get; set; }

        public decimal FullPrice { get; set; }

        public decimal DiscountedPrice { get; set; }

        public int FreeDeals { get; set; }
    }
}