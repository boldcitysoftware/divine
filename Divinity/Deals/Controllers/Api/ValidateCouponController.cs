﻿using Divinity.Common.BaseClasses;
using Divinity.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Stripe;

namespace Divinity.Deals.Controllers.Api {
    [Authorize]
    public class ValidateCouponController : UserController {
        public HttpResponseMessage Get(string couponCode) {
            try {
                var couponService = new StripeCouponService();
                var coupon = couponService.Get(couponCode);
                if (coupon == null) {
                    throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.NotFound));
                }
                else {
                    var planService = new StripePlanService();
                    var plan = planService.Get("deals");
                    decimal planCost = (decimal)plan.Amount / 100M;
                    decimal discountAmount = planCost * (decimal)coupon.PercentOff / 100M;
                    decimal discountedCost = planCost - discountAmount;

                    return Request.CreateResponse<CouponInfo>(new CouponInfo { DiscountedPrice = discountedCost, DiscountPercent = coupon.PercentOff ?? 0 });
                }
            }
            catch (StripeException se) {
                return Request.CreateResponse<StripeException>(se);
            }
        }
    }

    public class CouponInfo {
        public int DiscountPercent { get; set; }
        public decimal DiscountedPrice { get; set; }
    }
}