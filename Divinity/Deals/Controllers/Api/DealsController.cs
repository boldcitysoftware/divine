﻿using Divinity.Deals.Audit;
using Divinity.Common.BaseClasses;
using Divinity.Models;
using Divinity.Models.Helpers;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Filters;

namespace Divinity.Deals.Controllers.Api {
    [Authorize]
    public class DealsController : UserController {
        // GET api/Deals
        public IEnumerable<Deal> GetDeals() {
            var deals = db.Deal
                .Include(d => d.People)
                .Include(d => d.TaskLists)
                .Include(d => d.Properties)
                .Include(d => d.Forms)
                .Include(d => d.Forms.Select(f => f.FormTemplate))
                .Include(d => d.AccessGrants)
                .Include(d => d.Documents)
                .Where(d => d.AccessGrants.Any(ag => ag.UserID == this.UserID && !ag.Revoked))
                .ToList();
            
            return deals;
        }

        // GET api/Deals/5
        public Deal GetDeal(Guid id) {
            var deal = db.Deal
                .Include(d => d.People)
                .Include(d => d.TaskLists)
                .Include(d => d.Properties)
                .Include(d => d.Forms)
                .Include(d => d.Forms.Select(f => f.FormTemplate))
                .Include(d => d.AccessGrants)
                .Include(d => d.Documents)
                .Where(d => d.AccessGrants.Any(ag => ag.UserID == this.UserID && !ag.Revoked))
                .SingleOrDefault(d => d.DealID == id);
            if (deal == null) {
                throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.NotFound));
            }
            return deal;
        }

        // PUT api/Deals/5
        [Audit.Audit]
        public HttpResponseMessage PutDeal(Guid id, Deal deal) {
            if (!ModelState.IsValid) {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            if (id != deal.DealID) {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }

            var dbDeal = db.Deal
                .Include(d => d.People)
                .Include(d => d.TaskLists)
                .Include(d => d.Properties)
                .Include(d => d.AccessGrants)
                .Where(d => d.AccessGrants.Any(ag => ag.UserID == this.UserID && !ag.Revoked))
                .SingleOrDefault(d => d.DealID == id);

            
            if (dbDeal == null) {
                throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.NotFound));
            }

            db.Entry(dbDeal).CurrentValues.SetValues(deal);
            dbDeal.UpdatedAt = DateTimeOffset.Now;

            try {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException ex) {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, ex);
            }

            return Request.CreateResponse<Deal>(HttpStatusCode.OK, dbDeal);
        }

        // POST api/Deals
        [Audit]
        public HttpResponseMessage PostDeal(Deal deal) {
            if (ModelState.IsValid) {

                // let's make sure I can even make a deal..
                var subscription = db.UserSubscription.Find(this.UserID);


                int dealsCreated = db.Deal.Count(d => d.UserID == this.UserID);
                
                deal.DealID = SequentialGuidCreator.Create();
                deal.UserID = this.UserID;
                deal.CreatedAt = deal.UpdatedAt = DateTimeOffset.Now;

                var profile = db.UserProfile.Find(this.UserID);
                var user = db.AspNetUsers.Find(this.UserID);

                db.Deal.Add(deal);

                TaskList myTasks = new TaskList {
                    TaskListID = SequentialGuidCreator.Create(),
                    DealID = deal.DealID,
                    CreatedAt = DateTimeOffset.Now,
                    UpdatedAt = DateTimeOffset.Now,
                    DisplayName = "Tasks"
                };

                db.TaskList.Add(myTasks);

                // need to add the user as a person with access
                Person buyerAgent = new Person {
                    PersonID = SequentialGuidCreator.Create(),
                    FirstName = profile.FirstName,
                    LastName = profile.LastName,
                    Email = user.Email,
                    RoleName = "Buyer's Agent",
                    CreatedAt = DateTimeOffset.Now,
                    UpdatedAt = DateTimeOffset.Now,
                    DealID = deal.DealID
                };

                DealAccess buyerAgentAccess = new DealAccess {
                    AccessGrantedAt = DateTimeOffset.Now,
                    AccessGrantedBy = this.UserID,
                    AccessID = SequentialGuidCreator.Create(),
                    DealID = deal.DealID,
                    PersonID = buyerAgent.PersonID,
                    UserID = deal.UserID,
                };

                db.Person.Add(buyerAgent);
                db.DealAccess.Add(buyerAgentAccess);
                
                db.SaveChanges();
                

                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.Created, deal);
                response.Headers.Location = new Uri(Url.Link("DefaultApi", new { id = deal.DealID }));
                return response;
            }
            else {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
        }

        
    }
}