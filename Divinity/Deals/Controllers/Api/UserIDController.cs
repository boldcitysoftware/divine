﻿using System.Data.Entity;
using Divinity.Common.BaseClasses;
using Divinity.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Threading.Tasks;

namespace Divinity.Deals.Controllers.Api {
    public class UserIDResponse { public string UserID { get; set; } }
    public class UserIDController : UserController {
        [Authorize]
        public UserIDResponse Get() {
            return new UserIDResponse { UserID = this.UserID };
        }
    }
}