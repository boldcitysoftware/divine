/// <vs SolutionOpened='watch-app-js' />
/// <binding ProjectOpened='watch-app-js' />
/// <vs BeforeBuild='default' SolutionOpened='watch-app-js' />
var gulp = require('gulp');
var concat = require('gulp-concat');
var templateCache = require('gulp-angular-templatecache');

var paths = {
    appScripts: ['client/app/**/*.js', '!client/app/app-concat.js'],
    css: 'content/**/*.css',
    appViews: ['client/app/views/**/*.html','client/app/custom/**/*.html'],
    libScripts: ['scripts/jquery-2.1.1.js', 'scripts/jquery-ui.js', 'scripts/angular.js', 'scripts/angular-animate.js', 'scripts/angular-cookies.js', 'scripts/angular-flash.js', 'scripts/angular-loader.js', 'scripts/angular-local-storage.js', 'scripts/angular-mocks.js', 'scripts/angular-ng-grid.js', 'scripts/angular-resource.js', 'scripts/angular-route.js', 'scripts/angular-sanitize.js', 'scripts/angular-scenario.js', 'scripts/angular-touch.js', 'scripts/ng-flow-standalone.js', 'scripts/ng-flow.js', 'scripts/bootstrap.js', 'scripts/ui-bootstrap.js', 'scripts/ui-grid.js'],
    bundles:'client/bundles'
};

gulp.task('concat-app-js', function () {
    gulp.src(paths.appScripts)
        .pipe(concat('app-bundle.js'))
        .pipe(gulp.dest(paths.bundles));
});

gulp.task('concat-css', function () {
    gulp.src(paths.css)
        .pipe(concat('style-bundle.css'))
        .pipe(gulp.dest(paths.bundles));
});

gulp.task('precache-views', function () {
    gulp.src(paths.appViews)
        .pipe(templateCache({ filename: 'view-bundle.js', module: 'main' }))
        .pipe(gulp.dest(paths.bundles));
});

gulp.task('concat-lib-js', function () {
    gulp.src(paths.libScripts)
        .pipe(concat('lib-bundle.js'))
        .pipe(gulp.dest(paths.bundles));
});

gulp.task('watch-app-js', function () {
    
    gulp.watch(paths.appScripts, ['concat-app-js']);
    gulp.watch(paths.css, ['concat-css']);
    gulp.watch(paths.appViews, ['precache-views']);
    
});

gulp.task('default', ['precache-views', 'concat-app-js', 'concat-lib-js','concat-css']);